const { writeFileSync } = require("fs");
const { join } = require("path");

const trees = [
  "Birch",
  "Cedar",
  "Ceiba",
  "Fir",
  "Joshua",
  "Oak",
  "OldGrowthRedwood",
  "Palm",
  "Redwood",
  "SaguaroCactus",
  "Spruce",
];

const content = `
namespace Eco.Mods.Organisms
{
${trees
  .map(
    (tree) => `
    public partial class ${tree}
    {
        public partial class ${tree}Species
        {
            partial void ModsPostInitialize()
            {
                this.ReleasesCO2TonsPerDay *= 2;
            }
        }
    }`
  )
  .join("\n")}
}
`;

writeFileSync(join(__dirname, "..", "UserCode", "TreeCO2Rates.cs"), content);
