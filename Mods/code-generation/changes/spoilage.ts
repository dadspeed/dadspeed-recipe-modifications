import fs from "fs";
import path from "path";
import { ChangeSet } from "../change-implementations";

export function removeSpoilage(sourcePath: string): ChangeSet {
  const foodDir = fs
    .readdirSync(path.join(sourcePath, "AutoGen", "Food"))
    .map((filename) =>
      path.join("AutoGen", "Food", filename.replace(/\.cs$/, ""))
    );

  const seedDir = fs
    .readdirSync(path.join(sourcePath, "AutoGen", "Seed"))
    .map((filename) =>
      path.join("AutoGen", "Seed", filename.replace(/\.cs$/, ""))
    );

  return {
    description: `Remove spoilage from all seeds`,
    changes: [...seedDir].map((file) => {
      return {
        file,
        changes: [
          {
            type: "SetSpoilage",
            value: "float.PositiveInfinity",
          },
        ],
      };
    }),
  };
}
