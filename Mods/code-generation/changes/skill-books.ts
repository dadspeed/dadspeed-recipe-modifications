import path from "path";
import { ChangeSet } from "../change-implementations";

export const skillBookChanges: ChangeSet = {
  description: `Change skill books to instead give skill scrolls. Level requirements are also shifted to slow down progress and require more trading.`,
  changes: [
    {
      file: path.join("AutoGen", "Tech", "AdvancedBaking"),
      changes: [
        {
          type: "SkillRequirement",
          skillType: "BakingSkill",
          oldSkill: 1,
          skill: 3,
        },
        { type: "SkillBookToScroll", scrollAmount: 10 },
      ],
    },
    {
      file: path.join("AutoGen", "Tech", "AdvancedCooking"),
      changes: [
        {
          type: "SkillRequirement",
          skillType: "CookingSkill",
          oldSkill: 1,
          skill: 3,
        },
        { type: "SkillBookToScroll", scrollAmount: 10 },
      ],
    },
    {
      file: path.join("AutoGen", "Tech", "AdvancedMasonry"),
      changes: [
        {
          type: "SkillRequirement",
          skillType: "PotterySkill",
          oldSkill: 1,
          skill: 3,
        },
        { type: "SkillBookToScroll", scrollAmount: 10 },
      ],
    },
    {
      file: path.join("AutoGen", "Tech", "AdvancedSmelting"),
      changes: [
        {
          type: "SkillRequirement",
          skillType: "SmeltingSkill",
          oldSkill: 1,
          skill: 3,
        },
        { type: "SkillBookToScroll", scrollAmount: 5 },
      ],
    },
    {
      file: path.join("AutoGen", "Tech", "Baking"),
      changes: [
        {
          type: "SkillRequirement",
          skillType: "CampfireCookingSkill",
          oldSkill: 1,
          skill: 3,
        },
        { type: "SkillBookToScroll", scrollAmount: 3 },
      ],
    },
    {
      file: path.join("AutoGen", "Tech", "BasicEngineering"),
      changes: [{ type: "SkillBookToScroll", scrollAmount: 3 }],
    },
    {
      file: path.join("AutoGen", "Tech", "Blacksmith"),
      changes: [{ type: "SkillBookToScroll", scrollAmount: 3 }],
    },
    {
      file: path.join("AutoGen", "Tech", "Butchery"),
      changes: [{ type: "SkillBookToScroll", scrollAmount: 3 }],
    },
    {
      file: path.join("AutoGen", "Tech", "Carpentry"),
      changes: [{ type: "SkillBookToScroll", scrollAmount: 3 }],
    },
    {
      file: path.join("AutoGen", "Tech", "Composites"),
      changes: [
        {
          type: "SkillRequirement",
          skillType: "CarpentrySkill",
          oldSkill: 1,
          skill: 3,
        },
        { type: "SkillBookToScroll", scrollAmount: 10 },
      ],
    },
    {
      file: path.join("AutoGen", "Tech", "Cooking"),
      changes: [
        {
          type: "SkillRequirement",
          skillType: "CampfireCookingSkill",
          oldSkill: 1,
          skill: 3,
        },
        { type: "SkillBookToScroll", scrollAmount: 3 },
      ],
    },
    {
      file: path.join("AutoGen", "Tech", "CuttingEdgeCooking"),
      changes: [
        {
          type: "SkillRequirement",
          skillType: "AdvancedCookingSkill",
          oldSkill: 1,
          skill: 3,
        },
        { type: "SkillBookToScroll", scrollAmount: 15 },
      ],
    },
    {
      file: path.join("AutoGen", "Tech", "Electronics"),
      changes: [
        {
          type: "SkillRequirement",
          skillType: "MechanicsSkill",
          oldSkill: 1,
          skill: 3,
        },
        { type: "SkillBookToScroll", scrollAmount: 10 },
      ],
    },
    {
      file: path.join("AutoGen", "Tech", "Farming"),
      changes: [{ type: "SkillBookToScroll", scrollAmount: 3 }],
    },
    {
      file: path.join("AutoGen", "Tech", "Fertilizers"),
      changes: [
        {
          type: "SkillRequirement",
          skillType: "FarmingSkill",
          oldSkill: 1,
          skill: 3,
        },
        { type: "SkillBookToScroll", scrollAmount: 3 },
      ],
    },
    {
      file: path.join("AutoGen", "Tech", "Glassworking"),
      changes: [
        {
          type: "SkillRequirement",
          skillType: "MasonrySkill",
          oldSkill: 1,
          skill: 3,
        },
        { type: "SkillBookToScroll", scrollAmount: 5 },
      ],
    },
    {
      file: path.join("AutoGen", "Tech", "Industry"),
      changes: [
        {
          type: "SkillRequirement",
          skillType: "MechanicsSkill",
          oldSkill: 1,
          skill: 3,
        },
        { type: "SkillBookToScroll", scrollAmount: 10 },
      ],
    },
    {
      file: path.join("AutoGen", "Tech", "Masonry"),
      changes: [{ type: "SkillBookToScroll", scrollAmount: 3 }],
    },
    {
      file: path.join("AutoGen", "Tech", "Mechanics"),
      changes: [
        {
          type: "SkillRequirement",
          skillType: "BasicEngineeringSkill",
          oldSkill: 1,
          skill: 3,
        },
        { type: "SkillBookToScroll", scrollAmount: 5 },
      ],
    },
    {
      file: path.join("AutoGen", "Tech", "Milling"),
      changes: [
        {
          type: "SkillRequirement",
          skillType: "FarmingSkill",
          oldSkill: 1,
          skill: 3,
        },
        { type: "SkillBookToScroll", scrollAmount: 5 },
      ],
    },
    {
      file: path.join("AutoGen", "Tech", "Painting"),
      changes: [{ type: "SkillBookToScroll", scrollAmount: 5 }],
    },
    {
      file: path.join("AutoGen", "Tech", "OilDrilling"),
      changes: [
        {
          type: "SkillRequirement",
          skillType: "MechanicsSkill",
          oldSkill: 1,
          skill: 3,
        },
        { type: "SkillBookToScroll", scrollAmount: 10 },
      ],
    },
    {
      file: path.join("AutoGen", "Tech", "PaperMilling"),
      changes: [{ type: "SkillBookToScroll", scrollAmount: 5 }],
    },
    {
      file: path.join("AutoGen", "Tech", "Pottery"),
      changes: [
        {
          type: "SkillRequirement",
          skillType: "MasonrySkill",
          oldSkill: 1,
          skill: 3,
        },
        { type: "SkillBookToScroll", scrollAmount: 5 },
      ],
    },
    {
      file: path.join("AutoGen", "Tech", "Shipwright"),
      changes: [{ type: "SkillBookToScroll", scrollAmount: 5 }],
    },
    {
      file: path.join("AutoGen", "Tech", "Smelting"),
      changes: [{ type: "SkillBookToScroll", scrollAmount: 5 }],
    },
    {
      file: path.join("AutoGen", "Tech", "Tailoring"),
      changes: [{ type: "SkillBookToScroll", scrollAmount: 3 }],
    },
  ],
};
