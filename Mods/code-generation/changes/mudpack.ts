﻿import path from "path";
import {ChangeSet} from "../change-implementations";

export const mudpackChanges: ChangeSet[] = [
  {
    // Fish oil
    // New basic fish oil recipe had been added through a new file BasicFishOil.cs
    // - Remove the vanilla fish oil recipe since it makes regular fish oil in favour of the advanced fish oil recipe
    // - Alter the processed fish oil recipe so it spits out Fish Oil
    description: "Alter default fish oils to produce Fish Oil and change them around",
    changes: [
      {
        description: "Remove fish oil recipe",
        file: path.join("AutoGen", "Recipe", "FishOil"),
        changes: [
          {
            type: "RemoveFile",
          }
        ]
      },
      {
        description: "Change fish oil to be available from Campfire Cooking",
        file: path.join("AutoGen", "Recipe", "ProcessedFishOil"),
        changes: [
          {
            type: "RecipeInput",
            ingredient: "typeof(RawFishItem)",
            oldAmount: 5,
            newAmount: 30,
          },
          {
            type: "RecipeOutput",
            ingredient: "OilItem",
            oldAmount: 1,
            newAmount: 0,
          },
          {
            type: "RecipeOutput",
            ingredient: "FishOilItem",
            oldAmount: 0,
            newAmount: 10,
          },
        ],
      },
    ],
  },
  // Oil Drilling
  {
    description: "Change barrel recipe",
    changes: [
      {
        description: "Alter ingredients for barrels",
        file: path.join("AutoGen", "Block", "Barrel"),
        changes: [
          {
            type: "RecipeInput",
            ingredient: "typeof(IronBarItem)",
            oldAmount: 4,
            newAmount: 0,
          },
          {
            type: "RecipeInput",
            ingredient: "typeof(IronHullSheetItem)",
            oldAmount: 0,
            newAmount: 8,
            staticness: "dynamic"
          },
          {
            type: "RecipeInput",
            ingredient: "typeof(ResinItem)",
            oldAmount: 0,
            newAmount: 4,
            staticness: "dynamic"
          },
        ]
      }
    ]
  },
  {
    description: "Changes to Epoxy/Plastic/Rubber/Nylon/Lubricant",
    changes: [
      "Epoxy",
      "SyntheticRubber",
      "Plastic",
      "Nylon",
      "Lubricant",
    ].map((recipe) => {
      return {
        description: "Reduce petroleum usage but make input static. Change Barrel output for Used Barrel",
        file: path.join("Autogen", "Item", recipe),
        changes: [
          {
            type: "RecipeInput",
            ingredient: "typeof(PetroleumItem)",
            oldAmount: 4,
            newAmount: 2,
            staticness: "static",
          },
          {
            type: "RecipeOutput",
            ingredient: "BarrelItem",
            oldAmount: 3,
            newAmount: 0,
          },
          {
            type: "RecipeOutput",
            ingredient: "UsedOilBarrelItem",
            oldAmount: 0,
            newAmount: 1,
          },
        ]
      }
    }),
  },
  {
    description: "Add naphtha usage to gasoline",
    changes: [
      {
        description: "Alter Gasoline recipe to add naptha",
        file: path.join("Autogen", "Block", "Gasoline"),
        changes: [
          {
            type: "RecipeInput",
            ingredient: "typeof(PetroleumItem)",
            oldAmount: 4,
            newAmount: 2,
            staticness: "static",
          },
          {
            type: "RecipeInput",
            ingredient: "typeof(NaphthaItem)",
            oldAmount: 0,
            newAmount: 1,
            staticness: "static",
          },
        ]
      }
    ]
  },
  // Shipwright
  {
    description: "Changes for Shipwright from MudPack",
    changes: [
      {
        description: "Increase cost for Wooden Hull Planks",
        file: path.join("Autogen", "Item", "WoodenHullPlanks"),
        changes: [
          {
            type: "RecipeInput",
            ingredient: '"HewnLog"',
            oldAmount: 4,
            newAmount: 0,
          },
          {
            type: "RecipeInput",
            ingredient: '"WoodBoard"',
            oldAmount: 0,
            newAmount: 20,
            staticness: "dynamic",
          },
          {
            type: "RecipeInput",
            ingredient: 'typeof(FishOilItem)',
            oldAmount: 0,
            newAmount: 8,
            staticness: 'dynamic',
          }
        ]
      },
      {
        description: "Increase recipe cost for Iron Plates",
        file: path.join("Autogen", "Item", "IronHullSheet"),
        changes: [
          {
            type: "RecipeInput",
            ingredient: "typeof(IronPlateItem)",
            oldAmount: 2,
            newAmount: 4,
            staticness: "dynamic"
          },
          {
            type: "RecipeInput",
            ingredient: "typeof(ScrewsItem)",
            oldAmount: 4,
            newAmount: 8,
            staticness: "dynamic"
          }
        ]
      },
      {
        description: "Implement mudpack changes for Large Metal Ship Frame",
        file: path.join("Autogen", "Item", "LargeMetalShipFrame"),
        changes: [
          // Remove / Change old ingredients
          {
            type: "RecipeInput",
            ingredient: "typeof(MetalKeelItem)",
            oldAmount: 1,
            newAmount: 0,
          },
          {
            type: "RecipeInput",
            ingredient: "typeof(SteelBarItem)",
            oldAmount: 12,
            newAmount: 25,
            staticness: "dynamic"
          },
          {
            type: "RecipeInput",
            ingredient: "typeof(RivetItem)",
            oldAmount: 12,
            newAmount: 120,
            staticness: "dynamic",
          },
          // Add new ingredients
          {
            type: "RecipeInput",
            ingredient: "typeof(LargeMetalKeelItem)",
            oldAmount: 0,
            newAmount: 1,
            staticness: "static",
          },
          {
            type: "RecipeInput",
            ingredient: "typeof(DriftPinItem)",
            oldAmount: 0,
            newAmount: 10,
            staticness: "static"
          },
          {
            type: "RecipeInput",
            ingredient: "typeof(SteelPlateItem)",
            oldAmount: 0,
            newAmount: 16,
            staticness: "dynamic",
          }
        ]
      },
      {
        description: "Implement mudpack changes for Large Wooden Ship Frame",
        file: path.join("Autogen", "Item", "LargeWoodenShipFrame"),
        changes: [
          {
            type: "SkillRequirement",
            oldSkill: 2,
            skill: 4,
            skillType: "ShipwrightSkill",
          },
          // Remove / Change old ingredients
          {
            type: "RecipeInput",
            ingredient: "typeof(WoodenKeelItem)",
            oldAmount: 1,
            newAmount: 0,
          },
          // Add new ingredients
          {
            type: "RecipeInput",
            ingredient: "typeof(LargeWoodenKeelItem)",
            oldAmount: 0,
            newAmount: 1,
            staticness: "static",
          },
          {
            type: "RecipeInput",
            ingredient: "typeof(ResinItem)",
            oldAmount: 0,
            newAmount: 10,
            staticness: "static",
          },
          {
            type: "RecipeInput",
            ingredient: "typeof(IronPlateItem)",
            oldAmount: 0,
            newAmount: 20,
            staticness: "dynamic",
          }
        ]
      },
      {
        description: "Implement mudpack changes for Medium Metal Ship Frame",
        file: path.join("Autogen", "Item", "MediumMetalShipFrame"),
        changes: [
          // Remove / Change old ingredients
          {
            type: "RecipeInput",
            ingredient: "typeof(MetalKeelItem)",
            oldAmount: 1,
            newAmount: 0,
          },
          {
            type: "RecipeInput",
            ingredient: "typeof(IronBarItem)",
            oldAmount: 12,
            newAmount: 10,
            staticness: "dynamic",
          },
          {
            type: "RecipeInput",
            ingredient: "typeof(ScrewsItem)",
            oldAmount: 12,
            newAmount: 48,
            staticness: "dynamic"
          },
          // Add new ingredients
          {
            type: "RecipeInput",
            ingredient: "typeof(MediumMetalKeelItem)",
            oldAmount: 0,
            newAmount: 1,
            staticness: "static",
          },
          {
            type: "RecipeInput",
            ingredient: "typeof(IronPlateItem)",
            oldAmount: 0,
            newAmount: 8,
            staticness: "dynamic",
          },
        ]
      },
      {
        description: "Implement mudpack changes for Medium Wooden Ship Frame",
        file: path.join("Autogen", "Item", "MediumWoodenShipFrame"),
        changes: [
          // Remove / Change old ingredients
          {
            type: "RecipeInput",
            ingredient: "typeof(WoodenKeelItem)",
            oldAmount: 1,
            newAmount: 0,
          },
          // Add new ingredients
          {
            type: "RecipeInput",
            ingredient: "typeof(MediumWoodenKeelItem)",
            oldAmount: 0,
            newAmount: 1,
            staticness: "static",
          },
          {
            type: "RecipeInput",
            ingredient: "typeof(ResinItem)",
            oldAmount: 0,
            newAmount: 5,
            staticness: "static",
          },
          {
            type: "RecipeInput",
            ingredient: "typeof(ScrewsItem)",
            oldAmount: 0,
            newAmount: 30,
            staticness: "dynamic"
          },
          {
            type: "RecipeInput",
            ingredient: "typeof(IronPlateItem)",
            oldAmount: 0,
            newAmount: 6,
            staticness: "dynamic",
          }
        ]
      },
      {
        description: "Implement mudpack changes for Small Wooden Ship Frame",
        file: path.join("Autogen", "Item", "SmallWoodenShipFrame"),
        changes: [
          // Remove / Change old ingredients
          {
            type: "RecipeInput",
            ingredient: "typeof(WoodenKeelItem)",
            oldAmount: 1,
            newAmount: 0,
          },
          // Add new ingredients
          {
            type: "RecipeInput",
            ingredient: "typeof(SmallWoodenKeelItem)",
            oldAmount: 0,
            newAmount: 1,
            staticness: "static",
          },
          {
            type: "RecipeInput",
            ingredient: "typeof(ResinItem)",
            oldAmount: 0,
            newAmount: 2,
            staticness: "static",
          },
          {
            type: "RecipeInput",
            ingredient: "typeof(DowelItem)",
            oldAmount: 0,
            newAmount: 60,
            staticness: "dynamic"
          },
        ]
      },
      {
        description: "Remove vanilla Wood Keel",
        file: path.join("Autogen", "Item", "WoodenKeel"),
        changes: [{ type: "RemoveFile" }],
      },
      {
        description: "Remove vanilla Metal Keel",
        file: path.join("Autogen", "Item", "MetalKeel"),
        changes: [{ type: "RemoveFile" }],
      },
    ]
  },
  {
    description: "Change ashlar to include mortar, reinforced steel and not use steel bar or make crushed",
    changes: [
    "Basalt",
    "Gneiss",
    "Granite",
    "Limestone",
    "Sandstone",
    "Shale",
  ].map((rockType) => {
    return {
      description: `Change recipe requirements for ${rockType} ashlar`,
      file: path.join("Autogen", "Block", `Ashlar${rockType}`),
      changes: [
        {
          type: "RecipeInput",
          ingredient: `typeof(SteelBarItem)`,
          oldAmount: 1,
          newAmount: 0,
          staticness: "dynamic",
          comment: "Remove steelbar requirement",
        },
        {
          type: "RecipeInput",
          ingredient: `typeof(CementItem)`,
          oldAmount: 6,
          newAmount: 0,
          staticness: "dynamic",
          comment: "Remove cement requirement",
        },
        {
          type: "RecipeInput",
          ingredient: `typeof(ReinforcedConcreteItem)`,
          oldAmount: 0,
          newAmount: 2,
          staticness: "dynamic",
          comment: "Require 2 reinforced concrete - dynamic",
        },
        {
          type: "RecipeInput",
          ingredient: `typeof(MortarItem)`,
          oldAmount: 0,
          newAmount: 6,
          staticness: "dynamic",
          comment: "Require 6 mortar item ",
        },
        {
          type: "RecipeOutput",
          ingredient: `Crushed${rockType}Item`,
          oldAmount: 2,
          newAmount: 0,
          staticness: "dynamic",
        },
        {
          type: "RecipeOutput",
          ingredient: `Ashlar${rockType}Item`,
          oldAmount: 2,
          newAmount: 1,
          comment: "Vanilla set output to 2 in v11, we move it back to 1"
        }
      ]
    }
  })
  }
]