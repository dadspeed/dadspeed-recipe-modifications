import path from "path";
import { ChangeSet } from "../change-implementations";
export const lightChanges: ChangeSet = {
  description: `Change the power consumption of lights. Indoor lights are changed to mimic the power consumption of LEDs (around 10W) and outdoor lights are changed to mimic HPS (around 40W).`,

  changes: [
    // Lights
    // 100W HPS is about the same as 40W LED
    // 60W Incandescent is about the same as 10W LED
    {
      file: path.join("AutoGen", "WorldObject", "Chandelier"),
      changes: [
        {
          type: "ChangePowerConsumption",
          oldConsumption: 60,
          newConsumption: 20,
        },
      ],
    },
    {
      file: path.join("AutoGen", "WorldObject", "ElectricWallLamp"),
      changes: [
        {
          type: "ChangePowerConsumption",
          oldConsumption: 60,
          newConsumption: 40,
        },
      ],
    },
    {
      file: path.join("AutoGen", "WorldObject", "HangingElectricWallLamp"),
      changes: [
        {
          type: "ChangePowerConsumption",
          oldConsumption: 60,
          newConsumption: 40,
        },
      ],
    },
    {
      file: path.join("AutoGen", "WorldObject", "ModernDoubleStreetLight"),
      changes: [
        {
          type: "ChangePowerConsumption",
          oldConsumption: 100,
          newConsumption: 80,
        },
      ],
    },
    {
      file: path.join("AutoGen", "WorldObject", "ModernStreetLight"),
      changes: [
        {
          type: "ChangePowerConsumption",
          oldConsumption: 100,
          newConsumption: 40,
        },
      ],
    },
    {
      file: path.join("AutoGen", "WorldObject", "SteelAbstractFixture"),
      changes: [
        {
          type: "ChangePowerConsumption",
          oldConsumption: 60,
          newConsumption: 10,
        },
      ],
    },
    {
      file: path.join("AutoGen", "WorldObject", "SteelCeilingLight"),
      changes: [
        {
          type: "ChangePowerConsumption",
          oldConsumption: 60,
          newConsumption: 20,
        },
      ],
    },
    {
      file: path.join("AutoGen", "WorldObject", "SteelFloorLamp"),
      changes: [
        {
          type: "ChangePowerConsumption",
          oldConsumption: 60,
          newConsumption: 10,
        },
      ],
    },
    {
      file: path.join("AutoGen", "WorldObject", "SteelHangingFixture"),
      changes: [
        {
          type: "ChangePowerConsumption",
          oldConsumption: 60,
          newConsumption: 10,
        },
      ],
    },
    {
      file: path.join("AutoGen", "WorldObject", "SteelHangingLamp"),
      changes: [
        {
          type: "ChangePowerConsumption",
          oldConsumption: 60,
          newConsumption: 10,
        },
      ],
    },
    {
      file: path.join("AutoGen", "WorldObject", "SteelKitchenLamp"),
      changes: [
        {
          type: "ChangePowerConsumption",
          oldConsumption: 60,
          newConsumption: 10,
        },
      ],
    },
    {
      file: path.join("AutoGen", "WorldObject", "SteelSearchlight"),
      changes: [
        {
          type: "ChangePowerConsumption",
          oldConsumption: 60,
          newConsumption: 20,
        },
      ],
    },
    {
      file: path.join("AutoGen", "WorldObject", "SteelSquareFixture"),
      changes: [
        {
          type: "ChangePowerConsumption",
          oldConsumption: 60,
          newConsumption: 10,
        },
      ],
    },
    {
      file: path.join("AutoGen", "WorldObject", "SteelTableLamp"),
      changes: [
        {
          type: "ChangePowerConsumption",
          oldConsumption: 60,
          newConsumption: 10,
        },
      ],
    },
    {
      file: path.join("AutoGen", "WorldObject", "Streetlamp"),
      changes: [
        {
          type: "ChangePowerConsumption",
          oldConsumption: 100,
          newConsumption: 40,
        },
      ],
    },
    {
      file: path.join("AutoGen", "WorldObject", "WoodenCeilingLight"),
      changes: [
        {
          type: "ChangePowerConsumption",
          oldConsumption: 60,
          newConsumption: 10,
        },
      ],
    },
    {
      file: path.join("AutoGen", "WorldObject", "WoodenFloorLamp"),
      changes: [
        {
          type: "ChangePowerConsumption",
          oldConsumption: 60,
          newConsumption: 10,
        },
      ],
    },
    {
      file: path.join("AutoGen", "WorldObject", "WoodenKitchenLamp"),
      changes: [
        {
          type: "ChangePowerConsumption",
          oldConsumption: 60,
          newConsumption: 10,
        },
      ],
    },
    {
      file: path.join("AutoGen", "WorldObject", "WoodenTableLamp"),
      changes: [
        {
          type: "ChangePowerConsumption",
          oldConsumption: 60,
          newConsumption: 10,
        },
      ],
    },
    {
      file: path.join("AutoGen", "WorldObject", "LargeFestivePaperLantern"),
      changes: [
        {
          type: "ChangePowerConsumption",
          oldConsumption: 60,
          newConsumption: 20,
        },
      ],
    },
    {
      file: path.join("AutoGen", "WorldObject", "LargePaperLantern"),
      changes: [
        {
          type: "ChangePowerConsumption",
          oldConsumption: 60,
          newConsumption: 20,
        },
      ],
    },
    {
      file: path.join("AutoGen", "WorldObject", "SmallPaperLantern"),
      changes: [
        {
          type: "ChangePowerConsumption",
          oldConsumption: 60,
          newConsumption: 10,
        }
      ]
    },
  ],
};
