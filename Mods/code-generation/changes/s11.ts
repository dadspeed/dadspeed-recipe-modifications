import path from "path";
import { ChangeSet } from "../change-implementations";

export const s11Changes: ChangeSet[] = [
  {
    description:
      "Move Clay Mold recipe from Gathering to Farmer and update inputs",
    changes: [
      {
        description: "Change Clay Mold recipe input and update profession",
        file: path.join("AutoGen", "Item", "ClayMold"),
        changes: [
          {
            type: "SkillRequirement",
            oldSkill: 1,
            skill: 1,
            skillType: "GatheringSkill",
            newSkillType: "FarmingSkill",
          },
          {
            type: "RecipeInput",
            ingredient: "typeof(ClayItem)",
            oldAmount: 1,
            newAmount: 4,
            staticness: "dynamic",
            comment:
              "Increase Clay input and change to dynamic for Farmer profession",
          },
        ],
      },
    ],
  },

  {
    description:
      "Update Masonry recipes for new profession requirements and Cement recipe input changes",
    changes: [
      {
        description: "Move Quicklime recipe from Masonry to Blacksmithing",
        file: path.join("AutoGen", "Item", "Quicklime"),
        changes: [
          {
            type: "SkillRequirement",
            oldSkill: 1,
            skill: 1,
            skillType: "MasonrySkill",
            newSkillType: "BlacksmithSkill",
          },
        ],
      },
      {
        description: "Move Tallow Lamp recipe from Masonry to Pottery",
        file: path.join("AutoGen", "WorldObject", "TallowLamp"),
        changes: [
          {
            type: "SkillRequirement",
            oldSkill: 2,
            skill: 2,
            skillType: "MasonrySkill",
            newSkillType: "PotterySkill",
          },
        ],
      },
      {
        description: "Add Silica to Cement recipe and make it dynamic",
        file: path.join("AutoGen", "Item", "Cement"),
        changes: [
          {
            type: "RecipeInput",
            ingredient: '"Silica"',
            oldAmount: 0,
            newAmount: 2,
            staticness: "dynamic",
            comment: "Add dynamic Silica requirement to Cement recipe",
          },
        ],
      },
    ],
  },
  {
    description: "Update Pottery recipes for Ceramic Mold and Toilet",
    changes: [
      {
        description: "Change Ceramic Mold recipe inputs and outputs",
        file: path.join("AutoGen", "Item", "CeramicMold"),
        changes: [
          {
            type: "RecipeInput",
            ingredient: "typeof(ClayItem)",
            oldAmount: 1,
            newAmount: 4,
            staticness: "dynamic",
            comment: "Increase Clay input and make it dynamic",
          },
          {
            type: "RecipeInput",
            ingredient: "typeof(SandItem)",
            oldAmount: 1,
            newAmount: 1,
            staticness: "dynamic",
            comment: "Set Sand input to dynamic",
          },
          {
            type: "RecipeInput",
            ingredient: '"Silica"',
            oldAmount: 1,
            newAmount: 1,
            staticness: "dynamic",
            comment: "Set Silica input to dynamic",
          },
          {
            type: "RecipeInput",
            ingredient: "typeof(ClayMoldItem)",
            oldAmount: 0,
            newAmount: 8,
            staticness: "static",
            comment: "Add static Clay Mold input",
          },
        ],
      },
      {
        description: "Change Toilet recipe inputs",
        file: path.join("AutoGen", "WorldObject", "Toilet"),
        changes: [
          {
            type: "RecipeInput",
            ingredient: "typeof(SteelPipeItem)",
            oldAmount: 6,
            newAmount: 0,
            staticness: "dynamic",
            comment: "Remove Steel Pipes from recipe",
          },
          {
            type: "RecipeInput",
            ingredient: "typeof(IronBarItem)",
            oldAmount: 0,
            newAmount: 10,
            staticness: "dynamic",
            comment: "Add Iron Bars as a dynamic input",
          },
        ],
      },
    ],
  },
  {
    description: "Update Logging recipes for Hewn Chair and Dowel",
    changes: [
      {
        description: "Change Hewn Chair recipe inputs and outputs",
        file: path.join("AutoGen", "WorldObject", "HewnChair"),
        changes: [
          {
            type: "RecipeInput",
            ingredient: "typeof(CelluloseFiberItem)",
            oldAmount: 0,
            newAmount: 2,
            staticness: "dynamic",
            comment: "Add dynamic Cellulose Fibers as input",
          },
          {
            type: "RecipeInput",
            ingredient: '"HewnLog"',
            oldAmount: 12,
            newAmount: 6,
            staticness: "dynamic",
            comment: "Reduce Hewn Logs to 6 and set to dynamic",
          },
          {
            type: "RecipeInput",
            ingredient: '"WoodBoard"',
            oldAmount: 12,
            newAmount: 6,
            staticness: "dynamic",
            comment: "Reduce Wooden Boards to 6 and set to dynamic",
          },
        ],
      },
      {
        description: "Change Dowel recipe inputs and outputs",
        file: path.join("AutoGen", "Item", "Dowel"),
        changes: [
          {
            type: "RecipeInput",
            ingredient: '"Wood"',
            oldAmount: 2,
            newAmount: 1,
            staticness: "dynamic",
            comment: "Reduce Log input to 1 and set to dynamic",
          },
          {
            type: "RecipeOutput",
            ingredient: "DowelItem",
            oldAmount: 16,
            newAmount: 8,
            comment: "Reduce Dowel output to 8",
          },
        ],
      },
    ],
  },
  // Mining
  {
    description: "Add Cotton Fabric to Arrastra recipe inputs for crushing ores",
    changes: [
      "CrushedIronOre",
      "CrushedCopperOre",
      "CrushedGoldOre",
      "CrushedSulfur",
    ].map((recipe) => {
      return {
        description: "Add Cotton Fabric to recipe inputs",
        file: path.join("Autogen", "Block", recipe),
        changes: [
          {
            type: "RecipeInput",
            ingredient: "typeof(CottonFabricItem)",
            oldAmount: 0,
            newAmount: 2,
            staticness: "static",
            comment: "Add static Cotton Fabric requirement",
          }
        ]
      }
    })
  },
  {
    description: "Add Cotton Fabric to Stamp Mill recipe inputs for crushing ores",
    changes: [
      "CrushedIronLv2",
      "CrushedCopperLv2",
      "CrushedGoldLv2",
      "CrushedSulfurLv2",
    ].map((recipe) => {
      return {
        description: "Add Cotton Fabric to recipe inputs",
        file: path.join("Autogen", "Recipe", recipe),
        changes: [
          {
            type: "RecipeInput",
            ingredient: "typeof(CottonFabricItem)",
            oldAmount: 0,
            newAmount: 4,
            staticness: "static",
            comment: "Add static Cotton Fabric requirement",
          },
        ],
      }
    })
  },
  {
    description: "Add Ore/Sulfur recipes for Jaw Crusher",
    changes: [
      "CrushedIronLv3",
      "CrushedCopperLv3",
      "CrushedGoldLv3",
      "CrushedSulfurLv3",
    ].map((recipe) => {
      return {
        description: "Add Cotton Fabric to recipe inputs",
        file: path.join("Autogen", "Recipe", recipe),
        changes: [
          {
            type: "RecipeInput",
            ingredient: "typeof(CottonFabricItem)",
            oldAmount: 0,
            newAmount: 5,
            staticness: "static",
            comment: "Add static Cotton Fabric requirement",
          },
        ],
      }
    })
  },
  {
    description: "Update Dynamite recipe inputs",
    changes: [
      {
        description: "Change Dynamite recipe inputs",
        file: path.join("AutoGen", "WorldObject", "Dynamite"),
        changes: [
          {
            type: "RecipeInput",
            ingredient: "typeof(BlackPowderItem)",
            oldAmount: 4,
            newAmount: 4,
            staticness: "dynamic",
            comment: "Change Black Powder input to dynamic",
          },
          {
            type: "RecipeInput",
            ingredient: "typeof(PaperItem)",
            oldAmount: 12,
            newAmount: 24,
            staticness: "dynamic",
            comment: "Increase Paper input to 24 and set to dynamic",
          },
        ],
      },
    ],
  },
  {
    description: "Move Mining Charge recipe from Mining to Electronics L2",
    changes: [
      {
        description: "Move Mining Charge recipe",
        file: path.join("AutoGen", "WorldObject", "MiningCharge"),
        changes: [
          {
            type: "SkillRequirement",
            oldSkill: 5,
            skill: 2,
            skillType: "MiningSkill",
            newSkillType: "ElectronicsSkill",
          },
        ],
      },
    ],
  },
  // Shipwright
  {
    description: "Increase storage size of small canoe",
    changes: [
      {
        description:
          "Inrease storage size for small canoe to match a wood cart",
        file: path.join("AutoGen", "Vehicle", "SmallCanoe"),
        changes: [
          {
            type: "VehicleModification",
            oldStorage: { stacks: 3, weight: 400 },
            newStorage: { stacks: 8, weight: 1400 },
          },
        ],
      },
    ],
  },
  // Basic Engineering
  {
    description: "Add base paint to asphalt roads",
    changes: [
      {
        description: "Add base paint for asphalt requirement",
        file: path.join("Autogen", "Block", "AsphaltConcrete"),
        changes: [
          {
            type: "RecipeInput",
            ingredient: "typeof(BasePaintItem)",
            oldAmount: 0,
            newAmount: 1,
            staticness: "static",
            comment: "Add Base paint as a static requirement",
          },
        ],
      },
    ],
  },
  {
    description: "Add dowels to Arrastra input",
    changes: [
      {
        description: "Add dowels to arrastra input",
        file: path.join("Autogen", "WorldObject", "Arrastra"),
        changes: [
          {
            type: "RecipeInput",
            ingredient: "typeof(DowelItem)",
            oldAmount: 0,
            newAmount: 5,
            staticness: "dynamic",
            comment: "Add dowels as a dynamic requirement",
          },
        ],
      },
    ],
  },
  // Mechanics
  {
    description: "Add gold wires to several recipes for Industry",
    changes: [
      {
        description: "Add gold wire to Electric water pumps",
        file: path.join("Autogen", "WorldObject", "ElectricWaterPump"),
        changes: [
          {
            type: "RecipeInput",
            ingredient: "typeof(GoldWiringItem)",
            oldAmount: 0,
            newAmount: 2,
            staticness: "dynamic",
            comment: "Add 2 gold wires as dynamic input",
          },
        ],
      },
      {
        description: "Add gold wire to Lighthouse Lamp",
        file: path.join("Autogen", "WorldObject", "LighthouseLamp"),
        changes: [
          {
            type: "RecipeInput",
            ingredient: "typeof(GoldWiringItem)",
            oldAmount: 0,
            newAmount: 2,
            staticness: "dynamic",
            comment: "Add 2 gold wires as a dynamic input",
          },
          {
            type: "ChangePowerConsumption",
            oldConsumption: 100,
            newConsumption: 40,
          },
        ],
      },
      {
        description: "Add gold wire to steel buoy recipe",
        file: path.join("Autogen", "WorldObject", "SteelBuoy"),
        changes: [
          {
            type: "RecipeInput",
            ingredient: "typeof(GoldWiringItem)",
            oldAmount: 0,
            newAmount: 2,
            staticness: "dynamic",
            comment: "Add 2 gold wires as a dynamic input",
          },
        ],
      },
    ],
  },
  // Industry
  {
    description: "Add gold wires to several recipes for Industry",
    changes: [
      {
        description: "Add gold wire to Washing Machine",
        file: path.join("Autogen", "WorldObject", "WashingMachine"),
        changes: [
          {
            type: "RecipeInput",
            ingredient: "typeof(GoldWiringItem)",
            oldAmount: 0,
            newAmount: 3,
            staticness: "dynamic",
            comment: "Add 3 gold wires as dynamic input",
          },
        ],
      },
      /* There's some custom changes to refrigerator to increase shelf life / decrease storages
      {
        description: "Add gold wire to Refrigerator",
        file: path.join("Autogen", "WorldObject", "Refrigerator"),
        changes: [
          {
            type: "RecipeInput",
            ingredient: "typeof(GoldWiringItem)",
            oldAmount: 0,
            newAmount: 3,
            staticness: "dynamic",
            comment: "Add 3 gold wires as dynamic input",
          },
        ],
      },
      */
      {
        description: "Add gold wire to Plastic Buoy",
        file: path.join("Autogen", "WorldObject", "PlasticBuoy"),
        changes: [
          {
            type: "RecipeInput",
            ingredient: "typeof(GoldWiringItem)",
            oldAmount: 0,
            newAmount: 2,
            staticness: "dynamic",
            comment: "Add 2 gold wires as dynamic input",
          },
        ],
      },
      {
        description: "Add gold wire to Injection Mold",
        file: path.join("Autogen", "WorldObject", "InjectionMoldMachine"),
        changes: [
          {
            type: "RecipeInput",
            ingredient: "typeof(GoldWiringItem)",
            oldAmount: 0,
            newAmount: 3,
            staticness: "dynamic",
            comment: "Add 3 gold wires as dynamic input",
          },
        ],
      },
    ],
  },
  // Electronics
  {
    description: "Add several gold changes to Electronics",
    changes: [
      {
        description: "Set gold bars to static for gold flakes",
        file: path.join("Autogen", "Item", "GoldFlakes"),
        changes: [
          {
            type: "RecipeInput",
            ingredient: "typeof(GoldBarItem)",
            oldAmount: 2,
            newAmount: 2,
            staticness: "static",
            comment: "Set input from dynamic to static",
          },
        ],
      },
      {
        description: "Set gold flakes to static for basic circuits",
        file: path.join("Autogen", "Item", "BasicCircuit"),
        changes: [
          {
            type: "RecipeInput",
            ingredient: "typeof(GoldFlakesItem)",
            oldAmount: 10,
            newAmount: 10,
            staticness: "static",
            comment: "Set input from dynamic to static",
          },
        ],
      },
      {
        description: "Add gold wiring as input for Electric Motor",
        file: path.join("Autogen", "Item", "ElectricMotor"),
        changes: [
          {
            type: "RecipeInput",
            ingredient: "typeof(GoldWiringItem)",
            oldAmount: 0,
            newAmount: 2,
            staticness: "dynamic",
            comment: "Add 3 gold wiring as dynamic input",
          },
        ],
      },
      {
        description: "Add gold wiring as input for Fuse",
        file: path.join("Autogen", "Item", "Fuse"),
        changes: [
          {
            type: "RecipeInput",
            ingredient: "typeof(GoldWiringItem)",
            oldAmount: 0,
            newAmount: 1,
            staticness: "static",
            comment: "Add 3 gold wiring as static input",
          },
        ],
      },
      {
        description: "Add gold wiring as input for Street Lamp",
        file: path.join("Autogen", "WorldObject", "StreetLamp"),
        changes: [
          {
            type: "RecipeInput",
            ingredient: "typeof(GoldWiringItem)",
            oldAmount: 0,
            newAmount: 2,
            staticness: "static",
            comment: "Add 2 gold wiring as static input",
          },
        ],
      },
      {
        description: "Add gold wiring as input for Modern Street Light",
        file: path.join("Autogen", "WorldObject", "ModernStreetLight"),
        changes: [
          {
            type: "RecipeInput",
            ingredient: "typeof(GoldWiringItem)",
            oldAmount: 0,
            newAmount: 2,
            staticness: "static",
            comment: "Add 2 gold wiring as static input",
          },
        ],
      },
      {
        description: "Add gold wiring as input for Modern Double Street Light",
        file: path.join("Autogen", "WorldObject", "ModernDoubleStreetLight"),
        changes: [
          {
            type: "RecipeInput",
            ingredient: "typeof(GoldWiringItem)",
            oldAmount: 0,
            newAmount: 3,
            staticness: "static",
            comment: "Add 3 gold wiring as static input",
          },
        ],
      },
    ],
  },
  // Smelting
  {
    description: "Set inputs for Fe/Cu/Au to 3 static clay molds at Bloomery",
    changes: [
      "SmeltIron",
      "SmeltCopper",
      "SmeltGold",
    ].map((metal) => {
      return {
        description: "Set inputs for Fe/Cu/Au to 3 static clay molds",
        file: path.join("Autogen", "Recipe", metal),
        changes: [
          {
            type: "RecipeInput",
            ingredient: "typeof(ClayMoldItem)",
            oldAmount: 6,
            newAmount: 3,
            staticness: "static",
          },
          {
            type: "RecipeOutput",
            ingredient: "ClayMoldItem",
            oldAmount: 3,
            newAmount: 0,
          },
        ]
      }
    })
  },
  {
    description: "Set inputs for Fe/Cu/Au to 2 static ceramic molds at Blast Furnace",
    changes: [
      "IronBar", 
      "CopperBar", 
      "GoldBar",
    ].map((metal) => {
      return {
        description: "Make ceramic molds static at Blast Furnace",
        file: path.join("Autogen", "Block", metal),
        changes: [
          {
            type: "RecipeInput",
            ingredient: "typeof(ClayMoldItem)",
            oldAmount: 4,
            newAmount: 0,
          },
          {
            type: "RecipeInput",
            ingredient: "typeof(CeramicMoldItem)",
            oldAmount: 0,
            newAmount: 2,
            staticness: "static",
          },
          {
            type: "RecipeOutput",
            ingredient: "ClayMoldItem",
            oldAmount: 2,
            newAmount: 0,
          },
        ]
      }
    })
  },
  {
    description: "Move gold bar smelting from level 4 to 3",
    changes: [
      ["SmeltGold", "Recipe"],
      ["GoldBar", "Block"]
    ].map((recipe) => {
      return {
        description: "Set gold bar from level 4 to 3",
        file: path.join("Autogen", recipe[1], recipe[0]),
        changes: [
          {
            type: "SkillRequirement",
            oldSkill: 4,
            skill: 3,
            skillType: "SmeltingSkill",
          },
        ]
      }
    })
  },
  // Adv. Smelting
  {
    description: "Changes to advanced smelting",
    changes: [
      ["CharcoalSteel", "Recipe"],
      ["SteelBar", "Block"],
    ].map((recipe) => {
      return {
        description: "Replace iron concentrate for iron bars and make molds static",
        file: path.join("Autogen", recipe[1], recipe[0]),
        changes: [
          {
            type: "RecipeInput",
            ingredient: "typeof(IronConcentrateItem)",
            oldAmount: 1,
            newAmount: 0,
          },
          {
            type: "RecipeInput",
            ingredient: "typeof(IronBarItem)",
            oldAmount: 0,
            newAmount: 8,
            staticness: "dynamic",
          },
          {
            type: "RecipeInput",
            ingredient: "typeof(CeramicMoldItem)",
            oldAmount: 4,
            newAmount: 2,
            staticness: "static",
          },
          {
            type: "RecipeOutput",
            ingredient: "CeramicMoldItem",
            oldAmount: 3,
            newAmount: 0,
          }
        ]
      }
    })
  },
  // Tailoring
  {
    description: "Decrease natural fibre requirement for cellulose fibers",
    changes: [
      {
        description: "Decrease Natural fibre requirement for cellulose fiber",
        file: path.join("Autogen", "Item", "CelluloseFiber"),
        changes: [
          {
            type: "RecipeInput",
            ingredient: '"NaturalFiber"',
            oldAmount: 20,
            newAmount: 10,
            staticness: "dynamic",
          },
        ],
      },
    ],
  },
  // Cooking
  {
    description: "Add wrapping paper to all cooking foods",
    changes: [
      ["AutumnStew", "Food"],
      ["ClamChowder", "Food"],
      ["CrispyBacon", "Food"],
      ["ExoticFruitSalad", "Recipe"],
      ["ExoticSalad", "Recipe"],
      ["ExoticVegetableMedley", "Recipe"],
      ["ForestSalad", "Recipe"],
      ["GrasslandSalad", "Recipe"],
      ["LoadedTaroFries", "Food"],
      ["MixedFruitSalad", "Recipe"],
      ["MixedSalad", "Recipe"],
      ["MixedVegetableMedley", "Recipe"],
      ["Mochi", "Food"],
      ["MushroomMedley", "Recipe"],
      ["PhadThai", "Food"],
      ["Pupusas", "Food"],
      ["RainforestFruitSalad", "Recipe"],
      ["RainforestSalad", "Recipe"],
      ["RiceNoodles", "Food"],
      ["SharkFilletSoup", "Food"],
      ["SimmeredMeat", "Food"],
      ["SmoothGutNoodleRoll", "Food"],
      ["TaroFries", "Food"],
      ["VegetableSoup", "Food"],
    ].map((recipe) => {
      return {
        description: "Add baking paper as a static input",
        file: path.join("Autogen", recipe[1], recipe[0]),
        changes: [
          {
            type: "RecipeInput",
            ingredient: "typeof(WrappingPaperItem)",
            oldAmount: 0,
            newAmount: 1,
            staticness: "static",
          },
        ]
      }
    })
  },
  // Adv. Cooking
  {
    description: "Add wrapping paper to all advanced cooking foods",
    changes: [
      "AgoutiEnchiladas",
      "BanhXeo",
      "BearSUPREME",
      "BisonChowFun",
      "BoiledRice",
      "BoiledRice",
      "BoiledSausage",
      "CornFritters",
      "CrimsonSalad",
      "ElkTaco",
      "FishNChips",
      "FriedHareHaunches",
      "Hosomaki",
      "KelpyCrabRoll",
      "MillionairesSalad",
      "PineappleFriendRice",
      "PokeBowl",
      "SearedMeat",
      "SeededCamasRoll",
      "SpikyRoll",
      "SweetSalad",
      "Tortilla",
      "WildMix",
    ].map((filename) => {
      return {
        description: "Add baking paper as a static input",
        file: path.join("Autogen", "Food", filename),
        changes: [
          {
            type: "RecipeInput",
            ingredient: "typeof(WrappingPaperItem)",
            oldAmount: 0,
            newAmount: 1,
            staticness: "static",
          },
        ]
      }
    })
  },
  // Baking
  {
    description: "Add baking paper to all baking foods",
    changes: [
      "BakedAgave",
      "BakedBeet",
      "BakedCorn",
      "BakedHeartOfPalm",
      "BakedMeat",
      "BakedRoast",
      "BakedTaro",
      "BakedTomato",
      "Bread",
      "CamasBread",
      "CamasBulbBake",
      "Flatbread",
      "FruitMuffin",
      "HuckleberryFritter",
      "HuckleberryPie",
      "MeatPie",
      "RoastPumpkin",
      "WorldlyDonut",
    ].map((filename) => {
      return {
        description: "Add baking paper as a static input",
        file: path.join("Autogen", "Food", filename),
        changes: [
          {
            type: "RecipeInput",
            ingredient: "typeof(BakingPaperItem)",
            oldAmount: 0,
            newAmount: 1,
            staticness: "static",
          },
        ]
      }
    })
  },
  // Adv. Baking
  {
    description: "Add baking paper to all advanced baking foods",
    changes: [
      "Bearclaw",
      "ElkWellington",
      "FantasticForestPizza",
      "FruitTart",
      "HeartyHometownPizza",
      "Macarons",
      "Pirozhok",
      "SensuousSeaPizza",
      "StuffedTurkey",
      "TastyTropicalPizza",
    ].map((filename) => {
      return {
        description: "Add baking paper as a static input",
        file: path.join("Autogen", "Food", filename),
        changes: [
          {
            type: "RecipeInput",
            ingredient: "typeof(BakingPaperItem)",
            oldAmount: 0,
            newAmount: 1,
            staticness: "static",
          },
        ]
      }
    })
  },
  // Painting
  {
    description: "Move dye creation from milling level 2 to painting level 1 for regular mill recipes",
    changes: [
      ["BluePowder", "Item"],
      ["CharcoalPowder", "Item"],
      ["ColoredPowder", "Item"],
      ["CopperHydroxide", "Item"],
      ["IronOxide", "Item"],
      ["MagentaPowder", "Item"],
      ["PowderedCreosote", "Recipe"],
      ["WhitePowder", "Item"],
      ["YellowPowder", "Item"],
    ].map((recipe) => {
      return {
        description: "Move from milling to painting specialization",
        file: path.join("Autogen", recipe[1], recipe[0]),
        changes: [
          {
            type: "SkillRequirement",
            oldSkill: 2,
            skill: 1,
            skillType: "MillingSkill",
            newSkillType: "PaintingSkill",
          },
        ]
      }
    })
  },
  {
    description: "Move dye creation from milling level 2 to painting level 2 for industrial mill recipes",
    changes: [
      "ProcessedBluePowder",
      "ProcessedCharcoalPowder",
      "ProcessedColoredPowder",
      "ProcessedCopperHydroxide",
      "ProcessedCreosote",
      "ProcessedIronOxide",
      "ProcessedMagentaPowder",
      "ProcessedWhitePowder",
      "ProcessedYellowPowder",
    ].map((filename) => {
      return {
        description: "Move from milling to painting specialization",
        file: path.join("Autogen", "Recipe", filename),
        changes: [
          {
            type: "SkillRequirement",
            oldSkill: 2,
            skill: 2,
            skillType: "MillingSkill",
            newSkillType: "PaintingSkill",
          },
        ]
      }
    })
  },
  {
    description: "Change peat charcoal to be static 1 to 1",
    changes: [
      {
        description: "Change peat charcoal to be static 1 to 1",
        file: path.join("AutoGen", "Recipe", "PeatCharcoal"),
        changes: [
          {
            type: "RecipeInput",
            ingredient: 'typeof(PeatItem)',
            oldAmount: 1,
            newAmount: 1,
            staticness: "static",
            comment: "Make peat input static at 1",
          },
          {
            type: "RecipeOutput",
            ingredient: "CharcoalItem",
            oldAmount: 4,
            newAmount: 1,
            comment: "Reduce Peat Charcoal output to 1",
          },
        ],
      },
    ],
  },
  {
    description: "Switch urchin for orchids in magenta powder",
    changes: [
      {
        description: "Switch urchin for orchids in magenta powder",
        file: path.join("Autogen", "Item", `MagentaPowder`),
        changes: [
          {
            type: "RecipeInput",
            ingredient: 'typeof(UrchinItem)',
            oldAmount: 1,
            newAmount: 0,
            staticness: "dynamic",
            comment: "Remove urchin requirement",
          },
          {
            type: "RecipeInput",
            ingredient: 'typeof(OrchidItem)',
            oldAmount: 0,
            newAmount: 4,
            staticness: "dynamic",
            comment: "Add orchid requirement",
          }
        ]
      }
    ]
  },
  {
    description: "Switch urchin for orchids in processed magenta powder",
    changes: [
      {
        description: "Switch urchin for orchids in processed magenta powder",
        file: path.join("Autogen", "Recipe", `ProcessedMagentaPowder`),
        changes: [
          {
            type: "RecipeInput",
            ingredient: 'typeof(UrchinItem)',
            oldAmount: 2,
            newAmount: 0,
            staticness: "dynamic",
            comment: "Remove urchin requirement",
          },
          {
            type: "RecipeInput",
            ingredient: 'typeof(OrchidItem)',
            oldAmount: 0,
            newAmount: 8,
            staticness: "dynamic",
            comment: "Add orchid requirement",
          }
        ]
      }
    ]
  },
];
