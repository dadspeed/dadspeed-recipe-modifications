import path from "path";
import { ChangeSet } from "../change-implementations";

export const researchPaperChanges: ChangeSet = {
  description: `Change the level requirement of research papers to slow down progress and require more trading.`,
  changes: [
    {
      file: path.join("AutoGen", "Item", "AgricultureResearchPaperAdvanced"),
      changes: [
        {
          type: "RecipeInput",
          comment: "Increase dirt consumption by 5x",
          ingredient: "typeof(DirtItem)",
          oldAmount: 5,
          newAmount: 25,
        },
      ],
    },
    {
      file: path.join("AutoGen", "Item", "AgricultureResearchPaperModern"),
      changes: [
        {
          type: "SkillRequirement",
          skillType: "FertilizersSkill",
          oldSkill: 1,
          skill: 3,
        },
      ],
    },
    {
      file: path.join("AutoGen", "Item", "DendrologyResearchPaperAdvanced"),
      changes: [
        {
          type: "SkillRequirement",
          skillType: "CarpentrySkill",
          oldSkill: 1,
          skill: 2,
        },
      ],
    },
    {
      file: path.join("AutoGen", "Item", "DendrologyResearchPaperModern"),
      changes: [
        {
          type: "SkillRequirement",
          skillType: "CarpentrySkill",
          oldSkill: 1,
          skill: 3,
        },
      ],
    },
    {
      file: path.join("AutoGen", "Item", "EngineeringResearchPaperAdvanced"),
      changes: [
        {
          type: "SkillRequirement",
          skillType: "BasicEngineeringSkill",
          oldSkill: 1,
          skill: 2,
        },
      ],
    },
    {
      file: path.join("AutoGen", "Item", "EngineeringResearchPaperModern"),
      changes: [
        {
          type: "SkillRequirement",
          skillType: "MechanicsSkill",
          oldSkill: 1,
          skill: 3,
        },
      ],
    },
    {
      file: path.join("AutoGen", "Item", "GeologyResearchPaperAdvanced"),
      changes: [
        {
          type: "SkillRequirement",
          skillType: "MasonrySkill",
          oldSkill: 1,
          skill: 2,
        },
      ],
    },
    {
      file: path.join("AutoGen", "Item", "GeologyResearchPaperModern"),
      changes: [
        {
          type: "SkillRequirement",
          skillType: "PotterySkill",
          oldSkill: 1,
          skill: 3,
        },
      ],
    },
    {
      file: path.join("AutoGen", "Recipe", "GeologyResearchPaperModernGlass"),
      changes: [
        {
          type: "SkillRequirement",
          skillType: "GlassworkingSkill",
          oldSkill: 1,
          skill: 3,
        },
      ],
    },
    {
      file: path.join("AutoGen", "Item", "MetallurgyResearchPaperAdvanced"),
      changes: [
        {
          type: "SkillRequirement",
          skillType: "SmeltingSkill",
          oldSkill: 1,
          skill: 2,
        },
      ],
    },
    {
      file: path.join("AutoGen", "Item", "MetallurgyResearchPaperModern"),
      changes: [
        {
          type: "SkillRequirement",
          skillType: "AdvancedSmeltingSkill",
          oldSkill: 1,
          skill: 3,
        },
      ],
    },
  ],
};
