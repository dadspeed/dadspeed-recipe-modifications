﻿import path from "path";
import { ChangeSet } from "../change-implementations";

export const compositesChanges: ChangeSet[] = [
  { 
    description: "Change recipe requirements for specific wood type composites",
    changes: [
      "Birch",
      "Cedar",
      "Ceiba",
      "Fir",
      "Joshua",
      "Oak",
      "Palm",
      "Redwood",
      "Spruce",
    ].map((logType) => {
      return {
        description: `Change recipe requirements for ${logType} composites`,
        file: path.join("Autogen", "Block", `Composite${logType}Lumber`),
        changes: [
          {
            type: "RecipeInput",
            ingredient: `typeof(${logType}LogItem)`,
            oldAmount: 1,
            newAmount: 4,
            staticness: "dynamic",
            comment: "Increase amount of logs required from 1 to 4",
          },
          {
            type: "RecipeInput",
            ingredient: '"WoodBoard"',
            oldAmount: 4,
            newAmount: 0,
            staticness: "static",
            comment: "Reduce Wooden Boards requirement to 0",
          },
          {
            type: "RecipeInput",
            ingredient: '"Lumber"',
            oldAmount: 0,
            newAmount: 3,
            staticness: "dynamic",
            comment: "Add dynamic lumber requirement with base amount 3",
          }
        ]
      }
    })
  },
  {
    description: "Change recipe requirements for Saguaro composites",
    changes: [
      {
        description: "Change recipe requirements for Saguaro composites",
        file: path.join("Autogen", "Block", `CompositeSaguaroLumber`),
        changes: [
          {
            type: "RecipeInput",
            ingredient: 'typeof(SaguaroRibItem)',
            oldAmount: 1,
            newAmount: 4,
            staticness: "dynamic",
            comment: "Increase amount of logs required from 1 to 4",
          },
          {
            type: "RecipeInput",
            ingredient: '"WoodBoard"',
            oldAmount: 4,
            newAmount: 0,
            staticness: "static",
            comment: "Reduce Wooden Boards requirement to 0",
          },
          {
            type: "RecipeInput",
            ingredient: '"Lumber"',
            oldAmount: 0,
            newAmount: 3,
            staticness: "dynamic",
            comment: "Add dynamic lumber requirement with base amount 3",
          }
        ]
      }
    ]
  },
  {
    description: "Change recipe requirements for generic composites",
    changes: [
      {
        description: "Change recipe requirements for generic composites",
        file: path.join("Autogen", "Block", `CompositeLumber`),
        changes: [
          {
            type: "RecipeInput",
            ingredient: '"Wood"',
            oldAmount: 1,
            newAmount: 4,
            staticness: "dynamic",
            comment: "Increase amount of logs required from 1 to 4",
          },
          {
            type: "RecipeInput",
            ingredient: '"WoodBoard"',
            oldAmount: 4,
            newAmount: 0,
            staticness: "static",
            comment: "Reduce Wooden Boards requirement to 0",
          },
          {
            type: "RecipeInput",
            ingredient: '"Lumber"',
            oldAmount: 0,
            newAmount: 3,
            staticness: "dynamic",
            comment: "Add dynamic lumber requirement with base amount 3",
          }
        ]
      }
    ]
  }
]