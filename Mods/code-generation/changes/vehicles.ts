import path from "path";
import { ChangeSet } from "../change-implementations";

export const vehicleChanges: ChangeSet = {
  description: `Increase carrying capacity of vehicles and add speed modifiers`,
  changes: [
    {
      file: path.join("AutoGen", "Vehicle", "PoweredCart"),
      changes: [
        {
          type: "SkillRequirement",
          skillType: "BasicEngineeringSkill",
          oldSkill: 5,
          skill: 1,
        },
        {
          type: "VehicleModification",
          oldStorage: { stacks: 18, weight: 3500 },
          newStorage: { stacks: 24, weight: 4500 },
          fuelConsumptionModifier: 1.5,
          useSpeedModifier: { targetClass: "PoweredCartObject" },
        },
      ],
    },
    {
      file: path.join("AutoGen", "Vehicle", "SteamTruck"),
      changes: [
        {
          type: "VehicleModification",
          oldStorage: { stacks: 24, weight: 5000 },
          newStorage: { stacks: 32, weight: 6500 },
          fuelConsumptionModifier: 1.5,
          useSpeedModifier: { targetClass: "SteamTruckObject" },
        },
      ],
    },
    {
      file: path.join("AutoGen", "Vehicle", "Truck"),
      changes: [
        {
          type: "VehicleModification",
          oldStorage: { stacks: 36, weight: 8000 },
          newStorage: { stacks: 40, weight: 10000 },
          fuelConsumptionModifier: 1.5,
          useSpeedModifier: { targetClass: "TruckObject" },
        },
        // S11 change to add some gold wires to inputs
        {
          type: "RecipeInput",
          ingredient: "typeof(GoldWiringItem)",
          oldAmount: 0,
          newAmount: 4,
          staticness: "static",
          comment: "Add 4 gold wires as static input"
        },
      ],
    },
    {
      file: path.join("AutoGen", "Vehicle", "Excavator"),
      changes: [
        {
          type: "VehicleModification",
          fuelConsumptionModifier: 1.4,
        },
        // S11 change to add some gold wires to inputs
        {
          type: "RecipeInput",
          ingredient: "typeof(GoldWiringItem)",
          oldAmount: 0,
          newAmount: 4,
          staticness: "static",
          comment: "Add 4 gold wires as static input",
        },
      ],
    },
    {
      file: path.join("AutoGen", "Vehicle", "SkidSteer"),
      changes: [
        {
          type: "VehicleModification",
          fuelConsumptionModifier: 1.4,
        },
        // S11 change to add some gold wires to inputs
        {
          type: "RecipeInput",
          ingredient: "typeof(GoldWiringItem)",
          oldAmount: 0,
          newAmount: 4,
          staticness: "static",
          comment: "Add 4 gold wires as static input",
        },
      ],
    },
    {
      file: path.join("AutoGen", "Vehicle", "SteamTractor"),
      changes: [
        {
          type: "VehicleModification",
          speed: { old: 12, new: 15 },
          fuelConsumptionModifier: 1.4,
          useSpeedModifier: { targetClass: "SteamTractorObject" },
        },
      ],
    },
    {
      file: path.join("AutoGen", "Vehicle", "IndustrialBarge"),
      changes: [
        {
          type: "VehicleModification",
          fuelConsumptionModifier: 1.5,
          useSpeedModifier: { targetClass: "IndustrialBargeObject" },
        },
      ],
    },
    {
      file: path.join("AutoGen", "Vehicle", "MediumFishingTrawler"),
      changes: [
        {
          type: "VehicleModification",
          fuelConsumptionModifier: 1.5,
          useSpeedModifier: {
            targetClass: "MediumFishingTrawlerObject",
            fuels: {
              CharcoalItem: 1.2,
              GasolineItem: 1.4,
              BiodieselItem: 1.6,
            },
          },
          fuelTags: ["Burnable Fuel", "Liquid Fuel"],
        },
      ],
    },
    {
      file: path.join("AutoGen", "Vehicle", "WoodenTransportShip"),
      changes: [
        {
          type: "VehicleModification",
          fuelConsumptionModifier: 1.5,
          useSpeedModifier: {
            targetClass: "WoodenTransportShipObject",
            fuels: {
              CharcoalItem: 1.2,
              GasolineItem: 1.4,
              BiodieselItem: 1.6,
            },
          },
          fuelTags: ["Burnable Fuel", "Liquid Fuel"],
        },
        {
          type: "SkillRequirement",
          oldSkill: 2,
          skill: 4,
          skillType: "ShipwrightSkill",
        },
      ],
    },
  ],
};
