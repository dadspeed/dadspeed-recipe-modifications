export interface PowerConsumptionChange {
  type: "ChangePowerConsumption";
  oldConsumption: number;
  newConsumption: number;
}

export function changePowerConsumption(
  input: string,
  change: PowerConsumptionChange
) {
  return input
    .replace(
      new RegExp(
        `^( +)this.GetComponent<PowerConsumptionComponent>\\(\\)\\.Initialize\\(${change.oldConsumption}\\);`,
        "m"
      ),
      `$1/// Change Power Consumption from ${change.oldConsumption} to ${change.newConsumption}\n$1this.GetComponent<PowerConsumptionComponent>().Initialize(${change.newConsumption});`
    )
    .replace(
      new RegExp(
        `((PowerConsumptionTooltip\\(\\) => Localizer\\.Do\\(\\$"Consumes: \\{Text\\.Info\\()${change.oldConsumption}\\)})w`
      ),
      `$2${change.newConsumption}\)}w`
    );
}
