import { addAnnotation } from "./add-annotation";

export type FuelType = "BiodieselItem" | "GasolineItem" | "CharcoalItem";
export interface VehicleModificationChange {
  type: "VehicleModification";
  oldStorage?: { stacks: number; weight: number };
  newStorage?: { stacks: number; weight: number };
  fuelConsumptionModifier?: number;
  speed?: { old: number; new: number };
  useSpeedModifier?: {
    targetClass: string;
    fuels?: { [key in FuelType]?: number };
  };
  fuelTags?: string[];
}
export function changeVehicleModification(
  input: string,
  change: VehicleModificationChange
) {
  let changed;
  if (change.oldStorage && change.newStorage) {
    changed = input.replace(
      new RegExp(
        `^( +)this\\.GetComponent<PublicStorageComponent>\\(\\)\\.Initialize\\(${
          change.oldStorage.stacks
        }, ${change.oldStorage.weight * 1000}\\);`,
        "m"
      ),
      `$1/// Increase Storage from ${change.oldStorage.stacks} stacks and ${
        change.oldStorage.weight
      }kg to ${change.newStorage.stacks} stacks and ${
        change.newStorage.weight
      }kg\n$1this.GetComponent<PublicStorageComponent>().Initialize(${
        change.newStorage.stacks
      }, ${change.newStorage.weight * 1000});`
    );
    if (changed === input) {
      throw new Error(`Couldn't perform ${change.type}:storage`);
    }
    input = changed;
  }

  if (change.fuelConsumptionModifier) {
    changed = input.replace(
      /^( +)this\.GetComponent<FuelConsumptionComponent>\(\).Initialize\((\d+)\);/m,
      (_, indent, fuelConsumption) => {
        return `${indent}/// Increase fuel consumption by ${
          change.fuelConsumptionModifier
        }x\n${indent}this.GetComponent<FuelConsumptionComponent>().Initialize(${
          fuelConsumption * change.fuelConsumptionModifier
        });`;
      }
    );
    if (changed === input) {
      throw new Error(`Couldn't perform ${change.type}:fuelConsumption`);
    }
    input = changed;
  }

  if (change.speed) {
    changed = input.replace(
      new RegExp(
        `^( +)this\\.GetComponent<VehicleComponent>\\(\\)\\.Initialize\\(${change.speed.old},`,
        "m"
      ),
      `$1/// Change speed from ${change.speed.old} to ${change.speed.new}\n$1this\.GetComponent<VehicleComponent>().Initialize(${change.speed.new},`
    );

    if (changed === input) {
      throw new Error(`Couldn't perform ${change.type}:speed`);
    }
    input = changed;
  }

  if (change.useSpeedModifier) {
    changed = addAnnotation(input, {
      annotation: "[RequireComponent(typeof(VehicleModifiersComponent))]",
      targetClass: change.useSpeedModifier.targetClass,
      type: "AddAnnotation",
    });
    if (changed === input) {
      throw new Error(
        `Couldn't perform ${change.type}:useSpeedModifier:AddAnnotation`
      );
    }
    input = changed;

    changed = input.replace(
      /^( +)this\.GetComponent<VehicleComponent>\(\)\.Initialize\(/m,
      "$1/// Change to VehicleModifiersComponent\n$1this.GetComponent<VehicleModifiersComponent>().Initialize("
    );

    if (changed === input) {
      throw new Error(
        `Couldn't perform ${change.type}:useSpeedModifier:ChangeComponent`
      );
    }
    input = changed;

    if (change.useSpeedModifier.fuels) {
      changed = input.replace(
        /^( +)(this\.GetComponent<VehicleModifiersComponent>\(\)\.Initialize.*)$/m,
        `$1$2
$1/// Set fuel speeds
$1this.GetComponent<VehicleModifiersComponent>().FuelSpeeds = new Dictionary<Type, float>
$1{
${Object.entries(change.useSpeedModifier.fuels)
  .map(([k, v]) => `$1    { typeof(${k}), ${v}f },`)
  .join("\n")}
$1};`
      );
      if (changed === input) {
        throw new Error(
          `Couldn't perform ${change.type}:useSpeedModifier:setFuelSpeeds`
        );
      }
      input = changed;
    }

    const match = input.matchAll(/^( +)using .*;$/gm);
    let x: RegExpMatchArray;
    for (x of match) {
    }
    const index = x.index + x[0].length + 1;
    changed = [
      input.slice(0, index),
      `\n${x[1]}using VehicleModifiers;`,
      input.slice(index),
    ].join("");

    input = changed;
  }

  if (change.fuelTags) {
    changed = input.replace(
      /^( +)private static string\[\] fuelTagList = new string\[\]\s+\{[^}]*\};/m,
      `$1/// Update fuelTagList
$1private static string[] fuelTagList = new string[]
$1{
${change.fuelTags.map((fuel) => `$1    "${fuel}",`).join("\n")}
$1};`
    );

    if (changed === input) {
      throw new Error(`Couldn't perform ${change.type}:fuelTags`);
    }
    input = changed;
  }

  return changed;
}
