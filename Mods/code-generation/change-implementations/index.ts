import { AddAnnotationChange } from "./add-annotation";
import { AddUsingChange } from "./add-using";
import { DisplayDescriptionChange } from "./display-description";
import { LaborInCaloriesChange } from "./labor-in-calories";
import { PowerConsumptionChange } from "./power-consumption";
import { RecipeInputChange, RecipeOutputChange } from "./recipes";
import { RemoveFileChange } from "./remove-file";
import { SkillBookToScrollChange } from "./skill-book-to-scroll";
import { SkillRequirementChange } from "./skill-requirement";
import { SetSpoilageChange } from "./spoilage";
import { SetStorageShelfLifeMultiplierChange } from "./storage-shelf-life-multiplier";
import { VehicleModificationChange } from "./vehicle";

export interface CustomChange {
  type: "Custom";
  fn: (input: string) => string;
}

export type ChangeSpec =
  | SkillRequirementChange
  | RecipeInputChange
  | RecipeOutputChange
  | SkillBookToScrollChange
  | VehicleModificationChange
  | AddAnnotationChange
  | DisplayDescriptionChange
  | SetSpoilageChange
  | LaborInCaloriesChange
  | PowerConsumptionChange
  | SetStorageShelfLifeMultiplierChange
  | AddUsingChange
  | RemoveFileChange
  | CustomChange;

export interface FileChangeDefinition {
  file: string;
  description?: string;
  changes: ChangeSpec[];
}
export interface ChangeSet {
  description: string;
  changes: FileChangeDefinition[];
}
