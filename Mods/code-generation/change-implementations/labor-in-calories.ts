export interface LaborInCaloriesChange {
  type: "LaborInCalories";
  oldValue: number;
  newValue: number;
}

export function changeLaborInCalories(
  input: string,
  change: LaborInCaloriesChange
) {
  return input.replace(
    new RegExp(
      `^( +)(this\.LaborInCalories = CreateLaborInCaloriesValue\\()${change.oldValue},`,
      "m"
    ),
    `$1/// Change Calorie consumption from ${change.oldValue} to ${change.newValue}\n$1$2${change.newValue},`
  );
}
