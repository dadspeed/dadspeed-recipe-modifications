export interface DisplayDescriptionChange {
  type: "DisplayDescription";
  oldText: string;
  newText: string;
}
export function changeDisplayDescription(
  input: string,
  change: DisplayDescriptionChange
) {
  return input.replace(
    new RegExp(`^( +\\[LocDescription\\(")${change.oldText}"\\)\\]`, "m"),
    `$1${change.newText}")]`
  );
}
