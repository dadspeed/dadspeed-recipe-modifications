export interface SetStorageShelfLifeMultiplierChange {
  type: "SetStorageShelfLifeMultiplier";
  oldValue: number;
  value: number;
}

export function setStorageShelfLifeMultiplier(
  input: string,
  change: SetStorageShelfLifeMultiplierChange
) {
  let changed = input.replace(
    new RegExp(
      `^( +)storage\\.ShelfLifeMultiplier = ${change.oldValue}f;$`,
      "m"
    ),
    `$1/// Set ShelfLifeMultiplier to ${change.value}\n$1storage.ShelfLifeMultiplier = ${change.value}f;`
  );

  if (changed === input) {
    throw new Error(
      `Couldn't perform ${change.type}:storage.ShelfLifeMultiplier`
    );
  }

  // Set input to changed for the next update
  input = changed;
  changed = input.replace(
    new RegExp(
      `^( +)\\[Tooltip\\(50\\)\\] public TooltipSection UpdateTooltip\\(\\) => new TooltipSection\\(Localizer\\.Do\\(\\$"\\{Localizer\\.DoStr\\("(Increases|Decreases)"\\)\\} total shelf life by: \\{Text\\.InfoLight\\(Text\\.Percent\\(\\d+(\\.\\d+)?f\\)\\)\\}"\\)\\.Dash\\(\\)\\);$`,
      "m"
    ),

    `$1/// Update tooltip\n$1[Tooltip(50)] public TooltipSection UpdateTooltip() => new TooltipSection(Localizer.Do($"{Localizer.DoStr("Increases")} total shelf life by: {Text.InfoLight(Text.Percent(${
      change.value - 1
    }f))}").Dash());`
  );

  if (changed === input) {
    throw new Error(`Couldn't perform ${change.type}:tooltip on ${input}`);
  }

  return changed;
}
