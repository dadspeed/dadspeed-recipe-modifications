export interface SkillBookToScrollChange {
  type: "SkillBookToScroll";
  scrollAmount: number;
}

export function changeSkillBookToScroll(
  input: string,
  change: SkillBookToScrollChange
) {
  return input
    .replace(
      /Localizer\.DoStr\("(.+?) Skill Book"\)/g,
      'Localizer.DoStr("$1 Skill Scroll")'
    )
    .replace(
      /^( +)new CraftingElement<(\w+)SkillBook>\(\)/m,
      `$1/// Replace Skill Book with ${change.scrollAmount} Skill Scrolls\n$1new CraftingElement<$2SkillScroll>(${change.scrollAmount})`
    );
}
