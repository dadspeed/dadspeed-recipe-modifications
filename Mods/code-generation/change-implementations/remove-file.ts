export interface RemoveFileChange {
  type: "RemoveFile";
}

export function removeFile(input: string, change: RemoveFileChange) {
  return "/// Entire file was removed";
}
