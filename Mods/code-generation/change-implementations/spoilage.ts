export interface SetSpoilageChange {
  type: "SetSpoilage";
  value: string;
}
export function setSpoilage(input, change) {
  return input.replace(
    /^( +)(protected override float BaseShelfLife( +)=>) \(float\)TimeUtil\.HoursToSeconds\(\d+\);$/m,
    `$1/// Set Spoilage to ${change.value}\n$1$2 ${change.value};`
  );
}
