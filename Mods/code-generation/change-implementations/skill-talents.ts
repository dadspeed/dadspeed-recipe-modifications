import fs from "fs";
import path from "path";

const sourcePath = path.join(__dirname, "..", "..", "__core__");

const lavishFile = fs
  .readFileSync(
    path.join(sourcePath, "AutoGen", "Benefit", "LavishWorkspace.cs")
  )
  .toString();

export const lavishSkills = Array.from(
  lavishFile.matchAll(/typeof\((\w+)LavishResourcesTalent\)/g)
)
  .map((match) => match[1])
  .sort();

const focusedWorkflowFile = fs
  .readFileSync(
    path.join(sourcePath, "AutoGen", "Benefit", "FocusedWorkflow.cs")
  )
  .toString();

export const focusedWorkflowSkills = Array.from(
  focusedWorkflowFile.matchAll(/typeof\((\w+)FocusedSpeedTalent\)/g)
)
  .map((match) => match[1])
  .sort();

const parallelSpeedFile = fs
  .readFileSync(
    path.join(sourcePath, "AutoGen", "Benefit", "ParallelProcessing.cs")
  )
  .toString();

export const parallelSpeedSkills = Array.from(
  parallelSpeedFile.matchAll(/typeof\((\w+)ParallelSpeedTalent\)/g)
)
  .map((match) => match[1])
  .sort();
