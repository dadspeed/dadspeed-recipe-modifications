export interface AddAnnotationChange {
  type: "AddAnnotation";
  targetClass: string;
  annotation: string;
}

export function addAnnotation(input: string, change: AddAnnotationChange) {
  return input.replace(
    new RegExp(`^( +)(public partial class ${change.targetClass})`, "m"),
    `$1${change.annotation}\n$1$2`
  );
}
