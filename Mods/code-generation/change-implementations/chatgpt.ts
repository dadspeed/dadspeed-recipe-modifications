import path from "path";

export interface RecipeInputChange {
  type: "RecipeInput";
  comment?: string;
  oldAmount: number;
  newAmount: number;
  ingredient: string;
}
export interface RecipeOutputChange {
  type: "RecipeOutput";
  comment?: string;
  oldAmount: number;
  newAmount: number;
  ingredient: string;
}
export interface SkillRequirementChange {
  type: "SkillRequirement";
  comment?: string;
  oldSkill: number;
  skill: number;
  skillType: string;
  newSkillType?: string;
}
export interface AddAnnotationChange {
  type: "AddAnnotation";
  targetClass: string;
  annotation: string;
}
export interface DisplayDescriptionChange {
  type: "DisplayDescription";
  oldText: string;
  newText: string;
}
export interface LaborInCaloriesChange {
  type: "LaborInCalories";
  oldValue: number;
  newValue: number;
}
export interface PowerConsumptionChange {
  type: "ChangePowerConsumption";
  oldConsumption: number;
  newConsumption: number;
}
export interface AddUsingChange {
  type: "AddUsing";
  using: string;
}
export interface RemoveFileChange {
  type: "RemoveFile";
}
export interface CustomChange {
  type: "Custom";
  fn: (input: string) => string;
}

export type ChangeSpec =
  | SkillRequirementChange
  | RecipeInputChange
  | RecipeOutputChange
  | AddAnnotationChange
  | DisplayDescriptionChange
  | LaborInCaloriesChange
  | PowerConsumptionChange
  | AddUsingChange
  | RemoveFileChange
  | CustomChange;

export interface FileChangeDefinition {
  file: string;
  description?: string;
  changes: ChangeSpec[];
}
export interface ChangeSet {
  description: string;
  changes: FileChangeDefinition[];
}
const changeSets: ChangeSet[] = [
  {
    description: `Change the power consumption of lights. Indoor lights are changed to mimic the power consumption of LEDs (around 10W) and outdoor lights are changed to mimic HPS (around 40W).`,
    changes: [
      // Lights
      // 100W HPS is about the same as 40W LED
      // 60W Incandescent is about the same as 10W LED
      {
        file: path.join("AutoGen", "WorldObject", "ElectricWallLamp"),
        changes: [
          {
            type: "ChangePowerConsumption",
            oldConsumption: 60,
            newConsumption: 40,
          },
        ],
      },
    ],
  },
  {
    description: `Change the level requirement of research papers to slow down progress and require more trading.`,
    changes: [
      {
        file: path.join("AutoGen", "Item", "AgricultureResearchPaperAdvanced"),
        changes: [
          {
            type: "RecipeInput",
            comment: "Increase dirt consumption by 5x",
            ingredient: "typeof(DirtItem)",
            oldAmount: 5,
            newAmount: 25,
          },
        ],
      },
    ],
  },
  {
    description: "",
    changes: [
      {
        description: `Increase skill requirement of Boiled Grains to give Campfire Cooking more trade`,
        file: path.join("AutoGen", "Food", "BoiledGrains"),
        changes: [
          {
            type: "SkillRequirement",
            skillType: "CampfireCookingSkill",
            oldSkill: 0,
            skill: 1,
          },
        ],
      },
      {
        description: `Buff Charcoal recipe to increase it's competitiveness with regular coal.`,
        file: path.join("AutoGen", "Item", "Charcoal"),
        changes: [
          {
            type: "SkillRequirement",
            skillType: "LoggingSkill",
            oldSkill: 4,
            skill: 2,
          },
          {
            type: "RecipeInput",
            ingredient: '"Wood"',
            oldAmount: 7,
            newAmount: 6,
          },
          { type: "LaborInCalories", oldValue: 25, newValue: 100 },
          {
            type: "RecipeOutput",
            ingredient: "Charcoal",
            oldAmount: 1,
            newAmount: 2,
          },
          {
            type: "Custom",
            fn: (input) => {
              return input.replace("[Fuel(20000)]", "[Fuel(30000)]");
            },
          },
        ],
      },
      {
        description: `Reduce skill requirement for Quicklime Glass from 4 to 2 to make it easier for new players to enter the market`,
        file: path.join("AutoGen", "Recipe", "QuicklimeGlass"),
        changes: [
          {
            type: "SkillRequirement",
            skillType: "GlassworkingSkill",
            oldSkill: 4,
            skill: 2,
          },
        ],
      },
    ],
  },

  {
    description: `Add CEC level requirement for Ethanol to give a reason to put a star in the skill`,
    changes: [
      {
        file: path.join("AutoGen", "Recipe", "CornEthanol"),
        changes: [
          {
            type: "SkillRequirement",
            skillType: "CuttingEdgeCookingSkill",
            oldSkill: 0,
            skill: 1,
          },
        ],
      },
      {
        file: path.join("AutoGen", "Recipe", "WheatEthanol"),
        changes: [
          {
            type: "SkillRequirement",
            skillType: "CuttingEdgeCookingSkill",
            oldSkill: 0,
            skill: 1,
          },
        ],
      },
    ],
  },

  /** Waste Filter */
  {
    description: `Remove Sewage output from Waste Filter`,
    changes: [
      {
        file: path.join("AutoGen", "WorldObject", "WasteFilter"),
        changes: [
          {
            type: "DisplayDescription",
            oldText: "Treats raw sewage.",
            newText: "Treats raw sewage. Does not produce any Compost.",
          },
        ],
      },
      {
        file: path.join("Objects", "WaterFilter"),
        changes: [
          {
            type: "Custom",
            fn: (input) => {
              return input.replace(
                /^( +)(this\.converter\.OnConvert \+= this\.Converted;)$/m,
                "$1/// Remove OnConvert listener to stop compost from being output\n$1// $2"
              );
            },
          },
        ],
      },
    ],
  },
];
