export interface RecipeInputChange {
  type: "RecipeInput";
  comment?: string;
  oldAmount: number;
  newAmount: number;
  ingredient: string;
  staticness?: "static" | "dynamic";
}

function getStaticChange(input: string, change: RecipeInputChange) {
  const [_, skill] = input.match(
    /\[RequiresSkill\(typeof\((\w+)Skill\), \d+\)\]/
  );
  let staticChange = ",$2";
  if (change.staticness) {
    if (change.staticness === "static") {
      staticChange = `, true`;
    } else {
      const lavish = [
        "AdvancedBaking",
        "AdvancedCooking",
        "AdvancedMasonry",
        "AdvancedSmelting",
        "Baking",
        "BasicEngineering",
        "Blacksmith",
        "Butchery",
        "CampfireCooking",
        "Carpentry",
        "Composites",
        "Cooking",
        "CuttingEdgeCooking",
        "Electronics",
        "Farming",
        "Fertilizers",
        "Glassworking",
        "Industry",
        "Masonry",
        "Mechanics",
        "Milling",
        "OilDrilling",
        "Painting",
        "PaperMilling",
        "Pottery",
        "Shipwright",
        "Smelting",
        "Tailoring",
      ].includes(skill);
      staticChange = `, typeof(${skill}Skill)${
        lavish ? `, typeof(${skill}LavishResourcesTalent)` : ""
      }`;
    }
  }
  return staticChange;
}
export function changeRecipeInput(input: string, change: RecipeInputChange) {
  if (change.oldAmount === 0) {
    return addRecipeInput(input, change);
  }
  if (change.newAmount === 0) {
    return removeRecipeInput(input, change);
  }
  const comment =
    change.comment ??
    (change.oldAmount > change.newAmount
      ? `Reduce ${change.ingredient} consumption from ${change.oldAmount} to ${change.newAmount}`
      : `Increase ${change.ingredient} consumption from ${change.oldAmount} to ${change.newAmount}`);

  const staticChange = getStaticChange(input, change);

  const re = new RegExp(
    `^( +)new IngredientElement\\(${change.ingredient.replace(
      /(\(|\))/g,
      "\\$1"
    )}, ${
      change.oldAmount
    },( ?true| ?typeof\\(\\w+Skill\\)(, ?typeof\\(\\w+Talent\\))?)?\\)`,
    "m"
  );
  return input.replace(
    re,
    `$1/// ${comment}\n$1new IngredientElement(${change.ingredient}, ${change.newAmount}${staticChange})`
  );
}

function addRecipeInput(input: string, change: RecipeInputChange) {
  const comment = change.comment
    ? `/// ${change.comment}`
    : `/// Add ${change.ingredient}`;

  if (!change.staticness) {
    throw new Error("New recipes must specify staticness");
  }

  const staticChange = getStaticChange(input, change);

  const newIngredientElement = `
$1    ${comment}
$1    new IngredientElement(${change.ingredient}, ${change.newAmount}${staticChange}),`;

  return input.replace(
    // Regex to locate the end of the 'ingredients' list and add the new ingredient before the closing brace
    /^( +)(ingredients:\s*new\s+List<IngredientElement>\s*{\s*\n)([\s\S]*?)(\s*})/m,
    `$1$2$3\n${newIngredientElement}$4`
  );
}
function removeRecipeInput(input: string, change: RecipeInputChange) {
  const re = new RegExp(`^( +)new IngredientElement\\(${change.ingredient.replace(/[|\\{}()[\]^$+*?.]/g, '\\$&')}, ${change.oldAmount}.*\r\n`, "m");
  return input.replace(re, "");
}

export interface RecipeOutputChange {
  type: "RecipeOutput";
  comment?: string;
  oldAmount: number;
  newAmount: number;
  ingredient: string;
}

export function changeRecipeOutput(input: string, change: RecipeOutputChange) {
  if (change.oldAmount === 0) {
    return addRecipeOutput(input, change);
  }
  
  if (change.newAmount === 0) {
    return removeRecipeOutput(input, change);
  }
  
  const comment =
    change.comment ?? change.oldAmount > change.newAmount
      ? `Reduce ${change.ingredient} output from ${change.oldAmount} to ${change.newAmount}`
      : `Increase ${change.ingredient} output from ${change.oldAmount} to ${change.newAmount}`;

  return input.replace(
    new RegExp(
      `^( +)new CraftingElement<${change.ingredient}>\\(${
        change.oldAmount === 1 ? "" : change.oldAmount
      }\\)`,
      "m"
    ),
    `$1/// ${comment}\n$1new CraftingElement<${change.ingredient}>(${change.newAmount})`
  );
}

export function addRecipeOutput(input: string, change: RecipeOutputChange) {
  const comment = change.comment
    ? `/// ${change.comment}`
    : `/// Add ${change.ingredient}`;

  const newIngredientElement = `
$1    ${comment}
$1    new CraftingElement<${change.ingredient}>(${change.newAmount}),`;

  return input.replace(
    // Regex to locate the end of the 'items' list and add the new ingredient before the closing brace
    /^( +)(items:\s*new\s+List<CraftingElement>\s*{\s*\n)([\s\S]*?)(\s*})/m,
    `$1$2$3${newIngredientElement}\n$4`
  );
}

export function removeRecipeOutput(input: string, change: RecipeOutputChange) {
  const re = new RegExp(
    `^( +)new CraftingElement<${change.ingredient}>\(\S*\).*,`, 'm',
  );

  return input.replace(re, "");
}
