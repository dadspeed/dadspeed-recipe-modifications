export interface SkillRequirementChange {
  type: "SkillRequirement";
  comment?: string;
  // TODO: rename these to something better
  oldSkill: number;
  skill: number;
  skillType: string;
  newSkillType?: string;
}
import {
  focusedWorkflowSkills,
  lavishSkills,
  parallelSpeedSkills,
} from "./skill-talents";

function replaceTalent(
  input: string,
  oldSkillName: string,
  skillName: string,
  talentName: string,
  skillList: string[]
) {
  const re = new RegExp(`, ?typeof\\(${oldSkillName}${talentName}\\)`, "g");

  if (skillList.includes(skillName)) {
    return input.replace(re, `, typeof(${skillName}${talentName})`);
  } else {
    return input.replace(re, "");
  }
}

export function changeSkillRequirement(
  input: string,
  change: SkillRequirementChange
) {
  if (change.newSkillType && change.newSkillType !== change.skillType) {
    const re = new RegExp(`typeof\\(${change.skillType}\\)`, "g");
    let changed = input.replace(re, `typeof(${change.newSkillType})`);
    if (changed === input) {
      throw new Error("err");
    }
    input = changed;

    const oldSkillName = change.skillType.replace(/Skill$/, "");
    const skillName = change.newSkillType.replace(/Skill$/, "");

    input = replaceTalent(
      input,
      oldSkillName,
      skillName,
      "LavishResourcesTalent",
      lavishSkills
    );
    input = replaceTalent(
      input,
      oldSkillName,
      skillName,
      "FocusedSpeedTalent",
      focusedWorkflowSkills
    );
    input = replaceTalent(
      input,
      oldSkillName,
      skillName,
      "ParallelSpeedTalent",
      parallelSpeedSkills
    );
  }

  // TODO: make a better comment
  const comment =
    change.comment ??
    (change.oldSkill > change.skill
      ? `Reduce Skill requirement from ${change.oldSkill} to ${change.skill}`
      : `Increase Skill requirement from ${change.oldSkill} to ${change.skill}`);
  return input.replace(
    new RegExp(
      `^( +)\\[RequiresSkill\\(typeof\\(${
        change.newSkillType ?? change.skillType
      }\\), ${change.oldSkill}\\)\\]`,
      "m"
    ),
    `$1/// ${comment}\n$1[RequiresSkill(typeof(${
      change.newSkillType ?? change.skillType
    }), ${change.skill})]`
  );
}
