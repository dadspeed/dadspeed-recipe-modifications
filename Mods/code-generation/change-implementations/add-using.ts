export interface AddUsingChange {
  type: "AddUsing";
  using: string;
}

export function addUsing(input: string, change: AddUsingChange) {
  const match = input.matchAll(/^( +)using Eco\..*;$/gm);
  let x: RegExpMatchArray;
  for (x of match) {
  }
  const index = x.index + x[0].length + 1;
  return [
    input.slice(0, index),
    `\n${x[1]}using ${change.using};`,
    input.slice(index),
  ].join("");
}
