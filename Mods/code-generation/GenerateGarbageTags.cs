﻿using Eco.Core.Items;
using Eco.Core.Plugins.Interfaces;
using Eco.Gameplay.Items;
using Eco.Gameplay.Skills;
using Eco.Mods.TechTree;
using Eco.Shared.Localization;
using Eco.Shared.Utils;
using Eco.Stats;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.IO;
using Eco.Gameplay.Objects;
using Eco.Core.Utils;

namespace GarbageTags
{
    public class GenerateGarbageTags : IInitializablePlugin, IModKitPlugin
    {
        static GenerateGarbageTags()
        {
            GarbageTagsFilePath = Path.Combine(Directory.GetCurrentDirectory(), "Mods", "UserCode", "Dadspeed-Mods", "CrushedGarbage", "GarbageTags.cs");
            FindPartialClasses();
        }
        private static string GarbageTagsFilePath { get; set; }
        private static ISet<string> PartialClasses { get; } = new HashSet<string>();
        public static void GenerateTags(string filePath)
        {
            Item[] allItems = Item.AllItemsExceptHidden;
            IEnumerable<ItemDefinition> allItemDefinitions = allItems.Select(item => new ItemDefinition(item.Type));
            IEnumerable<ItemDefinition> relevantItemDefinitions = allItemDefinitions.Where(definition => CanBeTrashed(definition));

            string fileContents = GetTaggedPartialClasses(relevantItemDefinitions);

            File.WriteAllText(filePath, fileContents);
        }
        private static bool CanBeTrashed(ItemDefinition definition)
        {
            return !definition.Tags.Contains("Hidden")
                && !definition.Tags.Contains("Excavatable")
                && !definition.Tags.Contains("Diggable")
                && !definition.InheritedTypes.Contains(typeof(Skill))
                && PartialClasses.Contains(definition.ClassName);
        }
        //Could maybe find two classes with the same name in different namespaces, but Eco may already throw an error on that.
        //This is also relying on there being no extra whitespace in 'public partial [name]'
        private static void FindPartialClasses()
        {
            IEnumerable<string> directories = AllSubdirectories(new string[]
            {
                Path.Combine(Directory.GetCurrentDirectory(), "Mods", "__core__"),
                Path.Combine(Directory.GetCurrentDirectory(), "Mods", "UserCode"),
            });
            foreach (string directory in directories)
            {
                foreach (string filePath in Directory.EnumerateFiles(directory))
                {
                    if (filePath == GarbageTagsFilePath) continue; //the previous version of the garbage tag file might have the class as partial, so skip the file
                    string fileContents = File.ReadAllText(filePath);
                    int searchTermIndex;
                    do
                    {
                        const string searchTerm = "public partial class";
                        searchTermIndex = fileContents.IndexOf(searchTerm);
                        if (searchTermIndex >= 0)
                        {
                            fileContents = fileContents.Remove(0, searchTermIndex + searchTerm.Length + 1);
                            string className = new string(fileContents.TakeWhile(c => c != ' ').ToArray());
                            PartialClasses.Add(className);
                        }
                    }
                    while (searchTermIndex >= 0);
                    
                }
            }
        }
        private static IEnumerable<string> AllSubdirectories(IEnumerable<string> directories)
        {
            foreach(string directory in directories)
            {
                foreach(string subDirectory in AllSubdirectories(directory))
                {
                    yield return subDirectory;
                }
            }
        }

        private static IEnumerable<string> AllSubdirectories(string directory)
        {
            yield return directory;
            foreach (string subDirectory in Directory.GetDirectories(directory))
            {
                foreach(string subSubDirectory in AllSubdirectories(subDirectory))
                {
                    yield return subSubDirectory;
                }
            }
        }
        private static string GetTaggedPartialClasses(IEnumerable<ItemDefinition> definitions)
        {
            Dictionary<Type, string> trashableTags = new Dictionary<Type, string>();
            foreach (ItemDefinition definition in definitions)
            {
                trashableTags.Add(definition.Type, GetTrashableTag(definition));
            }
            IOrderedEnumerable<IGrouping<string, ItemDefinition>> itemsNamespaceGroups =
                from definition in definitions
                orderby definition.Namespace ascending, definition.ClassName ascending
                group definition by definition.Namespace into itemsByNamespace
                orderby itemsByNamespace.Key
                select itemsByNamespace;
            
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendLine("using Eco.Core.Items;");

            foreach (IGrouping<string, ItemDefinition> namespaceGroup in itemsNamespaceGroups)
            {
                bool hasNamespace = !string.IsNullOrEmpty(namespaceGroup.Key);
                if (hasNamespace)
                {
                    stringBuilder.AppendLine($"namespace {namespaceGroup.Key}");
                    stringBuilder.AppendLine($"{{");
                    foreach (ItemDefinition item in namespaceGroup)
                    {
                        stringBuilder.AppendLine($"\t[Tag(\"{trashableTags[item.Type]}\")]");
                        stringBuilder.AppendLine($"\tpublic partial class {item.ClassName} {{}}");
                    }
                    stringBuilder.AppendLine($"}}");
                }
                else
                {
                    foreach (ItemDefinition item in namespaceGroup)
                    {
                        stringBuilder.AppendLine($"[Tag(\"{trashableTags[item.Type]}\")]");
                        stringBuilder.AppendLine($"public partial class {item.ClassName} {{}}");
                    }
                }
            }
            return stringBuilder.ToString();
        }
        private static string GetTrashableTag(ItemDefinition definition)
        {
            float tagWeighting = GetWeighting(definition);
            if (tagWeighting >= 0.5f) return "LargeTrashable";
            if (tagWeighting >= 0.2f) return "MediumTrashable";
            return "SmallTrashable";
        }
        private static float GetWeighting(ItemDefinition definition)
        {
            Type type = definition.Type;
            float weighting = 0;
            IEnumerable<string> tags = definition.Tags;
            string className = definition.ClassName;
            int numBlocksOccupied = definition.NumBlocksOccupied;
            IEnumerable<Type> inheritedTypes = definition.InheritedTypes;
            if (tags.Contains("Constructable")) weighting = 0.25f;
            else if (type == typeof(CementItem)) weighting = 0.25f;
            else if (type == typeof(RivetItem)) weighting = 0.15f;
            else if (type == typeof(StorageChestItem)) weighting = 0.25f;
            else if (type == typeof(WaterwheelItem)) weighting = 0.5f;
            else if (className.Contains("Door")) weighting = className.Contains("Large") ? 0.5f : 0.25f;
            else if (className.Contains("Stockpile") && !className.Contains("Lumber")) weighting = 0.25f;
            else if (className.Contains("Sign")) weighting = 0.25f;
            else if (className.Contains("Skill")) weighting = 0.1f;
            else if (tags.Contains("Seed")) weighting = 0.01f;
            else if (tags.Contains("Food")) weighting = 0.1f;
            else if (tags.Contains("Clothes")) weighting = 0.25f;
            else if (tags.Contains("Research")) weighting = 0.1f;
            else if (tags.Contains("Upgrade")) weighting = 0.1f;
            else if (tags.Contains("Tool")) weighting = 0.25f;
            else if (tags.Contains("Metal")) weighting = 0.25f;
            else if (tags.Contains("Fertilizer")) weighting = 0.15f;
            else if (numBlocksOccupied >= 6) weighting = 0.5f;
            else if (numBlocksOccupied >= 1) weighting = 0.25f;
            else if (inheritedTypes.Contains(typeof(BlockItem))) weighting = 0.25f;
            else if (tags.Contains("Fuel")) weighting = 0.1f;
            else weighting = definition.Weight / 10000.0f;
            return weighting;
        }
        /// <summary>
        /// Get all base classes and interfaces
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private static IEnumerable<Type> GetParentTypes(Type type)
        {
            yield return type;
            if (type.BaseType != null)
            {
                foreach (Type baseType in GetParentTypes(type.BaseType))
                {
                    yield return baseType;
                }
            }
            foreach(Type interfaceType in type.GetInterfaces())
            {
                foreach (Type baseInterfaceType in GetParentTypes(interfaceType))
                {
                    yield return baseInterfaceType;
                }
            }
        }

        public void Initialize(TimedTask timer)
        {
            GenerateTags(GarbageTagsFilePath);
        }

        public string GetStatus() => "";
        public string GetCategory() => "";

        public class ItemDefinition
        {
            public Type Type { get; }

            public ItemDefinition(Type type)
            {
                Type = type;
                ClassName = type.Name;
                Namespace = type.Namespace;
                Weight = Item.Get(type).WeightWithoutModifiers;
                Categories.AddRange(type.GetCustomAttributes<LocCategoryAttribute>(true).Select(categoryAttribute => categoryAttribute.Category));
                InheritedTypes.AddRange(GetParentTypes(type));
                Type worldObjectType = InheritedTypes.FirstOrDefault(t => t.IsGenericType && t.GetGenericTypeDefinition() == typeof(WorldObjectItem<>))?.GetGenericArguments()[0];
                NumBlocksOccupied = worldObjectType != null ? WorldObject.GetOccupancy(worldObjectType).Count : 0;
            }

            public string ClassName { get; }
            public string Namespace { get;  }
            public int Weight { get;  }
            public IEnumerable<string> Tags => TagManager.TypeToTags.TryGetValue(Type, out var tags) ? tags.Select(tag => tag.Name) : Enumerable.Empty<string>();
            public List<string> Categories { get;set; } = new List<string>();
            public ISet<Type> InheritedTypes { get; } = new HashSet<Type>();
            public int NumBlocksOccupied { get; set; }
        }
    }
}
