import fs from "fs";
import path from "path";
import { researchPaperChanges } from "./changes/research-papers";
import { skillBookChanges } from "./changes/skill-books";
import { vehicleChanges } from "./changes/vehicles";
import { lightChanges } from "./changes/lights";
import { removeSpoilage } from "./changes/spoilage";
import { changeSkillRequirement } from "./change-implementations/skill-requirement";
import {
  changeRecipeInput,
  changeRecipeOutput,
} from "./change-implementations/recipes";
import { changeSkillBookToScroll } from "./change-implementations/skill-book-to-scroll";
import { changeVehicleModification } from "./change-implementations/vehicle";
import { addAnnotation } from "./change-implementations/add-annotation";
import { changeDisplayDescription } from "./change-implementations/display-description";
import { setSpoilage } from "./change-implementations/spoilage";
import { changeLaborInCalories } from "./change-implementations/labor-in-calories";
import { changePowerConsumption } from "./change-implementations/power-consumption";
import { setStorageShelfLifeMultiplier } from "./change-implementations/storage-shelf-life-multiplier";
import { ChangeSet } from "./change-implementations/index";
import { addUsing } from "./change-implementations/add-using";
import { removeFile } from "./change-implementations/remove-file";
import { s11Changes } from "./changes/s11";
import { mudpackChanges } from "./changes/mudpack";
import { compositesChanges } from "./changes/composites";

const sourcePath = path.join(__dirname, "..", "__core__");
const destinationPath = path.join(__dirname, "..", "UserCode");

const changeSets: ChangeSet[] = [
  researchPaperChanges,
  lightChanges,
  skillBookChanges,
  vehicleChanges,
  ...s11Changes,
  ...mudpackChanges,
  ...compositesChanges,

  removeSpoilage(sourcePath),

  {
    description: "",
    changes: [
      {
        description: `Increase skill requirement of Boiled Grains to give Campfire Cooking more trade`,
        file: path.join("AutoGen", "Food", "BoiledGrains"),
        changes: [
          {
            type: "SkillRequirement",
            skillType: "CampfireCookingSkill",
            oldSkill: 0,
            skill: 1,
          },
        ],
      },
      {
        description: `Buff Charcoal recipe to increase it's competitiveness with regular coal.`,
        file: path.join("AutoGen", "Item", "Charcoal"),
        changes: [
          {
            type: "SkillRequirement",
            skillType: "LoggingSkill",
            oldSkill: 4,
            skill: 2,
          },
          {
            type: "RecipeInput",
            ingredient: '"Wood"',
            oldAmount: 7,
            newAmount: 6,
          },
          { type: "LaborInCalories", oldValue: 25, newValue: 100 },
          {
            type: "Custom",
            fn: (input) => {
              return input.replace("[Fuel(20000)]", "[Fuel(30000)]");
            },
          },
        ],
      },
      {
        description: `Reduce skill requirement for Quicklime Glass from 4 to 2 to make it easier for new players to enter the market`,
        file: path.join("AutoGen", "Recipe", "QuicklimeGlass"),
        changes: [
          {
            type: "SkillRequirement",
            skillType: "GlassworkingSkill",
            oldSkill: 4,
            skill: 2,
          },
        ],
      },
    ],
  },

  {
    description: `Add CEC level requirement for Ethanol to give a reason to put a star in the skill`,
    changes: [
      {
        file: path.join("AutoGen", "Recipe", "CornEthanol"),
        changes: [
          {
            type: "SkillRequirement",
            skillType: "CuttingEdgeCookingSkill",
            oldSkill: 0,
            skill: 1,
          },
        ],
      },
      {
        file: path.join("AutoGen", "Recipe", "WheatEthanol"),
        changes: [
          {
            type: "SkillRequirement",
            skillType: "CuttingEdgeCookingSkill",
            oldSkill: 0,
            skill: 1,
          },
        ],
      },
    ],
  },

  /** Waste Filter */
  {
    description: `Remove Sewage output from Waste Filter`,
    changes: [
      {
        file: path.join("AutoGen", "WorldObject", "WasteFilter"),
        changes: [
          {
            type: "DisplayDescription",
            oldText: "Treats raw sewage.",
            newText: "Treats raw sewage. Does not produce any Compost.",
          },
        ],
      },
      {
        file: path.join("Objects", "WaterFilter"),
        changes: [
          {
            type: "Custom",
            fn: (input) => {
              return input.replace(
                /^( +)(this\.converter\.OnConvert \+= this\.Converted;)$/m,
                "$1/// Remove OnConvert listener to stop compost from being output\n$1// $2"
              );
            },
          },
        ],
      },
    ],
  },
  /** Inventory carry size for logs */
  {
    description: `Fixes for XPBenefits`,
    changes: [
      {
        file: path.join("Objects", "TreeObject"),
        changes: [
          {
            type: "Custom",
            fn: (input) => {
              return input.replace(
                /^( +)else if \(carried\.Stacks\.First\(\)\.Quantity \+ numItems > resource\.MaxStackSize\) +\{ player\.Error\(Localizer\.Format\("You can't carry \{0:n0\} more \{1:items\} \(\{2\} max\)\.", numItems, resource\.UILink\(numItems != 1 \? LinkConfig\.ShowPlural : 0\), resource\.MaxStackSize\)\); +return; \}/m,
                `$1else
$1{
$1    // Let the carry inventory decide how many logs it can hold, instead of using the default log stack size
$1    int maxStackSize = carried.GetMaxAcceptedVal(resource, carried.Stacks.First().Quantity);
$1    if (carried.Stacks.First().Quantity + numItems > maxStackSize) {
$1      player.Error(Localizer.Format("You can't carry {0:n0} more {1:items} ({2} max).", numItems, resource.UILink(numItems != 1 ? LinkConfig.ShowPlural : 0), maxStackSize));
$1      return;
$1    }
$1}`
              );
            },
          },
        ],
      },
      {
        file: path.join("Tools", "ShovelItem"),
        changes: [
          { type: "AddUsing", using: "Eco.Gameplay.Utils" },
          {
            type: "Custom",
            fn: (input) => {
              return input.replace(
                /( +)if \(this.MaxTake > 0 && carry\.Quantity >= this\.MaxTake\)/m,
                `
$1int maxTake = this.MaxTake;
$1//(All) Big Shovel sets MaxTake to zero, meaning you can keep digging until the block's max stack size, excluding the server multiplier. Leaving MaxTake as 0 will not work with this mod, so this calculates what that limit would normally be
$1if (maxTake <= 0 && target.IsBlock)
$1{
$1    Item blockItem = target.Block().GetItem();
$1    maxTake = ItemAttribute.Get<MaxStackSizeAttribute>(blockItem.Type)?.MaxStackSize ?? 10;
$1}

$1if (XPBenefits.ExtraCarryStackLimitBenefit.ShovelBenefit != null)
$1{
$1    maxTake = (int)(maxTake * (1 + XPBenefits.ExtraCarryStackLimitBenefit.ShovelBenefit.CalculateBenefit(player.User)));
$1}
$1if (carry.Quantity >= maxTake)`
              );
            },
          },
        ],
      },
      {
        file: path.join("Benefits", "SweepingHands"),
        changes: [
          {
            type: "Custom",
            fn: (input) => {
              return input.replace(
                /^( +)var numToTake = Item\.GetMaxStackSize\(itemType\) - 1;/m,
                `
$1var carried = user.Inventory.Carried;
$1var resource = Item.Get(itemType);
$1int maxStackSize = carried.GetMaxAcceptedVal(resource, carried.Stacks.First().Quantity);
$1var numToTake = maxStackSize - 1;`
              );
            },
          },
        ],
      },
    ],
  },
  {
    description:
      "Remove Wheat Mortar and Rice Mortar, people should use Boiled Grains Mortar instead",
    changes: [
      {
        file: path.join("AutoGen", "Recipe", "RiceMortar"),
        changes: [{ type: "RemoveFile" }],
      },
      {
        file: path.join("AutoGen", "Recipe", "WheatMortar"),
        changes: [{ type: "RemoveFile" }],
      },
    ],
  },
];

const touchedFiles: string[] = [];
for (const changeSet of changeSets) {
  for (const { file, changes } of changeSet.changes) {
    const fileDestinationPath = path.join(
      destinationPath,
      `${file}.override.cs`
    );

    let sourceFilePath = path.join(sourcePath, `${file}.cs`);
    if (touchedFiles.includes(file)) {
      sourceFilePath = fileDestinationPath;
    }
    touchedFiles.push(file);

    const baseFile = fs.readFileSync(sourceFilePath).toString();

    let modified = baseFile;
    try {
      for (const change of changes) {
        let changed;
        if (change.type === "SkillRequirement") {
          changed = changeSkillRequirement(modified, change);
        } else if (change.type === "RecipeInput") {
          changed = changeRecipeInput(modified, change);
        } else if (change.type === "RecipeOutput") {
          changed = changeRecipeOutput(modified, change);
        } else if (change.type === "SkillBookToScroll") {
          changed = changeSkillBookToScroll(modified, change);
        } else if (change.type === "VehicleModification") {
          changed = changeVehicleModification(modified, change);
        } else if (change.type === "AddAnnotation") {
          changed = addAnnotation(modified, change);
        } else if (change.type === "DisplayDescription") {
          changed = changeDisplayDescription(modified, change);
        } else if (change.type === "LaborInCalories") {
          changed = changeLaborInCalories(modified, change);
        } else if (change.type === "ChangePowerConsumption") {
          changed = changePowerConsumption(modified, change);
        } else if (change.type === "SetSpoilage") {
          changed = setSpoilage(modified, change);
        } else if (change.type === "SetStorageShelfLifeMultiplier") {
          changed = setStorageShelfLifeMultiplier(modified, change);
        } else if (change.type == "AddUsing") {
          changed = addUsing(modified, change);
        } else if (change.type == "RemoveFile") {
          changed = removeFile(modified, change);
        } else if (change.type === "Custom") {
          changed = change.fn(modified);
        } else {
          throw new Error(`Unsupported change type ${(change as any).type}`);
        }
        if (changed === modified) {
          throw new Error(`Couldn't perform ${change.type} on ${file}`);
        }
        modified = changed;
      }
    } catch (e: Error) {
      throw new Error(`${e.message} on ${file}`);
    }

    if (!fs.existsSync(path.dirname(fileDestinationPath))) {
      fs.mkdirSync(path.dirname(fileDestinationPath), { recursive: true });
    }

    fs.writeFileSync(fileDestinationPath, modified);
  }
}

console.log("List of changes:");
for (const changeSet of changeSets) {
  if (changeSet.description) {
    console.log(`  - ${changeSet.description}`);
  } else {
    for (const change of changeSet.changes) {
      if (change.description) {
        console.log(`  - ${change.description}`);
      }
    }
  }
}
