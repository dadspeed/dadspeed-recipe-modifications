This mod creates some new API endpoints for fetching information about recipes, tags and current prices in shops.

The code is available at https://gitlab.com/dadspeed/recipe-api
