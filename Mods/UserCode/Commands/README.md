# Commands
## User
#### /specialists demographic*, skillName*
###### Description
Shows how many players with the given skill there are in the given demographic. Defaults to Everyone if demographic is not provided. Defaults to all skills if skillName is not provided.
###### Notes
In the query results for all skills, the skill icon will link to the skill, and the skill name will link to a new tooltip for the players with that skill in the demographic. The tooltip will refresh every time it pops up as the player list is not cached