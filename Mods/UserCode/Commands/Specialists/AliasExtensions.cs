﻿using Eco.Gameplay.Aliases;
using Eco.Gameplay.Civics.Demographics;
using Eco.Gameplay.Civics.Titles;
using Eco.Gameplay.Systems.TextLinks;
using Eco.Shared.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commands
{
    internal static class AliasExtensions
    {
        public static LocString GetUILinkOrGeneric(this IAlias alias)
        {
            if (alias is Demographic demographic)
            {
                return demographic.UILink();
            }
            if (alias is Title title)
            {
                return title.UILink();
            }
            return alias.UILinkGeneric();
        }
    }
}
