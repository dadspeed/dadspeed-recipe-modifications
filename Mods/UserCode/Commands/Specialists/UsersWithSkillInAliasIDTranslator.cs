﻿using Eco.Core.Systems;
using Eco.Gameplay.Civics.Demographics;
using Eco.Gameplay.Civics.Titles;
using Eco.Gameplay.Settlements;
using Eco.Gameplay.Skills;
using Eco.Gameplay.Systems.TextLinks;
using Eco.Shared.Utils;
using System;
using System.Linq;

namespace Commands
{
    /// <summary>
    /// Allow tooltips, chat etc to reference the tooltip popup object with a string identifier
    /// </summary>
    internal class UsersWithSkillInAliasIDTranslator : ObjectLinkIdTranslator<UsersWithSkillTooltip>
    {
        protected override UsersWithSkillTooltip GetTypedLinkTarget(string linkId)
        {
            //Try to recreate the tooltip for previous searches still in chat after a server restart
            try
            {
                string[] linkParams = linkId.Split(':');
                string skillName = linkParams[0];
                Demographic demographic = int.TryParse(linkParams[1], out int demographicID) ? Registrars.All<Demographic>().FirstOrDefault(demographic => demographic.Id == demographicID) : null;
                Title title = int.TryParse(linkParams[2], out int titleID) ? Registrars.All<Title>().FirstOrDefault(title => title.Id == titleID) : null;
                Settlement settlement = int.TryParse(linkParams[3], out int settlementId) ? Registrars.All<Settlement>().FirstOrDefault(settlement => settlement.Id == settlementId) : null;

                Skill skill = Skill.AllSkills.FirstOrDefault(s => s.Name == skillName);
                if (skill == null) return null;
                UsersWithSkillTooltip newTooltip = UsersWithSkillTooltip.GetOrAdd(skill, demographic, title, settlement);

                return newTooltip;
            }
            catch
            {
                return null;
            }
        }

        protected string GetTypedLinkId(string skillName, int? demographicID, int? titleID, int? settlementId)
        {
            return $"{skillName}:{demographicID}:{titleID}:{settlementId}";
        }

        protected override string GetTypedLinkId(UsersWithSkillTooltip linkTarget)
        {
            //Generate a link id for the tooltip
            string linkId = GetTypedLinkId(linkTarget.Skill?.Name, linkTarget.Demographic?.Id, linkTarget.Title?.Id, linkTarget.Settlement?.Id);
            return linkId;
        }
    }
}