﻿using Eco.Core.Plugins.Interfaces;
using Eco.Core.Utils;
using Eco.Gameplay.Skills;
using Eco.Shared.Localization;
using Eco.Shared.Utils;
using System.Collections.Generic;
using System.Linq;

namespace Commands
{
    internal static class ParsingUtility
    {
        private static string RemoveLeadingSpaces(this string str)
        {
            str = new string(str.SkipWhile(c => c == ' ').ToArray());
            return str;
        }

        /// <summary>
        /// Find the skill with the given skill name, or the first skill that starts with the partial name (case insensitive)
        /// </summary>
        public static Skill ParseSkill(string skillName, SupportedLanguage language = SupportedLanguage.English)
        {
            if (skillName == null)
            {
                return null;
            }
            skillName = skillName.RemoveLeadingSpaces();
            //Don't think skills or translations will be null, but to be on the safe side exclude any which are
            IEnumerable<Skill> allSkills = Skill.AllSkills.Where(skill => skill != null && Localizer.LocalizeString(skill.DisplayName.NotTranslated, language) != null);
            //Find skills which match the name exactly. Prefer non-root skills, then alphabetical
            IOrderedEnumerable<Skill> fullyMatchedSkills = from skl in allSkills
                                                      where Localizer.LocalizeString(skl.DisplayName.NotTranslated, language).ToLower() == skillName.ToLower()
                                                      orderby !skl.IsRoot ? 0 : 1 ascending, skl.Name ascending
                                                      select skl;

            Skill skill = fullyMatchedSkills.FirstOrDefault();
            if (skill == null)
            {
                //Find skills whose name starts with the query. Prefer non-root skills, then alphabetical
                IOrderedEnumerable<Skill> partiallyMatchedSkills = from skl in allSkills
                                                                 where Localizer.LocalizeString(skl.DisplayName.NotTranslated, language).ToLower().StartsWith(skillName.ToLower())
                                                                 orderby !skl.IsRoot ? 0 : 1 ascending, skl.Name ascending
                                                                 select skl;
                skill = partiallyMatchedSkills.FirstOrDefault();
            }
            return skill;
        }
    }
}