﻿using Eco.Gameplay.Civics.Demographics;
using Eco.Gameplay.Civics.Titles;
using Eco.Gameplay.Items;
using Eco.Gameplay.Players;
using Eco.Gameplay.Settlements;
using Eco.Gameplay.Skills;
using Eco.Gameplay.Systems.TextLinks;
using Eco.Shared.Localization;
using Eco.Shared.Utils;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Commands
{
    internal static class SpecialistsSearch
    {
        public static IEnumerable<User> FindUsers(Skill skill, Demographic demographic, Title title, Settlement settlement)
        {
            return from user in UserManager.Users
                   where skill != null && user.IsSpecialist(skill.Type)
                   where demographic == null || demographic.ContainsUser(user)
                   where title == null || title.ContainsUser(user)
                   where settlement == null || settlement.HasCitizen(user)
                   select user;
        }
        public static LocString DisplayAllSkills(Demographic demographic, Title title, Settlement settlement)
        {
            LocStringBuilder stringBuilder = new LocStringBuilder();
            //Ignore root skills such as 'Carpenter' or 'Survivalist', and display them alphabetically
            IOrderedEnumerable<Skill> allSkills = Skill.AllSkills.Where(s => !s.IsRoot).OrderBy(s => s.Name);
            List<(Skill, User[], int)> takenSkills = new List<(Skill, User[], int)>();
            List<Skill> emptySkills = new List<Skill>();

            foreach (Skill skill in allSkills)
            {
                User[] users = FindUsers(skill, demographic, title, settlement).ToArray();

                int numberOfUsersWithSkill = users.Count();
                if (numberOfUsersWithSkill >= 1)
                {
                    takenSkills.Add((skill, users, numberOfUsersWithSkill));
                }
                else
                {
                    emptySkills.Add(skill);
                }
            }

            stringBuilder.AppendLine(SpecialistsTooltipLibrary.SkillsAliasCitizenship(demographic, title, settlement));
            
            foreach ((Skill skill, User[] usersWithSkill, int numberOfUsersWithSkill) in takenSkills)
            {
                LocString iconToSkillTooltip = skill.UILink(new LocString(Text.Icon(skill.Name)));
                LocString skillNameToUsersTooltip = UsersWithSkillTooltip.GetOrAdd(skill, demographic, title, settlement).UILink(skill.DisplayName.Style(Text.Styles.Item));
                stringBuilder.AppendLine(iconToSkillTooltip + skillNameToUsersTooltip + ": " + numberOfUsersWithSkill.ToString());
            }
            if (emptySkills.Count > 0)
            {
                stringBuilder.AppendLine();
                stringBuilder.AppendLine(SpecialistsTooltipLibrary.NoSkillsAliasCitizenship(demographic, title, settlement));

                foreach (Skill skill in emptySkills)
                {
                    LocString iconToSkillTooltip = skill.UILink(new LocString(Text.Icon(skill.Name)));
                    LocString skillNameToUsersTooltip = UsersWithSkillTooltip.GetOrAdd(skill, demographic, title, settlement).UILink(skill.DisplayName.Style(Text.Styles.Item));
                    stringBuilder.AppendLine(iconToSkillTooltip + skillNameToUsersTooltip);
                }
            }
            return stringBuilder.ToLocString();
        }

        public static LocString DisplaySkill(Skill skill, Demographic demographic, Title title, Settlement settlement = null)
        {
            LocStringBuilder stringBuilder = new LocStringBuilder();
            IEnumerable<User> usersWithSkill = FindUsers(skill, demographic, title, settlement);

            stringBuilder.AppendLine(SpecialistsTooltipLibrary.UsersByLevelInAlias(skill, demographic, title, usersWithSkill, settlement));
            return stringBuilder.ToLocString();
        }
    }
}