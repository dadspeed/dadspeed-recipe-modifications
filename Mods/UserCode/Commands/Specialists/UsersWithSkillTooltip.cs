﻿using Eco.Core.Controller;
using Eco.Core.Systems;
using Eco.Gameplay.Civics.Demographics;
using Eco.Gameplay.Players;
using Eco.Gameplay.Settlements;
using Eco.Gameplay.Skills;
using Eco.Gameplay.Systems.NewTooltip;
using Eco.Gameplay.Systems.TextLinks;
using Eco.Shared.Items;
using Eco.Shared.Localization;
using Eco.Shared.Utils;
using Eco.Shared.View;
using System;
using System.Linq;
using System.Collections.Generic;
using Eco.Gameplay.Civics.Titles;
using Eco.Shared.IoC;

namespace Commands
{
    /// <summary>
    /// The tooltip box that pops up to list the players with a certain skill, demographic, title and citizenship
    /// </summary>
    internal class UsersWithSkillTooltip : IController, IViewController, IHasUniversalID, ILinkable
    {
        private static ISet<UsersWithSkillTooltip> ExistingSearches { get; } = new HashSet<UsersWithSkillTooltip>();
        public static UsersWithSkillTooltip GetOrAdd(Skill skill, Demographic demographic, Title title, Settlement settlement)
        {
            lock (ExistingSearches)
            {
                var existingTooltip = ExistingSearches.FirstOrDefault(tooltip => tooltip.Skill?.Type == skill?.Type && tooltip.Demographic?.Id == demographic?.Id && tooltip.Title?.Id == title?.Id && tooltip.Settlement?.Id == settlement?.Id);
                if (existingTooltip != null) return existingTooltip;
                UsersWithSkillTooltip newTooltip = new UsersWithSkillTooltip(skill, demographic, title, settlement);
                ExistingSearches.Add(newTooltip);
                return newTooltip;
            }
        }
        private UsersWithSkillTooltip(Skill skill, Demographic demographic, Title title, Settlement settlement)
        {
            Skill = skill;
            Demographic = demographic;
            Title = title;
            Settlement = settlement;
            Skillset.UserSkillLevelChangedEvent.Add((_, s) => { if (s.Type == Skill.Type) Refresh(); });
            demographic?.UserSetChangedEvent.Add(_ => Refresh());
            title?.Subscribe(nameof(Title.Name), Refresh);
            title?.UserSetChangedEvent.Add(_ => Refresh());
            settlement?.CitizenDemographic?.UserSetChangedEvent.Add(_ => Refresh());
            UsersWithSkill = SpecialistsSearch.FindUsers(Skill, Demographic, Title, Settlement);
        }

        public Skill Skill { get; }
        public Demographic Demographic { get; }
        public Title Title { get; }
        public Settlement Settlement { get; }

        public IEnumerable<User> UsersWithSkill { get; private set; }
        public ref int ControllerID => ref id;
        private int id;

        [NewTooltipTitle(CacheAs.Global)]
        public LocString TooltipTitle => Localizer.DoStr("Specialists").Style(Text.Styles.Title).Underline();

        [NewTooltip(CacheAs.Instance)]
        public LocString UILinkContent()
        {
            if (Skill == null) return TextLoc.Error(Localizer.DoStr("Skill not found"));
            return SpecialistsTooltipLibrary.UsersByLevelInAlias(Skill, Demographic, Title, UsersWithSkill, Settlement);
        }
        /// <summary>
        /// Mark the tooltip dirty so it will be refreshed on the client
        /// </summary>
        private void Refresh()
        {
            UsersWithSkill = SpecialistsSearch.FindUsers(Skill, Demographic, Title, Settlement);
            ServiceHolder<ITooltipSubscriptions>.Obj.MarkTooltipPartDirty(nameof(UILinkContent), typeof(UsersWithSkillTooltip), this);
        }
        public void OnLinkClicked(TooltipOrigin origin, TooltipClickContext clickContext, User user)
        {
        }

        public override bool Equals(object obj)
        {
            return obj is UsersWithSkillTooltip alias &&
                   EqualityComparer<Skill>.Default.Equals(Skill, alias.Skill) &&
                   EqualityComparer<Demographic>.Default.Equals(Demographic, alias.Demographic) &&
                   EqualityComparer<Title>.Default.Equals(Title, alias.Title) &&
                   EqualityComparer<Settlement>.Default.Equals(Settlement, alias.Settlement);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Skill, Demographic, Title, Settlement);
        }
    }
}