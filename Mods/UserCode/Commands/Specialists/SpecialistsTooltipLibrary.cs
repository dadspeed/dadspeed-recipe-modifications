﻿using Eco.Gameplay.Civics.Demographics;
using Eco.Gameplay.Civics.Titles;
using Eco.Gameplay.Players;
using Eco.Gameplay.Settlements;
using Eco.Gameplay.Skills;
using Eco.Gameplay.Systems.NewTooltip;
using Eco.Gameplay.Systems.NewTooltip.TooltipLibraryFiles;
using Eco.Shared.Localization;
using Eco.Shared.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Commands
{
    [TooltipLibrary]
    internal static class SpecialistsTooltipLibrary
    {
        public static void Initialize()
        {
        }

        /// <summary>
        /// A list of the users, with the users shown by skill level
        /// <br>e.g.</br>
        /// <code>
        /// Level 4:<br>
        /// </br>    Player 1<br>
        /// </br>    Player 3<br>
        /// </br>Level 1:<br>
        /// </br>    Player 2
        /// </code>
        /// </summary>
        public static LocString UsersByLevel(this IEnumerable<User> users, Type skillType)
        {
            var usersWithSkill = from user in users
                                 let skills = user.Skillset
                                 where user.IsSpecialist(skillType)
                                 group user by skills.GetSkill(skillType).Level into skillLevel
                                 orderby skillLevel.Key descending
                                 select skillLevel;

            LocStringBuilder stringBuilder = new LocStringBuilder();
            foreach (var usersBySkillLevel in usersWithSkill)
            {
                if (usersBySkillLevel.Any())
                {
                    stringBuilder.AppendLineLocStr("Level " + usersBySkillLevel.Key + ":");
                    stringBuilder.AppendLine(usersBySkillLevel.OrderBy(user => user.Name).Select(user => user.MarkedUpName.Indent()).FoldoutListLoc("player", Eco.Shared.Items.TooltipOrigin.None));
                }
            }
            return stringBuilder.ToLocString();
        }

        /// <summary>
        /// A list of the users from demographic or title with a skill, with the users shown by skill level
        /// <br>e.g.</br>
        /// <code>
        /// There are 3 players with Cooking:<br>
        /// </br>Level 4:<br>
        /// </br>    Player 1<br>
        /// </br>    Player 3<br>
        /// </br>Level 1:<br>
        /// </br>    Player 2
        /// </code>
        /// </summary>
        public static LocString UsersByLevelInAlias(Skill skill, Demographic demographic, Title title, IEnumerable<User> usersWithSkill, Settlement settlement)
        {
            LocStringBuilder stringBuilder = new LocStringBuilder();
            int number = usersWithSkill.Count();
            string endOfLine = number > 0 ? ":" : "";
            FormattableString specialistsString = $"There {PluralizeNum($"{skill.MarkedUpName} specialist", number)}";
            FormattableString demographicString = demographic != null ? $" in the {demographic.MarkedUpName} demographic" : (FormattableString)$"";
            FormattableString titleString = title != null ? $" with the {title.MarkedUpName} title" : (FormattableString)$"";
            FormattableString settlementString = settlement != null ? $" who {IsOrAreAPluralize("citizen", number)} of {settlement.MarkedUpName}" : (FormattableString)$"";
            LocStringBuilder lineBuilder = new LocStringBuilder();
            lineBuilder.Append(specialistsString).Append(titleString).Append(demographicString).Append(settlementString).Append(endOfLine);
            stringBuilder.AppendLine(TextLoc.Bold(lineBuilder.ToLocString()));
            if (number > 0) stringBuilder.AppendLine(usersWithSkill.UsersByLevel(skill.Type));
            
            return stringBuilder.ToLocString();
        }
        private static FormattableString PluralizeNum(string s, int num)
        {
            if (num == 0) return $"are no {Localizer.PluralNoNum(s, num)}";
            else if (num == 1) return $"is {num} {s}";
            else return $"are {num} {Localizer.PluralNoNum(s, num)}";
        }
        private static FormattableString IsOrAreAPluralize(string s, int num)
        {
            if (num == 1) return $"is a {s}";
            else return $"are a {s}";
        }

        public static LocString SkillsAliasCitizenship(Demographic demographic, Title title, Settlement settlement)
        {
            string endOfLine = ":";
            FormattableString specialistsString = $"Skills owned by players";
            FormattableString demographicString = demographic != null ? $" in the {demographic.MarkedUpName} demographic" : (FormattableString)$"";
            FormattableString titleString = title != null ? $" with the {title.MarkedUpName} title" : (FormattableString)$"";
            FormattableString settlementString = settlement != null ? $" who are citizens of {settlement.MarkedUpName}" : (FormattableString)$"";
            LocStringBuilder lineBuilder = new LocStringBuilder();
            lineBuilder.Append(specialistsString).Append(titleString).Append(demographicString).Append(settlementString).Append(endOfLine);

            return TextLoc.Bold(lineBuilder.ToLocString());
        }
        public static LocString NoSkillsAliasCitizenship(Demographic demographic, Title title, Settlement settlement)
        {
            string endOfLine = " has these skills:";
            FormattableString specialistsString = $"Nobody";
            FormattableString demographicString = demographic != null ? $" in the {demographic.MarkedUpName} demographic" : (FormattableString)$"";
            FormattableString titleString = title != null ? $" with the {title.MarkedUpName} title" : (FormattableString)$"";
            FormattableString settlementString = settlement != null ? $" who is a citizen of {settlement.MarkedUpName}" : (FormattableString)$"";
            LocStringBuilder lineBuilder = new LocStringBuilder();
            lineBuilder.Append(specialistsString).Append(titleString).Append(demographicString).Append(settlementString).Append(endOfLine);

            return TextLoc.Bold(lineBuilder.ToLocString());
        }
    }
}