﻿using Eco.Gameplay.Players;
using System;

namespace Commands
{
    internal static class SpecialistsExtensions
    {
        public static bool IsSpecialist(this User user, Type skillType)
        {
            return user.Skillset.HasSkill(skillType) && user.Skillset.GetSkill(skillType).Level >= 1;
        }
    }
}