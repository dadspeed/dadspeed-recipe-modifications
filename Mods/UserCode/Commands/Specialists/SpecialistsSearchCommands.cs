﻿using Eco.Gameplay.Civics.Demographics;
using Eco.Gameplay.Civics.Titles;
using Eco.Gameplay.Players;
using Eco.Gameplay.Settlements;
using Eco.Gameplay.Skills;
using Eco.Gameplay.Systems.Messaging.Chat.Commands;
using Eco.Shared.Localization;
using Eco.Shared.Services;
using Eco.Shared.Utils;
using System.Numerics;

namespace Commands
{
    [ChatCommandHandler]
    public static class SpecialistsSearchCommands
    {
        [ChatCommand("Shows how many players there are with a specific skill, demographic, title and citizenship", ChatAuthorizationLevel.User)]
        public static void Specialists(User caller, string skillName = null, Demographic demographic = null, Title title = null, Settlement settlement = null)
        {
            skillName = skillName?.Trim();
            bool skillIsSpecified = !string.IsNullOrEmpty(skillName);
            if (skillIsSpecified)
            {
                //Search for the name of the skill in the player's language.
                //e.g. if the player is playing in German, they can search 'land' for 'landwirtschaft' (farming)
                SupportedLanguage language = SupportedLanguageUtils.Parse(caller.Language);
                Skill skill = ParsingUtility.ParseSkill(skillName, language);
                //The player could also be searching in English, so check this too
                if (skill == null && language != SupportedLanguage.English)
                {
                    skill = ParsingUtility.ParseSkill(skillName, SupportedLanguage.English);
                }
                if (skill == null)
                {
                    caller.MsgLocStr("Skill not found", NotificationStyle.Error);
                    return;
                }
                LocString message = SpecialistsSearch.DisplaySkill(skill, demographic, title, settlement);
                caller.Msg(message, NotificationStyle.Chat);
            }
            else
            {
                LocString message = SpecialistsSearch.DisplayAllSkills(demographic, title, settlement);
                caller.Msg(message, NotificationStyle.Chat);
            }
        }
    }
}