This is a mod that hides/shows the oil layers.
The oil is hidden on server boot if oil drilling hasn't been researched yet.
The first player to read the scroll triggers the oil layer to be regenerated. The values will be different every time since it's based on a random number generator that the game uses elsewhere, so its state cannot be predicted.