﻿using Eco.Shared.Math;
using Eco.Shared.Utils;
using Eco.Simulation.WorldLayers;
using Eco.Simulation.WorldLayers.Layers;
using Eco.WorldGenerator;
using System.Linq;

namespace HiddenOil
{
    public static class OilMap
    {
        public static WorldLayer OilfieldWorldLayer { get => WorldLayerManager.Obj.GetLayer("Oilfield"); }
        public static bool IsOilHidden() => !OilfieldWorldLayer.Map.Any(oilValue => oilValue > float.Epsilon);
        public static void RegenerateOilLayer()
        {
            OilfieldWorldLayer.NoiseInit(5f * WorldGeneratorPlugin.Settings.FrequencyScale);
        }
        public static void WipeOilLayer()
        {
            for (int x = 0; x < OilfieldWorldLayer.Width; x++)
            {
                for (int z = 0; z < OilfieldWorldLayer.Height; z++)
                {
                    Vector2i worldPos = OilfieldWorldLayer.LayerPosToWorldPos(new Vector2i(x, z));
                    OilfieldWorldLayer.SetAtWorldPos(worldPos, 0f);
                }
            }
            OilfieldWorldLayer.DoTick();
        }
    }
}