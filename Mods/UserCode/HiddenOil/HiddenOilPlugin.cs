﻿using Eco.Core.Plugins.Interfaces;
using Eco.Core.Utils;
using Eco.Gameplay.Items;
using Eco.Gameplay.Skills;
using Eco.Mods.TechTree;
using Eco.Gameplay.Systems.Messaging.Chat.Commands;
using Eco.Gameplay.Players;
using Eco.Shared.Services;

namespace HiddenOil
{
    public class HiddenOilPlugin : IInitializablePlugin, IModKitPlugin
    {
        public string GetCategory() => "Mods";
        public string GetStatus() => "Active";
        public void Initialize(TimedTask timer)
        {
        //     bool isOilHidden = OilMap.IsOilHidden();
        //     OilDrillingSkill oilDrillingSkill = Item.Get<OilDrillingSkill>();
        //     bool isOilDrillingUnlocked = oilDrillingSkill.IsDiscovered();
        //     if (!isOilDrillingUnlocked && !isOilHidden) OilMap.WipeOilLayer();
        //     else if (isOilDrillingUnlocked && isOilHidden) OilMap.RegenerateOilLayer();
        //     SkillTree.DiscoveredEvent.Add(skillTree =>
        //     {
        //         if (skillTree == oilDrillingSkill.SpecialtySkillTree && OilMap.IsOilHidden())
        //         {
        //             OilMap.RegenerateOilLayer();
        //         }
        //     });
        }
    }

    [ChatCommandHandler]
    public class HiddenOilCommands
    {
        /// <summary>
        /// Chat command to regenerate the oil layer
        /// </summary>
        [ChatCommand("Regenerates the Oil Layer", ChatAuthorizationLevel.Admin)]
        public static void OilLayerRegenerate(User caller, string confirm = null)
        {
            if (confirm != null && confirm == "confirm")
            {
                OilMap.RegenerateOilLayer();
                caller.MsgLocStr("Oil layer regenerated.", NotificationStyle.Chat);
            }
            else
            {
                caller.MsgLocStr("Do you really want to regenerate the oil layer?\n Usage: /oillayerregenerate confirm", NotificationStyle.Error);
            }
        }
        /// <summary>
        /// Chat command to wipe the oil layer
        /// </summary>
        [ChatCommand("Wipes the Oil Layer", ChatAuthorizationLevel.Admin)]
        public static void OilLayerWipe(User caller, string confirm = null)
        {
            if (confirm != null && confirm == "confirm")
            {
                OilMap.WipeOilLayer();
                caller.MsgLocStr("Oil layer wiped.", NotificationStyle.Chat);
            }
            else
            {
                caller.MsgLocStr("Do you really want to wipe the oil layer?\n Usage: /oillayerwipe confirm", NotificationStyle.Error);
            }
        }
    }
}
