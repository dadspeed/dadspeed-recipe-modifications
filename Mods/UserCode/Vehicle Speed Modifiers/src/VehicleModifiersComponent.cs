﻿using Eco.Core.Controller;
using Eco.Gameplay.Components;
using Eco.Gameplay.Objects;
using Eco.Gameplay.Players;
using Eco.Mods.TechTree;
using Eco.Shared.Serialization;
using Eco.Shared.Logging;
using Eco.Shared.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using Eco.Gameplay.Components.Storage;

namespace VehicleModifiers
{
    [NoIcon]
    [RequireComponent(typeof(VehicleComponent), null)]
    [RequireComponent(typeof(FuelSupplyComponent), null)]
    [RequireComponent(typeof(FuelConsumptionComponent), null)]
    [Serialized]
    public class VehicleModifiersComponent : WorldObjectComponent
    {
        public Dictionary<Type, float>? FuelSpeeds { get; set; }

        private float RegularMaxSpeed { get; set; }
        private float EfficiencyMultiplier { get; set; }
        private int Seats { get; set; }
        private VehicleComponent Vehicle { get; set; }
        private FuelSupplyComponent FuelSupply { get; set; }
        private FuelConsumptionComponent FuelConsumption { get; set; }
        private float? RegularJoulesPerSecond { get; set; }

        private float PreviousSpeedMultiplier { get; set; }

        public void Initialize(float maxSpeed, float efficiencyMultiplier, int seats = 1, string controlHints = null, bool isDrivenUnderwater = false)
        {
            PreviousSpeedMultiplier = 1f;
            RegularMaxSpeed = maxSpeed;
            EfficiencyMultiplier = efficiencyMultiplier;
            Seats = seats;
            Vehicle = Parent.GetComponent<VehicleComponent>();
            FuelSupply = Parent.GetComponent<FuelSupplyComponent>();
            FuelConsumption = Parent.GetComponent<FuelConsumptionComponent>();

            Vehicle.Initialize(maxSpeed, efficiencyMultiplier, seats, controlHints, isDrivenUnderwater);
            FuelSupply.Inventory.OnChanged.Add(OnInventoryChanged);
            UpdateSpeedAndFuelConsumption();
        }

        private void OnInventoryChanged(User _user)
        {
            UpdateSpeedAndFuelConsumption();
        }

        private void UpdateSpeedAndFuelConsumption()
        {
            try
            {
                float speedMultiplier = GetSpeedMultiplier();
                if (PreviousSpeedMultiplier != speedMultiplier)
                {
                    PreviousSpeedMultiplier = speedMultiplier;

                    RegularJoulesPerSecond ??= FuelConsumption.JoulesPerSecond;

                    // Store the mounted players so we can mount them again after re-initializing the vehicle
                    int[] playerIDs = Vehicle.Mounts.OccupantIDs;
                    List<(Player, int)> mounts = Vehicle.Mounts.MountedPlayers.Select((player) => (player, Array.IndexOf(playerIDs, player.ID))).ToList();

                    Log.WriteLine(Localizer.DoStr("Setting vehicle speed to: " + RegularMaxSpeed * speedMultiplier));
                    Vehicle.Initialize(RegularMaxSpeed * speedMultiplier, EfficiencyMultiplier, Seats, Vehicle.ControlHints, Vehicle.IsDrivenUnderwater);

                    foreach ((Player player, int seatIndex) in mounts)
                    {
                        Vehicle.Mounts.MountSeat(seatIndex, player);
                    }
                    FuelConsumption.JoulesPerSecond = RegularJoulesPerSecond.Value * speedMultiplier;
                }
            }
            finally { }
        }


        private float GetSpeedMultiplier()
        {
            Type fuelType = FuelSupply.CurrentFuel?.GetType();
            if (fuelType != null && FuelSpeeds != null && FuelSpeeds.ContainsKey(fuelType))
            {
                return FuelSpeeds[fuelType];
            }
            else if (FuelSupply.CurrentFuel is BiodieselItem biodiesel)
            {
                return biodiesel.VehicleSpeedMultiplier;
            }
            else if (FuelSupply.CurrentFuel is CharcoalItem charcoal)
            {
                return charcoal.VehicleSpeedMultiplier;
            }
            else
            {
                return 1f;
            }
        }
    }
}