Increases the top speed for vehicles using biofuel by around 25%, and the fuel consumption rate by the same amount.

## Tests
I did some testing and here are some results:

### First test

So I made a 300 block long test track and ran some tests.

The times are from the moment I press the gas pedal, with the back of the vehicle against the back wall, to when I hit the wall on the other side (so actually the distance is slightly less than 300 blocks).

Steam Truck:
Coal: 35s, ~8.5 blocks/s
Charcoal: 29s, ~10.3 blocks/s
Speed increase: ~20.7%

Truck:
Gasoline: 30s, 10 blocks/s
Biodiesel: 26s, ~11.5 blocks/s
Speed increase: ~15.4%

Powered Cart:
Coal: 56s, ~5.4 blocks/s
Charcoal: 46s, ~6.5 blocks/s
Speed increase: ~21.7%

Steam Tractor:
Coal: 52s, ~5.8 blocks/s
Charcoal: 43s, ~7.0 blocks/s
Speed increase: ~20.9%

### Second test
I deviced a new test where I let the trucks get up to speed first by only starting the timer after I passed a line around halfway

Truck:
Gasoline: 16.6s
Biodiesel: 13.3s
Speed increase: ~24.8%

Steam Truck:
Coal: 17.7s
Charcoal: 14.3s
Speed increase: ~23.8%

Which seems to suggest that the actual top speed increase is close enough to 25%, but that the acceleration time is what confounded the previous tests 