
namespace Eco.Mods.Organisms
{

    public partial class Birch
    {
        public partial class BirchSpecies
        {
            partial void ModsPostInitialize()
            {
                this.ReleasesCO2TonsPerDay *= 2;
            }
        }
    }

    public partial class Cedar
    {
        public partial class CedarSpecies
        {
            partial void ModsPostInitialize()
            {
                this.ReleasesCO2TonsPerDay *= 2;
            }
        }
    }

    public partial class Ceiba
    {
        public partial class CeibaSpecies
        {
            partial void ModsPostInitialize()
            {
                this.ReleasesCO2TonsPerDay *= 2;
            }
        }
    }

    public partial class Fir
    {
        public partial class FirSpecies
        {
            partial void ModsPostInitialize()
            {
                this.ReleasesCO2TonsPerDay *= 2;
            }
        }
    }

    public partial class Joshua
    {
        public partial class JoshuaSpecies
        {
            partial void ModsPostInitialize()
            {
                this.ReleasesCO2TonsPerDay *= 2;
            }
        }
    }

    public partial class Oak
    {
        public partial class OakSpecies
        {
            partial void ModsPostInitialize()
            {
                this.ReleasesCO2TonsPerDay *= 2;
            }
        }
    }

    public partial class OldGrowthRedwood
    {
        public partial class OldGrowthRedwoodSpecies
        {
            partial void ModsPostInitialize()
            {
                this.ReleasesCO2TonsPerDay *= 2;
            }
        }
    }

    public partial class Palm
    {
        public partial class PalmSpecies
        {
            partial void ModsPostInitialize()
            {
                this.ReleasesCO2TonsPerDay *= 2;
            }
        }
    }

    public partial class Redwood
    {
        public partial class RedwoodSpecies
        {
            partial void ModsPostInitialize()
            {
                this.ReleasesCO2TonsPerDay *= 2;
            }
        }
    }

    public partial class SaguaroCactus
    {
        public partial class SaguaroCactusSpecies
        {
            partial void ModsPostInitialize()
            {
                this.ReleasesCO2TonsPerDay *= 2;
            }
        }
    }

    public partial class Spruce
    {
        public partial class SpruceSpecies
        {
            partial void ModsPostInitialize()
            {
                this.ReleasesCO2TonsPerDay *= 2;
            }
        }
    }
}
