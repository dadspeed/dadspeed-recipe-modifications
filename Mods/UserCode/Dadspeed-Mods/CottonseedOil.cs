namespace Eco.Mods.TechTree
{
    using System.Collections.Generic;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Items.Recipes;
    public partial class CottonseedOilRecipe
    {
        partial void ModsPreInitialize()
        {
            /// Replace oil with flax seed oil
            Recipes[0].ReplaceOutput(
                typeof(OilItem),
                new CraftingElement<FlaxseedOilItem>(1)
            );
        }
    }
}