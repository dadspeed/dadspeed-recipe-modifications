﻿using Eco.Gameplay.Items;
using Eco.Gameplay.Systems.NewTooltip;
using Eco.Gameplay.Systems.TextLinks;
using Eco.Shared.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Eco.Mods.TechTree
{
    public partial class OilDrillingFocusedWorkflowTalentGroup
    {
        [NewTooltip(Shared.Items.CacheAs.Global, 1)]
        public LocString PumpJackDisclaimer => Localizer.DoStr("Does not affect " + Item.Get<PumpJackItem>().UILinkPlural()).Italic().Bold();
    }
}
