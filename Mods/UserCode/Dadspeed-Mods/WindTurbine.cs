﻿namespace Eco.Mods.TechTree
{
    using System.Collections.Generic;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.Objects;
    using Eco.Gameplay.Items;
    using Eco.Shared.Localization;
    using Eco.Gameplay.Items.Recipes;

    public partial class WindTurbineRecipe
    {
        partial void ModsPreInitialize()
        {
            /// Make recipe a lot more expensive
            /// Remove all bonuses from Upgrades/Skill Level
            this.Recipes[0].Ingredients.Clear();
            this.Recipes[0].Ingredients.AddRange(new List<IngredientElement> {
               new IngredientElement(typeof(SteelPlateItem), 25, true),
               new IngredientElement(typeof(SteelGearboxItem), 5, true),
               new IngredientElement(typeof(AdvancedCircuitItem), 20, true),
                /// Add Nylon Fabric to make it more expensive and give some extra business to Tailoring
               new IngredientElement(typeof(NylonFabricItem), 10, true),
                /// Add Epoxy to make it more expensive and give some extra business to Oil Drilling 
               new IngredientElement(typeof(EpoxyItem), 8, true),
               /// Add Fiberglass and Concrete for the pad it's placed on
               new IngredientElement(typeof(FiberglassItem), 6, true),
               new IngredientElement(typeof(ReinforcedConcreteItem), 8, true),
            });

            /// Increase Exp by 2x to compensate slightly for increased costs
            this.ExperienceOnCraft *= 2;

            /// Increase calories 6x
            this.LaborInCalories = CreateLaborInCaloriesValue(7200, typeof(ElectronicsSkill));

            /// Increase Craft Time 2x
            this.CraftMinutes = CreateCraftTimeValue(beneficiary: typeof(WindTurbineRecipe), start: 40, skillType: typeof(ElectronicsSkill), typeof(ElectronicsFocusedSpeedTalent), typeof(ElectronicsParallelSpeedTalent));
        }
    }
}
