namespace Eco.Mods.TechTree
{
    using System;
    using System.Collections.Generic;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.DynamicValues;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Players;
    using Eco.Gameplay.Skills;
    using Eco.Shared.Utils;
    using Eco.World;
    using Eco.World.Blocks;
    using Gameplay.Systems.TextLinks;
    using Eco.Shared.Localization;
    using Eco.Gameplay.Items.Recipes;

    [RequiresSkill(typeof(FertilizersSkill), 4)]
    public partial class CompostClayRecipe : RecipeFamily
    {
        public CompostClayRecipe()
        {
            var recipe = new Recipe();
            recipe.Init(
                "CompostClay",  //noloc
                Localizer.DoStr("Compost Clay"),
                new List<IngredientElement>
                {
                    new IngredientElement(typeof(CrushedShaleItem), 1, typeof(FertilizersSkill), typeof(FertilizersLavishResourcesTalent)),
                    new IngredientElement(typeof(CompostItem), 2, typeof(FertilizersSkill), typeof(FertilizersLavishResourcesTalent)),
                    new IngredientElement(typeof(DirtItem), 1, typeof(FertilizersSkill), typeof(FertilizersLavishResourcesTalent)),
                },
                new List<CraftingElement>
                {
                    new CraftingElement<ClayItem>(3),
                });
            this.Recipes = new List<Recipe> { recipe };
            this.LaborInCalories = CreateLaborInCaloriesValue(120, typeof(FertilizersSkill));
            this.CraftMinutes = CreateCraftTimeValue(typeof(CompostClayRecipe), 1.2f, typeof(FertilizersSkill), typeof(FertilizersFocusedSpeedTalent), typeof(FertilizersParallelSpeedTalent));

            this.ModsPreInitialize();
            this.Initialize(Localizer.DoStr("Compost Clay"), typeof(CompostClayRecipe));
            this.ModsPostInitialize();
            CraftingComponent.AddRecipe(typeof(FrothFloatationCellObject), this);
        }

        /// <summary>Hook for mods to customize RecipeFamily before initialization. You can change recipes, xp, labor, time here.</summary>
        partial void ModsPreInitialize();
        /// <summary>Hook for mods to customize RecipeFamily after initialization, but before registration. You can change skill requirements here.</summary>
        partial void ModsPostInitialize();
    }
}
