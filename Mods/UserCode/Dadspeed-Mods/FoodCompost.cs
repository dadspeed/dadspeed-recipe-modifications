namespace Eco.Mods.TechTree
{
    using System;
    using System.Collections.Generic;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.DynamicValues;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Players;
    using Eco.Gameplay.Skills;
    using Eco.Shared.Utils;
    using Eco.World;
    using Eco.World.Blocks;
    using Gameplay.Systems.TextLinks;
    using Eco.Shared.Localization;
    using Eco.Gameplay.Items.Recipes;

    public partial class FoodCompostRecipe : RecipeFamily
    {
        public FoodCompostRecipe()
        {
            var recipe10 = new Recipe();
            recipe10.Init(
                "FoodCompost10",  //noloc
                Localizer.DoStr("Food Compost x10"),
                new List<IngredientElement>
                {
                    new IngredientElement("Food", 70, true), //noLoc
                    new IngredientElement(typeof(DirtItem), 3, true),
                },
                new List<CraftingElement>
                {
                    new CraftingElement<CompostItem>(10),
                });
            var recipe100 = new Recipe();
            recipe100.Init(
                "FoodCompost100",  //noloc
                Localizer.DoStr("Food Compost x100"),
                new List<IngredientElement>
                {
                    new IngredientElement("Food", 700, true), //noLoc
                    new IngredientElement(typeof(DirtItem), 30, true),
                },
                new List<CraftingElement>
                {
                    new CraftingElement<CompostItem>(100),
                });
            var recipeSpoiledFood10 = new Recipe();
            recipeSpoiledFood10.Init(
                "SpoiledFoodCompost10",  //noloc
                Localizer.DoStr("Spoiled Food Compost x10"),
                new List<IngredientElement>
                {
                    new IngredientElement(typeof(SpoiledFoodItem), 70, true),
                    new IngredientElement(typeof(DirtItem), 3, true),
                },
                new List<CraftingElement>
                {
                    new CraftingElement<CompostItem>(10),
                });
            var recipeSpoiledFood100 = new Recipe();
            recipeSpoiledFood100.Init(
                "SpoiledFoodCompost100",  //noloc
                Localizer.DoStr("Spoiled Food Compost x100"),
                new List<IngredientElement>
                {
                    new IngredientElement(typeof(SpoiledFoodItem), 700, true),
                    new IngredientElement(typeof(DirtItem), 30, true),
                },
                new List<CraftingElement>
                {
                    new CraftingElement<CompostItem>(100),
                });

            this.Recipes = new List<Recipe> { recipe10, recipe100, recipeSpoiledFood10, recipeSpoiledFood100 };
            this.LaborInCalories = CreateLaborInCaloriesValue(0f);
            this.CraftMinutes = CreateCraftTimeValue(600f);

            this.ModsPreInitialize();
            this.Initialize(Localizer.DoStr("Food Compost"), typeof(FoodCompostRecipe));
            this.ModsPostInitialize();
            CraftingComponent.AddRecipe(typeof(FarmersTableObject), this);
        }

        /// <summary>Hook for mods to customize RecipeFamily before initialization. You can change recipes, xp, labor, time here.</summary>
        partial void ModsPreInitialize();
        /// <summary>Hook for mods to customize RecipeFamily after initialization, but before registration. You can change skill requirements here.</summary>
        partial void ModsPostInitialize();
    }
}
