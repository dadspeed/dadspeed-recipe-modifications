
namespace Eco.Mods.TechTree
{
    using System.Collections.Generic;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Items.Recipes;

    public partial class OilRecipe
    {
        partial void ModsPreInitialize()
        {
            /// Increase the amount of Cereal Germs needed from 12 to 15
            this.Recipes[0].ReplaceIngredient(
                typeof(CerealGermItem),
                new IngredientElement(typeof(CerealGermItem), 15, typeof(MillingSkill), typeof(MillingLavishResourcesTalent))
            );
        }
    }
}