
namespace Eco.Mods.TechTree
{
    using System.Collections.Generic;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Items.Recipes;

    public partial class SunflowerOilRecipe
    {
        partial void ModsPreInitialize()
        {
            /// Decrease the number of seeds needed from 24 to 16
            this.Recipes[0].ReplaceIngredient(
                typeof(SunflowerSeedItem),
                new IngredientElement(typeof(SunflowerSeedItem), 16, typeof(MillingSkill), typeof(MillingLavishResourcesTalent))
            );
        }
    }
}