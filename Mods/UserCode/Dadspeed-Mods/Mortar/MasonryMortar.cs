
namespace Eco.Mods.TechTree
{
    using System.Collections.Generic;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Items.Recipes;
    public partial class MasonryMortarRecipe
    {
        partial void ModsPreInitialize()
        {
            /// Change sand input to static
            Recipes[0].ReplaceIngredient(
                typeof(SandItem),
                new IngredientElement(typeof(SandItem), 1, true)
            );

            /// Decrease Mortar output from 3 to 1
            Recipes[0].ReplaceOutput(
                typeof(MortarItem),
                new CraftingElement<MortarItem>(1)
            );
        }
    }
}
