# Dadspeed Season 8 mod compilation

## XP Benefits

You can earn +6000 extra calorie capacity, +30kg higher weight limit for carrying items on your person and carry +100% blocks in the hands.
The higher your food xp and housing xp, the more benefit you get. Now you've a reason to still buy expensive once you've levelled your skills - Keep the economy going!
You'll need both food _and_ housing xp; neglecting either xp type will really hurt the amount of reward you get.

Your skill rates are scaled by the "maximum" xp scores, minus the food base gain, to work out how much benefit you get.
The maximum food is set at 120 (times server skill rate) and the maximum housing is set at 200 (times server skill rate). They are only 'maximum' in the sense that if you achieve it you'll get all of the listed benefit amount. If you can get higher, you'll keep earning more reward.

...Skip over the examples if you're not interested in how the exact figure is calculated...

### Example 1:

If you have 5% of the food xp and 80% of the housing xp, you'll get sqrt(0.05 \* 0.8) = 20% of the benefits -> +1200 extra calorie space, +6kg weight limit and +20% hands stack limit.
Eating better food will pay dividends compared with improving your housing score when it's already way ahead of your food.

### Example 2:

If you have 100% of the maximum food xp and 0 housing xp, you'll get no benefits. Build a house! Those furniture makers deserve sales too!

## Specialists Commands

Type `/specialists <demographic>, <specialty>` in the chat to see how many players with the given specialty are in the given demographic. Defaults to Everyone if the demographic is not provided. Defaults to show all specialities if the specialty isn't provided.

In the query results for all skills, the skill icon will link to the skill, and the skill name will link to a new tooltip for the players with that skill in the demographic. The tooltip will refresh every time it pops up as the player list is not cached.

## Dynamic Stack Limits

Allows players to set their own stack limit in their storages by setting the object name to anything which ends with 'SL: \<int>' (case and space sensitive). The stack limit is updated on a timer. It adds this as an extra restriction on top of the ordinary maximum stack size and item type limitations. Fuel and module storages are not affected as they are a different type of storage

## Vehicle speed modifiers

If you're using **Biodiesel** or **Charcoal** to run your vehicles (Powered Cart, Steam Truck, Steam Tractor, Truck) you will enjoy a 25% increase in the top speed. Does **not** affect Skid Steers or Excavators.

## Crushed Garbage

We've added a new recipe to crush things into garbage using the Jaw Crusher. The recipe takes 250 SmallTrashable (mostly items that stack to 100), 5 MediumTrashable (mostly building blocks) or 2 LargeTrashable (large items) to create 1 Garbage.

This is meant to help streamline the handling of materials when cleaning up abandoned players.

## Garbage Burning

There's also a recipe to burn Garbage in a Blast Furnace. 10 Garbage gives 1 Dry Tailings and takes 30 minutes to burn. This makes it so we can mostly get rid of any excess items from cleaning up abandoned properties.

## Compost Clay

We've added a way for Fertilizers to make Clay from Compost, to reduce the amount of clay that needs to be mined for Pots for Smelting.

The recipe is 1 Crushed Shale, 2 Compost and 1 Dirt to make 3 Clay.

## Food Compost

In order to more easily create Compost, we've added a recipe on the Farmers Table to create 10 Compost from 70 Food or Spoiled Food and 3 Dirt. There are also recipes with the same ratios that create 100 Compost to reduce the number of crafting tables needed.

## Oil Drilling Rate Changes

We changed how pump jack time works. We shifted the production rate curve to limit the maximum production rate to 2 barrels per minute and removed the effect of perks on pump jacks. The intention is to make OP spots less advantageous so that more drillers could compete with those with the very best spot.
The production rate curve is as though everyone had the focused perk, which some drillers were relying on to be able to compete with the best spots.

The graph shows the petroleum production rate in barrels per minute.
The dashed red line is the vanilla curve before perks. At 100% it is theoretically limitless which is how one season there was a spot which had a time of 3s per barrel, vastly superior to what the rest of the oil drillers could achieve.
The blue solid line is what the production rate curve is now. It has a limit of 2bpm, significantly reducing the gap between the best spots and the rest, allowing more oil drillers to stay competitive even at lower rates.

![Graph over Oil Curves](https://cdn.discordapp.com/attachments/996520899257831586/1149485366047408318/image.png)

## Tree CO2 absorption

All trees absorb 2x the amount of CO2

## Recipe changes

### Advanced Masonry

- Ashlar uses reinforced concrete instead of steel bars in order to increase use of CrushedRock and Sand

### Advanced Smelting

- Steel bars recipe has been doubled and additionally uses 1 Ceramic Mold

### Basic Engineering

- Increased Crushed Rock usage from 5 to 10 for Asphalt Concrete

### Electronics

- Added 4 Cotton Fabric to Copper and Gold Wiring
- Solar generators output 600W of power and has the following recipe:
  - 32x Steel Plates
  - 16x Servo
  - 12x Basic Circuit
  - 24x Framed Glass
  - 24x Gold Wiring
  - 2x experience
  - 2x craft time
  - 3x calorie cost
- Wind Turbines output 300W of power and has the following recipe:
  - 25 Steel Plates
  - 5 Steel Gearboxes
  - 20 Advanced Circuits
  - 10 Nylon Fabric
  - 8 Epoxy
  - 6 Fiberglass
  - 8 Reinforced Concrete
  - 2x experience
  - 2x craft time
  - 6x calorie cost

### Industry

- Increased costs to make Excavators and Skid Steers, made all inputs static and increased the amounts of some
- Increased costs to make Advanced Combustion Engines by making all ingredients static
- Increased costs to make Radiators by 2x
- Increased costs to make Rubber Wheels by 2x
- Increased costs to make Steel Axles by 2x

### Masonry

- Added new recipe for Boiled Grains Mortar, 3 Mortar from 10 Boiled Grains
- Charred Mortar:
  - Increased Natural Fiber consumption from 5 to 20
  - Increased Calorie consumption from 15 to 45
- Baked Mortar:
  - Increased Wood Pulp consumption from 5 to 12
  - Increased Calorie consumption from 60 to 100
- Masonry Mortar:
  - Reduced Mortar output from 3 to 1
- Increased Crushed Rocks usage by adding 2 Silica to Cement recipe
- Increased Crushed Rocks usage from 5 to 8 for Reinforced Concrete

### Milling

- Increase cost of Cereal Germ Oil from 12 cereal germs to 15
- Reduce cost of Sunflower Seed Oil from 24 seeds to 16

### Oil Drilling

- Change Barrels to use 2 Iron Plates instead of 1 Iron Bar

### Smelting

- Make Iron/Copper/Gold Blast Furnace bar recipes use Ceramic Molds instead of Clay Molds

### Tailoring

- Carpets cost 2x to make, to balance better with other T4 blocks

## Misc Changes

- Change the level requirement of research papers to slow down progress and require more trading.
- Change the power consumption of lights. Indoor lights are changed to mimic the power consumption of LEDs (around 10W) and outdoor lights are changed to mimic HPS (around 40W).
- Change skill books to instead give skill scrolls. Level requirements are also shifted to slow down progress and require more trading.
- Increase carrying capacity of vehicles and increase their fuel consumption by 5x
- Remove spoilage from all seeds
- Increase skill requirement of Boiled Grains to give Campfire Cooking more trade
- Buff Charcoal recipe to increase it's competitiveness with regular coal.
- Reduce skill requirement for Quicklime Glass from 4 to 2 to make it easier for new players to enter the market
- Add CEC level requirement for Ethanol to give a reason to put a star in the skill
- Add "Ceramic Mold" tags to pots so they can be used for smelting metals. Reduce stack size.
- Remove Sewage output from Waste Filter

## Other mods

- NidToolbox
