namespace Eco.Mods.TechTree
{
    using Eco.Gameplay.Components;
    using Eco.Gameplay.Objects;
    using Eco.Shared.Localization;

    public partial class CurrencyExchangeObject
    {
        partial void ModsPreInitialize()
        {
            JurisdictionComponent jurisdictionComponent = this.GetComponent<JurisdictionComponent>();
            if (jurisdictionComponent != null)
            {
                jurisdictionComponent.DisableWhenInvalid = false;
                StatusElement statusElement = this.GetComponent<StatusComponent>().CreateStatusElement();
                statusElement.SetStatusMessage(true, Localizer.DoStr("The currency exchange will work even without a settlement."));
            }
        }
    }
}
