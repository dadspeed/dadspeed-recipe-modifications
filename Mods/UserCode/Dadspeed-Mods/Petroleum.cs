﻿namespace Eco.Mods.TechTree
{
    using Eco.Gameplay.DynamicValues;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Items.Recipes;

    public partial class PetroleumRecipe : RecipeFamily
    {
        partial void ModsPreInitialize()
        {
            /**
             * The new function is:
             * Time per barrel = talent discounts * (maxtime * (1 - dampener * oil))
             */

            // New max time. It makes a big difference to the equation and the crossover point between the new curve and the original
            float maxTime = 10f;

            // How much to dampen the curve by
            float dampener = 0.95f;

            // The value that LayerModifiedValue gives is actually (1 - oil), so we must reverse it to be able to use the oil value in the equation
            var oil = new MultiDynamicValue(
                MultiDynamicOps.Sum,
                new ConstantValue(1f),
                new MultiDynamicValue(
                    MultiDynamicOps.Multiply,
                    new ConstantValue(-1f),
                    new LayerModifiedValue(Eco.Simulation.WorldLayers.LayerNames.Oilfield, 3)
                )
            );

            var dampenerTimesOil = new MultiDynamicValue(
                MultiDynamicOps.Multiply,
                oil,
                new ConstantValue(dampener)
            );

            // Put it back in the format 1 - oil
            var oneMinusOil = new MultiDynamicValue(
                MultiDynamicOps.Sum,
                new ConstantValue(1f),
                new MultiDynamicValue(
                    MultiDynamicOps.Multiply,
                    new ConstantValue(-1f),
                    dampenerTimesOil
                )
            );

            // Remove the oil drilling perks that would speed up production. Focused perk that halves time will no longer be applied
            this.CraftMinutes = new MultiDynamicValue(
                MultiDynamicOps.Multiply,
                CreateCraftTimeValue(typeof(PetroleumRecipe), maxTime, typeof(OilDrillingSkill)),
                oneMinusOil
            );
        }
    }
}