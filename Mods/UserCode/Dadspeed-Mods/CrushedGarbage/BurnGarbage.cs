namespace Eco.Mods.TechTree
{
    using Eco.Gameplay.Components;
    using Eco.Gameplay.DynamicValues;
    using Eco.Gameplay.Items.Recipes;
    using Eco.Shared.Localization;
    using System.Collections.Generic;
    using Eco.Gameplay.Items.Recipes;

    public partial class BurnGarbageRecipe : RecipeFamily
    {
        public BurnGarbageRecipe()
        {
            var recipe = new Recipe();
            recipe.Init(
                name: "BurnGarbage",  //noloc
                displayName: Localizer.DoStr("Burn Garbage"),

                // Defines the ingredients needed to craft this recipe. An ingredient items takes the following inputs
                // type of the item, the amount of the item, the skill required, and the talent used.
                ingredients: new List<IngredientElement>
                {
                    new IngredientElement(typeof(GarbageItem), 10, true)
                },

                // Define our recipe output items.
                // For every output item there needs to be one CraftingElement entry with the type of the final item and the amount
                // to create.
                items: new List<CraftingElement>
                {
                    new CraftingElement<TailingsItem>(1)
                });
            this.Recipes = new List<Recipe> { recipe };
            this.ExperienceOnCraft = 0f; // Defines how much experience is gained when crafted.

            // Defines the amount of labor required and the required skill to add labor
            this.LaborInCalories = new ConstantValue(100);

            // Defines our crafting time for the recipe
            this.CraftMinutes = new ConstantValue(30f);

            // Perform pre/post initialization for user mods and initialize our recipe instance with the display name "Small Crushed Garbage"
            this.ModsPreInitialize();
            this.Initialize(displayText: Localizer.DoStr("Burn Garbage"), recipeType: typeof(BurnGarbageRecipe));
            this.ModsPostInitialize();

            // Register our RecipeFamily instance with the crafting system so it can be crafted.
            CraftingComponent.AddRecipe(tableType: typeof(BlastFurnaceObject), recipe: this);
        }
        /// <summary>Hook for mods to customize RecipeFamily before initialization. You can change recipes, xp, labor, time here.</summary>
        partial void ModsPreInitialize();

        /// <summary>Hook for mods to customize RecipeFamily after initialization, but before registration. You can change skill requirements here.</summary>
        partial void ModsPostInitialize();
    }
}