namespace Eco.Mods.TechTree
{
    using Eco.Gameplay.Components;
    using Eco.Gameplay.DynamicValues;
    using Eco.Gameplay.Items.Recipes;
    using Eco.Shared.Localization;
    using System.Collections.Generic;
    using Eco.Gameplay.Items.Recipes;

    public partial class CrushedGarbageSmallRecipe : RecipeFamily
    {
        public CrushedGarbageSmallRecipe()
        {
            var recipe = new Recipe();
            recipe.Init(
                name: "GarbageCrushedSmall",  //noloc
                displayName: Localizer.DoStr("Small Crushed Garbage"),

                // Defines the ingredients needed to craft this recipe. An ingredient items takes the following inputs
                // type of the item, the amount of the item, the skill required, and the talent used.
                ingredients: new List<IngredientElement>
                {
                    new IngredientElement("SmallTrashable", 250)
                },

                // Define our recipe output items.
                // For every output item there needs to be one CraftingElement entry with the type of the final item and the amount
                // to create.
                items: new List<CraftingElement>
                {
                    new CraftingElement<GarbageItem>(1)
                });
            this.Recipes = new List<Recipe> { recipe };
            this.ExperienceOnCraft = 0f; // Defines how much experience is gained when crafted.

            // Defines the amount of labor required and the required skill to add labor
            this.LaborInCalories = new ConstantValue(100);

            // Defines our crafting time for the recipe
            this.CraftMinutes = new ConstantValue(5f);

            // Perform pre/post initialization for user mods and initialize our recipe instance with the display name "Small Crushed Garbage"
            this.ModsPreInitialize();
            this.Initialize(displayText: Localizer.DoStr("Small Crushed Garbage"), recipeType: typeof(CrushedGarbageSmallRecipe));
            this.ModsPostInitialize();

            // Register our RecipeFamily instance with the crafting system so it can be crafted.
            CraftingComponent.AddRecipe(tableType: typeof(JawCrusherObject), recipe: this);
        }
        /// <summary>Hook for mods to customize RecipeFamily before initialization. You can change recipes, xp, labor, time here.</summary>
        partial void ModsPreInitialize();

        /// <summary>Hook for mods to customize RecipeFamily after initialization, but before registration. You can change skill requirements here.</summary>
        partial void ModsPostInitialize();
    }
    public partial class CrushedGarbageMediumRecipe : RecipeFamily
    {
        public CrushedGarbageMediumRecipe()
        {
            var recipe = new Recipe();
            recipe.Init(
                name: "GarbageCrushedMedium",  //noloc
                displayName: Localizer.DoStr("Medium Crushed Garbage"),

                // Defines the ingredients needed to craft this recipe. An ingredient items takes the following inputs
                // type of the item, the amount of the item, the skill required, and the talent used.
                ingredients: new List<IngredientElement>
                {
                    new IngredientElement("MediumTrashable", 5)
                },

                // Define our recipe output items.
                // For every output item there needs to be one CraftingElement entry with the type of the final item and the amount
                // to create.
                items: new List<CraftingElement>
                {
                    new CraftingElement<GarbageItem>(1)
                });
            this.Recipes = new List<Recipe> { recipe };
            this.ExperienceOnCraft = 0f; // Defines how much experience is gained when crafted.

            // Defines the amount of labor required and the required skill to add labor
            this.LaborInCalories = new ConstantValue(100);

            // Defines our crafting time for the recipe
            this.CraftMinutes = new ConstantValue(5f);

            // Perform pre/post initialization for user mods and initialize our recipe instance with the display name "Medium Crushed Garbage"
            this.ModsPreInitialize();
            this.Initialize(displayText: Localizer.DoStr("Medium Crushed Garbage"), recipeType: typeof(CrushedGarbageMediumRecipe));
            this.ModsPostInitialize();

            // Register our RecipeFamily instance with the crafting system so it can be crafted.
            CraftingComponent.AddRecipe(tableType: typeof(JawCrusherObject), recipe: this);
        }
        /// <summary>Hook for mods to customize RecipeFamily before initialization. You can change recipes, xp, labor, time here.</summary>
        partial void ModsPreInitialize();

        /// <summary>Hook for mods to customize RecipeFamily after initialization, but before registration. You can change skill requirements here.</summary>
        partial void ModsPostInitialize();
    }
    public partial class CrushedGarbageLargeRecipe : RecipeFamily
    {
        public CrushedGarbageLargeRecipe()
        {
            var recipe = new Recipe();
            recipe.Init(
                name: "GarbageCrushedLarge",  //noloc
                displayName: Localizer.DoStr("Large Crushed Garbage"),

                // Defines the ingredients needed to craft this recipe. An ingredient items takes the following inputs
                // type of the item, the amount of the item, the skill required, and the talent used.
                ingredients: new List<IngredientElement>
                {
                    new IngredientElement("LargeTrashable", 2)
                },

                // Define our recipe output items.
                // For every output item there needs to be one CraftingElement entry with the type of the final item and the amount
                // to create.
                items: new List<CraftingElement>
                {
                    new CraftingElement<GarbageItem>(1)
                });
            this.Recipes = new List<Recipe> { recipe };
            this.ExperienceOnCraft = 0f; // Defines how much experience is gained when crafted.

            // Defines the amount of labor required and the required skill to add labor
            this.LaborInCalories = new ConstantValue(100);

            // Defines our crafting time for the recipe
            this.CraftMinutes = new ConstantValue(5f);

            // Perform pre/post initialization for user mods and initialize our recipe instance with the display name "Large Crushed Garbage"
            this.ModsPreInitialize();
            this.Initialize(displayText: Localizer.DoStr("Large Crushed Garbage"), recipeType: typeof(CrushedGarbageLargeRecipe));
            this.ModsPostInitialize();

            // Register our RecipeFamily instance with the crafting system so it can be crafted.
            CraftingComponent.AddRecipe(tableType: typeof(JawCrusherObject), recipe: this);
        }

        /// <summary>Hook for mods to customize RecipeFamily before initialization. You can change recipes, xp, labor, time here.</summary>
        partial void ModsPreInitialize();

        /// <summary>Hook for mods to customize RecipeFamily after initialization, but before registration. You can change skill requirements here.</summary>
        partial void ModsPostInitialize();
    }
}