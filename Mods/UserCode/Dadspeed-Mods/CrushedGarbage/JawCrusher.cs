namespace Eco.Mods.TechTree
{
    using Eco.Gameplay.Systems.NewTooltip;
    using Eco.Shared.Items;
    using Eco.Shared.Localization;
    using Eco.Shared.Utils;
    public partial class JawCrusherItem
    {
        [NewTooltip(CacheAs.Global)]
        public LocString GarbageWarning => Localizer.Format("Attention! Safely store {0} inventory (including clothes) before crushing garbage. You have been warned!", new LocString("entire").Bold()).Color(Color.Red.HexRGBA);
    }
}
