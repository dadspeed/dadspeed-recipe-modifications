using Eco.Core.Items;
namespace Eco.Mods.TechTree
{
	[Tag("SmallTrashable")]
	public partial class AcornItem {}
	[Tag("SmallTrashable")]
	public partial class AcornPowderItem {}
	[Tag("MediumTrashable")]
	public partial class AdornedAshlarBasaltBenchItem {}
	[Tag("MediumTrashable")]
	public partial class AdornedAshlarBasaltChairItem {}
	[Tag("MediumTrashable")]
	public partial class AdornedAshlarBasaltTableItem {}
	[Tag("MediumTrashable")]
	public partial class AdornedAshlarGneissBenchItem {}
	[Tag("MediumTrashable")]
	public partial class AdornedAshlarGneissChairItem {}
	[Tag("MediumTrashable")]
	public partial class AdornedAshlarGneissTableItem {}
	[Tag("MediumTrashable")]
	public partial class AdornedAshlarGraniteBenchItem {}
	[Tag("MediumTrashable")]
	public partial class AdornedAshlarGraniteChairItem {}
	[Tag("MediumTrashable")]
	public partial class AdornedAshlarGraniteTableItem {}
	[Tag("MediumTrashable")]
	public partial class AdornedAshlarLimestoneBenchItem {}
	[Tag("MediumTrashable")]
	public partial class AdornedAshlarLimestoneChairItem {}
	[Tag("MediumTrashable")]
	public partial class AdornedAshlarLimestoneTableItem {}
	[Tag("MediumTrashable")]
	public partial class AdornedAshlarSandstoneBenchItem {}
	[Tag("MediumTrashable")]
	public partial class AdornedAshlarSandstoneChairItem {}
	[Tag("MediumTrashable")]
	public partial class AdornedAshlarSandstoneTableItem {}
	[Tag("MediumTrashable")]
	public partial class AdornedAshlarShaleBenchItem {}
	[Tag("MediumTrashable")]
	public partial class AdornedAshlarShaleChairItem {}
	[Tag("MediumTrashable")]
	public partial class AdornedAshlarShaleTableItem {}
	[Tag("MediumTrashable")]
	public partial class AdornedAshlarStoneBenchItem {}
	[Tag("MediumTrashable")]
	public partial class AdornedAshlarStoneChairItem {}
	[Tag("MediumTrashable")]
	public partial class AdornedAshlarStoneTableItem {}
	[Tag("SmallTrashable")]
	public partial class AdvancedBakingSkillBook {}
	[Tag("SmallTrashable")]
	public partial class AdvancedBakingSkillScroll {}
	[Tag("SmallTrashable")]
	public partial class AdvancedBakingUpgradeItem {}
	[Tag("LargeTrashable")]
	public partial class AdvancedCarpentryTableItem {}
	[Tag("SmallTrashable")]
	public partial class AdvancedCircuitItem {}
	[Tag("SmallTrashable")]
	public partial class AdvancedCombustionEngineItem {}
	[Tag("SmallTrashable")]
	public partial class AdvancedCookingSkillBook {}
	[Tag("SmallTrashable")]
	public partial class AdvancedCookingSkillScroll {}
	[Tag("SmallTrashable")]
	public partial class AdvancedCookingUpgradeItem {}
	[Tag("SmallTrashable")]
	public partial class AdvancedMasonrySkillBook {}
	[Tag("SmallTrashable")]
	public partial class AdvancedMasonrySkillScroll {}
	[Tag("LargeTrashable")]
	public partial class AdvancedMasonryTableItem {}
	[Tag("SmallTrashable")]
	public partial class AdvancedMasonryUpgradeItem {}
	[Tag("SmallTrashable")]
	public partial class AdvancedSmeltingSkillBook {}
	[Tag("SmallTrashable")]
	public partial class AdvancedSmeltingSkillScroll {}
	[Tag("SmallTrashable")]
	public partial class AdvancedSmeltingUpgradeItem {}
	[Tag("LargeTrashable")]
	public partial class AdvancedTailoringTableItem {}
	[Tag("SmallTrashable")]
	public partial class AdvancedUpgradeLvl1Item {}
	[Tag("SmallTrashable")]
	public partial class AdvancedUpgradeLvl2Item {}
	[Tag("SmallTrashable")]
	public partial class AdvancedUpgradeLvl3Item {}
	[Tag("SmallTrashable")]
	public partial class AdvancedUpgradeLvl4Item {}
	[Tag("SmallTrashable")]
	public partial class AgaveLeavesItem {}
	[Tag("SmallTrashable")]
	public partial class AgaveSeedItem {}
	[Tag("MediumTrashable")]
	public partial class AgoutiCarcassItem {}
	[Tag("SmallTrashable")]
	public partial class AgoutiEnchiladasItem {}
	[Tag("SmallTrashable")]
	public partial class AgricultureResearchPaperAdvancedItem {}
	[Tag("SmallTrashable")]
	public partial class AgricultureResearchPaperModernItem {}
	[Tag("MediumTrashable")]
	public partial class AlligatorCarcassItem {}
	[Tag("SmallTrashable")]
	public partial class AmanitaMushroomsItem {}
	[Tag("SmallTrashable")]
	public partial class AmanitaMushroomSporesItem {}
	[Tag("LargeTrashable")]
	public partial class AmendmentsItem {}
	[Tag("MediumTrashable")]
	public partial class AnvilItem {}
	[Tag("SmallTrashable")]
	public partial class ArcticWillowSeedItem {}
	[Tag("LargeTrashable")]
	public partial class ArrastraItem {}
	[Tag("SmallTrashable")]
	public partial class ArrowItem {}
	[Tag("SmallTrashable")]
	public partial class ArtSuppliesItem {}
	[Tag("MediumTrashable")]
	public partial class AshlarBasaltBenchItem {}
	[Tag("MediumTrashable")]
	public partial class AshlarBasaltChairItem {}
	[Tag("MediumTrashable")]
	public partial class AshlarBasaltDoorItem {}
	[Tag("LargeTrashable")]
	public partial class AshlarBasaltFireplaceItem {}
	[Tag("MediumTrashable")]
	public partial class AshlarBasaltItem {}
	[Tag("MediumTrashable")]
	public partial class AshlarBasaltTableItem {}
	[Tag("MediumTrashable")]
	public partial class AshlarGneissBenchItem {}
	[Tag("MediumTrashable")]
	public partial class AshlarGneissChairItem {}
	[Tag("MediumTrashable")]
	public partial class AshlarGneissDoorItem {}
	[Tag("LargeTrashable")]
	public partial class AshlarGneissFireplaceItem {}
	[Tag("MediumTrashable")]
	public partial class AshlarGneissItem {}
	[Tag("MediumTrashable")]
	public partial class AshlarGneissTableItem {}
	[Tag("MediumTrashable")]
	public partial class AshlarGraniteBenchItem {}
	[Tag("MediumTrashable")]
	public partial class AshlarGraniteChairItem {}
	[Tag("MediumTrashable")]
	public partial class AshlarGraniteDoorItem {}
	[Tag("LargeTrashable")]
	public partial class AshlarGraniteFireplaceItem {}
	[Tag("MediumTrashable")]
	public partial class AshlarGraniteItem {}
	[Tag("MediumTrashable")]
	public partial class AshlarGraniteTableItem {}
	[Tag("LargeTrashable")]
	public partial class AshlarLargeBasaltFountainItem {}
	[Tag("LargeTrashable")]
	public partial class AshlarLargeGneissFountainItem {}
	[Tag("LargeTrashable")]
	public partial class AshlarLargeGraniteFountainItem {}
	[Tag("LargeTrashable")]
	public partial class AshlarLargeLimestoneFountainItem {}
	[Tag("LargeTrashable")]
	public partial class AshlarLargeSandstoneFountainItem {}
	[Tag("LargeTrashable")]
	public partial class AshlarLargeShaleFountainItem {}
	[Tag("LargeTrashable")]
	public partial class AshlarLargeStoneFountainItem {}
	[Tag("MediumTrashable")]
	public partial class AshlarLimestoneBenchItem {}
	[Tag("MediumTrashable")]
	public partial class AshlarLimestoneChairItem {}
	[Tag("MediumTrashable")]
	public partial class AshlarLimestoneDoorItem {}
	[Tag("LargeTrashable")]
	public partial class AshlarLimestoneFireplaceItem {}
	[Tag("MediumTrashable")]
	public partial class AshlarLimestoneItem {}
	[Tag("MediumTrashable")]
	public partial class AshlarLimestoneTableItem {}
	[Tag("MediumTrashable")]
	public partial class AshlarSandstoneBenchItem {}
	[Tag("MediumTrashable")]
	public partial class AshlarSandstoneChairItem {}
	[Tag("MediumTrashable")]
	public partial class AshlarSandstoneDoorItem {}
	[Tag("LargeTrashable")]
	public partial class AshlarSandstoneFireplaceItem {}
	[Tag("MediumTrashable")]
	public partial class AshlarSandstoneItem {}
	[Tag("MediumTrashable")]
	public partial class AshlarSandstoneTableItem {}
	[Tag("MediumTrashable")]
	public partial class AshlarShaleBenchItem {}
	[Tag("MediumTrashable")]
	public partial class AshlarShaleChairItem {}
	[Tag("MediumTrashable")]
	public partial class AshlarShaleDoorItem {}
	[Tag("LargeTrashable")]
	public partial class AshlarShaleFireplaceItem {}
	[Tag("MediumTrashable")]
	public partial class AshlarShaleItem {}
	[Tag("MediumTrashable")]
	public partial class AshlarShaleTableItem {}
	[Tag("MediumTrashable")]
	public partial class AshlarSmallBasaltFountainItem {}
	[Tag("MediumTrashable")]
	public partial class AshlarSmallGneissFountainItem {}
	[Tag("MediumTrashable")]
	public partial class AshlarSmallGraniteFountainItem {}
	[Tag("MediumTrashable")]
	public partial class AshlarSmallLimestoneFountainItem {}
	[Tag("MediumTrashable")]
	public partial class AshlarSmallSandstoneFountainItem {}
	[Tag("MediumTrashable")]
	public partial class AshlarSmallShaleFountainItem {}
	[Tag("MediumTrashable")]
	public partial class AshlarSmallStoneFountainItem {}
	[Tag("MediumTrashable")]
	public partial class AshlarStoneBenchItem {}
	[Tag("MediumTrashable")]
	public partial class AshlarStoneChairItem {}
	[Tag("MediumTrashable")]
	public partial class AshlarStoneDoorItem {}
	[Tag("LargeTrashable")]
	public partial class AshlarStoneFireplaceItem {}
	[Tag("MediumTrashable")]
	public partial class AshlarStoneTableItem {}
	[Tag("MediumTrashable")]
	public partial class AsphaltConcreteItem {}
	[Tag("LargeTrashable")]
	public partial class AssemblyLineItem {}
	[Tag("LargeTrashable")]
	public partial class AutomaticLoomItem {}
	[Tag("SmallTrashable")]
	public partial class AutumnStewItem {}
	[Tag("SmallTrashable")]
	public partial class BakedAgaveItem {}
	[Tag("SmallTrashable")]
	public partial class BakedBeetItem {}
	[Tag("SmallTrashable")]
	public partial class BakedCornItem {}
	[Tag("SmallTrashable")]
	public partial class BakedHeartOfPalmItem {}
	[Tag("SmallTrashable")]
	public partial class BakedMeatItem {}
	[Tag("SmallTrashable")]
	public partial class BakedRoastItem {}
	[Tag("SmallTrashable")]
	public partial class BakedTaroItem {}
	[Tag("SmallTrashable")]
	public partial class BakedTomatoItem {}
	[Tag("LargeTrashable")]
	public partial class BakeryOvenItem {}
	[Tag("SmallTrashable")]
	public partial class BakingSkillBook {}
	[Tag("SmallTrashable")]
	public partial class BakingSkillScroll {}
	[Tag("SmallTrashable")]
	public partial class BakingUpgradeItem {}
	[Tag("MediumTrashable")]
	public partial class BallotBoxItem {}
	[Tag("MediumTrashable")]
	public partial class BandanaItem {}
	[Tag("SmallTrashable")]
	public partial class BanhXeoItem {}
	[Tag("LargeTrashable")]
	public partial class BankItem {}
	[Tag("SmallTrashable")]
	public partial class BannockItem {}
	[Tag("SmallTrashable")]
	public partial class BarrelCactusSeedItem {}
	[Tag("MediumTrashable")]
	public partial class BarrelItem {}
	[Tag("MediumTrashable")]
	public partial class BasicBackpackItem {}
	[Tag("SmallTrashable")]
	public partial class BasicCircuitItem {}
	[Tag("SmallTrashable")]
	public partial class BasicEngineeringSkillBook {}
	[Tag("SmallTrashable")]
	public partial class BasicEngineeringSkillScroll {}
	[Tag("SmallTrashable")]
	public partial class BasicEngineeringUpgradeItem {}
	[Tag("SmallTrashable")]
	public partial class BasicSaladItem {}
	[Tag("SmallTrashable")]
	public partial class BasicUpgradeLvl1Item {}
	[Tag("SmallTrashable")]
	public partial class BasicUpgradeLvl2Item {}
	[Tag("SmallTrashable")]
	public partial class BasicUpgradeLvl3Item {}
	[Tag("SmallTrashable")]
	public partial class BasicUpgradeLvl4Item {}
	[Tag("SmallTrashable")]
	public partial class BassItem {}
	[Tag("MediumTrashable")]
	public partial class BathtubItem {}
	[Tag("SmallTrashable")]
	public partial class BeanPasteItem {}
	[Tag("SmallTrashable")]
	public partial class BeansItem {}
	[Tag("SmallTrashable")]
	public partial class BeanSproutItem {}
	[Tag("SmallTrashable")]
	public partial class BearclawItem {}
	[Tag("MediumTrashable")]
	public partial class BearpackItem {}
	[Tag("SmallTrashable")]
	public partial class BearSUPREMEItem {}
	[Tag("SmallTrashable")]
	public partial class BeetCampfireSaladItem {}
	[Tag("SmallTrashable")]
	public partial class BeetGreensItem {}
	[Tag("SmallTrashable")]
	public partial class BeetItem {}
	[Tag("SmallTrashable")]
	public partial class BeetSeedItem {}
	[Tag("SmallTrashable")]
	public partial class BerryExtractFertilizerItem {}
	[Tag("MediumTrashable")]
	public partial class BigBackpackItem {}
	[Tag("SmallTrashable")]
	public partial class BigBluestemSeedItem {}
	[Tag("MediumTrashable")]
	public partial class BighornCarcassItem {}
	[Tag("MediumTrashable")]
	public partial class BiodieselItem {}
	[Tag("MediumTrashable")]
	public partial class BirchLogItem {}
	[Tag("SmallTrashable")]
	public partial class BirchSeedItem {}
	[Tag("LargeTrashable")]
	public partial class BisonCarcassItem {}
	[Tag("SmallTrashable")]
	public partial class BisonChowFunItem {}
	[Tag("LargeTrashable")]
	public partial class BisonMountItem {}
	[Tag("LargeTrashable")]
	public partial class BlastFurnaceItem {}
	[Tag("SmallTrashable")]
	public partial class BloodMealFertilizerItem {}
	[Tag("MediumTrashable")]
	public partial class BloomeryItem {}
	[Tag("MediumTrashable")]
	public partial class BlueSharkItem {}
	[Tag("SmallTrashable")]
	public partial class BoardItem {}
	[Tag("LargeTrashable")]
	public partial class BoardOfElectionsItem {}
	[Tag("SmallTrashable")]
	public partial class BoiledGrainsItem {}
	[Tag("SmallTrashable")]
	public partial class BoiledRiceItem {}
	[Tag("SmallTrashable")]
	public partial class BoiledSausageItem {}
	[Tag("SmallTrashable")]
	public partial class BoilerItem {}
	[Tag("SmallTrashable")]
	public partial class BoleteMushroomsItem {}
	[Tag("SmallTrashable")]
	public partial class BoleteMushroomSporesItem {}
	[Tag("MediumTrashable")]
	public partial class BookshelfItem {}
	[Tag("MediumTrashable")]
	public partial class BrazierItem {}
	[Tag("SmallTrashable")]
	public partial class BreadItem {}
	[Tag("LargeTrashable")]
	public partial class BrickFireplaceItem {}
	[Tag("MediumTrashable")]
	public partial class BrickItem {}
	[Tag("MediumTrashable")]
	public partial class BuilderBootsItem {}
	[Tag("MediumTrashable")]
	public partial class BuilderGlovesItem {}
	[Tag("MediumTrashable")]
	public partial class BuilderHelmetItem {}
	[Tag("MediumTrashable")]
	public partial class BuilderOverallsItem {}
	[Tag("MediumTrashable")]
	public partial class BuilderShirtItem {}
	[Tag("SmallTrashable")]
	public partial class BullrushSeedItem {}
	[Tag("SmallTrashable")]
	public partial class BunchgrassSeedItem {}
	[Tag("SmallTrashable")]
	public partial class ButcherySkillBook {}
	[Tag("SmallTrashable")]
	public partial class ButcherySkillScroll {}
	[Tag("MediumTrashable")]
	public partial class ButcheryTableItem {}
	[Tag("SmallTrashable")]
	public partial class ButcheryUpgradeItem {}
	[Tag("MediumTrashable")]
	public partial class BycocketItem {}
	[Tag("SmallTrashable")]
	public partial class CamasAshFertilizerItem {}
	[Tag("SmallTrashable")]
	public partial class CamasBreadItem {}
	[Tag("SmallTrashable")]
	public partial class CamasBulbBakeItem {}
	[Tag("SmallTrashable")]
	public partial class CamasBulbItem {}
	[Tag("SmallTrashable")]
	public partial class CamasPasteItem {}
	[Tag("SmallTrashable")]
	public partial class CameraFilmItem {}
	[Tag("MediumTrashable")]
	public partial class CamoPantsItem {}
	[Tag("MediumTrashable")]
	public partial class CamoShirtItem {}
	[Tag("SmallTrashable")]
	public partial class CampfireCookingUpgradeItem {}
	[Tag("MediumTrashable")]
	public partial class CampfireItem {}
	[Tag("SmallTrashable")]
	public partial class CampfireRoastItem {}
	[Tag("LargeTrashable")]
	public partial class CampsiteItem {}
	[Tag("MediumTrashable")]
	public partial class CandleStandItem {}
	[Tag("SmallTrashable")]
	public partial class CanvasItem {}
	[Tag("LargeTrashable")]
	public partial class CapitolItem {}
	[Tag("MediumTrashable")]
	public partial class CaprisItem {}
	[Tag("MediumTrashable")]
	public partial class CarbideHelmetLampItem {}
	[Tag("SmallTrashable")]
	public partial class CarpentryAdvancedUpgradeItem {}
	[Tag("SmallTrashable")]
	public partial class CarpentryBasicUpgradeItem {}
	[Tag("SmallTrashable")]
	public partial class CarpentrySkillBook {}
	[Tag("SmallTrashable")]
	public partial class CarpentrySkillScroll {}
	[Tag("MediumTrashable")]
	public partial class CarpentryTableItem {}
	[Tag("MediumTrashable")]
	public partial class CarvedPumpkinItem {}
	[Tag("LargeTrashable")]
	public partial class CastIronBedItem {}
	[Tag("LargeTrashable")]
	public partial class CastIronRoyalBedItem {}
	[Tag("LargeTrashable")]
	public partial class CastIronStoveItem {}
	[Tag("MediumTrashable")]
	public partial class CedarLogItem {}
	[Tag("SmallTrashable")]
	public partial class CedarSeedItem {}
	[Tag("MediumTrashable")]
	public partial class CeibaLogItem {}
	[Tag("SmallTrashable")]
	public partial class CeibaSeedItem {}
	[Tag("MediumTrashable")]
	public partial class CeilingCandleItem {}
	[Tag("SmallTrashable")]
	public partial class CelluloseFiberItem {}
	[Tag("MediumTrashable")]
	public partial class CementItem {}
	[Tag("LargeTrashable")]
	public partial class CementKilnItem {}
	[Tag("LargeTrashable")]
	public partial class CensusBureauItem {}
	[Tag("SmallTrashable")]
	public partial class CerealGermItem {}
	[Tag("MediumTrashable")]
	public partial class ChainsawItem {}
	[Tag("SmallTrashable")]
	public partial class CharcoalItem {}
	[Tag("SmallTrashable")]
	public partial class CharredAgaveItem {}
	[Tag("SmallTrashable")]
	public partial class CharredBeansItem {}
	[Tag("SmallTrashable")]
	public partial class CharredBeetItem {}
	[Tag("SmallTrashable")]
	public partial class CharredCactusFruitItem {}
	[Tag("SmallTrashable")]
	public partial class CharredCamasBulbItem {}
	[Tag("SmallTrashable")]
	public partial class CharredCornItem {}
	[Tag("SmallTrashable")]
	public partial class CharredFireweedShootsItem {}
	[Tag("SmallTrashable")]
	public partial class CharredFishItem {}
	[Tag("SmallTrashable")]
	public partial class CharredHeartOfPalmItem {}
	[Tag("SmallTrashable")]
	public partial class CharredMeatItem {}
	[Tag("SmallTrashable")]
	public partial class CharredMushroomsItem {}
	[Tag("SmallTrashable")]
	public partial class CharredPapayaItem {}
	[Tag("SmallTrashable")]
	public partial class CharredPineappleItem {}
	[Tag("SmallTrashable")]
	public partial class CharredSausageItem {}
	[Tag("SmallTrashable")]
	public partial class CharredTaroItem {}
	[Tag("SmallTrashable")]
	public partial class CharredTomatoItem {}
	[Tag("MediumTrashable")]
	public partial class ChefHatItem {}
	[Tag("MediumTrashable")]
	public partial class ChefPantsItem {}
	[Tag("MediumTrashable")]
	public partial class ChefShirtItem {}
	[Tag("MediumTrashable")]
	public partial class ChefShoesItem {}
	[Tag("MediumTrashable")]
	public partial class ClaimToolItem {}
	[Tag("SmallTrashable")]
	public partial class ClamChowderItem {}
	[Tag("SmallTrashable")]
	public partial class ClamItem {}
	[Tag("MediumTrashable")]
	public partial class ClothBeltItem {}
	[Tag("MediumTrashable")]
	public partial class ClothCapeItem {}
	[Tag("SmallTrashable")]
	public partial class CodItem {}
	[Tag("SmallTrashable")]
	public partial class ColoredPowderItem {}
	[Tag("LargeTrashable")]
	public partial class CombustionEngineItem {}
	[Tag("LargeTrashable")]
	public partial class CombustionGeneratorItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeBirchBenchItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeBirchChairItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeBirchDoorItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeBirchLumberItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeBirchTableItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeBowItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeCedarBenchItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeCedarChairItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeCedarDoorItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeCedarLumberItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeCedarTableItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeCeibaBenchItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeCeibaChairItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeCeibaDoorItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeCeibaLumberItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeCeibaTableItem {}
	[Tag("SmallTrashable")]
	public partial class CompositeFillerItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeFirBenchItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeFirChairItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeFirDoorItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeFirLumberItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeFirTableItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeHardwoodBenchItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeHardwoodChairItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeHardwoodDoorItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeHardwoodTableItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeJoshuaBenchItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeJoshuaChairItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeJoshuaDoorItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeJoshuaLumberItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeJoshuaTableItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeLumberBenchItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeLumberChairItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeLumberDoorItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeLumberItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeLumberTableItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeOakBenchItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeOakChairItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeOakDoorItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeOakLumberItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeOakTableItem {}
	[Tag("MediumTrashable")]
	public partial class CompositePalmBenchItem {}
	[Tag("MediumTrashable")]
	public partial class CompositePalmChairItem {}
	[Tag("MediumTrashable")]
	public partial class CompositePalmDoorItem {}
	[Tag("MediumTrashable")]
	public partial class CompositePalmLumberItem {}
	[Tag("MediumTrashable")]
	public partial class CompositePalmTableItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeRedwoodBenchItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeRedwoodChairItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeRedwoodDoorItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeRedwoodLumberItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeRedwoodTableItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeSaguaroBenchItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeSaguaroChairItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeSaguaroDoorItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeSaguaroLumberItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeSaguaroTableItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeSoftwoodBenchItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeSoftwoodChairItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeSoftwoodDoorItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeSoftwoodTableItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeSpruceBenchItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeSpruceChairItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeSpruceDoorItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeSpruceLumberItem {}
	[Tag("MediumTrashable")]
	public partial class CompositeSpruceTableItem {}
	[Tag("SmallTrashable")]
	public partial class CompositesSkillBook {}
	[Tag("SmallTrashable")]
	public partial class CompositesSkillScroll {}
	[Tag("SmallTrashable")]
	public partial class CompositesUpgradeItem {}
	[Tag("SmallTrashable")]
	public partial class CompostFertilizerItem {}
	[Tag("LargeTrashable")]
	public partial class ComputerLabItem {}
	[Tag("MediumTrashable")]
	public partial class ConstructionPostItem {}
	[Tag("LargeTrashable")]
	public partial class ContractBoardItem {}
	[Tag("SmallTrashable")]
	public partial class CookeinaMushroomsItem {}
	[Tag("SmallTrashable")]
	public partial class CookeinaMushroomSporesItem {}
	[Tag("SmallTrashable")]
	public partial class CookingSkillBook {}
	[Tag("SmallTrashable")]
	public partial class CookingSkillScroll {}
	[Tag("SmallTrashable")]
	public partial class CookingUpgradeItem {}
	[Tag("MediumTrashable")]
	public partial class CopperBarItem {}
	[Tag("MediumTrashable")]
	public partial class CopperFrameLandscapeItem {}
	[Tag("MediumTrashable")]
	public partial class CopperFramePortraitItem {}
	[Tag("MediumTrashable")]
	public partial class CopperFrameSquareItem {}
	[Tag("MediumTrashable")]
	public partial class CopperFrameTallItem {}
	[Tag("MediumTrashable")]
	public partial class CopperFrameWideItem {}
	[Tag("MediumTrashable")]
	public partial class CopperPipeItem {}
	[Tag("SmallTrashable")]
	public partial class CopperPlateItem {}
	[Tag("SmallTrashable")]
	public partial class CopperWiringItem {}
	[Tag("SmallTrashable")]
	public partial class CornFrittersItem {}
	[Tag("SmallTrashable")]
	public partial class CornItem {}
	[Tag("SmallTrashable")]
	public partial class CornmealItem {}
	[Tag("SmallTrashable")]
	public partial class CornSeedItem {}
	[Tag("MediumTrashable")]
	public partial class CorrugatedSteelDoorItem {}
	[Tag("MediumTrashable")]
	public partial class CorrugatedSteelFenceDoorItem {}
	[Tag("MediumTrashable")]
	public partial class CorrugatedSteelItem {}
	[Tag("SmallTrashable")]
	public partial class CottonBollItem {}
	[Tag("MediumTrashable")]
	public partial class CottonCarpetItem {}
	[Tag("MediumTrashable")]
	public partial class CottonCurtainsItem {}
	[Tag("SmallTrashable")]
	public partial class CottonFabricItem {}
	[Tag("SmallTrashable")]
	public partial class CottonLintItem {}
	[Tag("SmallTrashable")]
	public partial class CottonSeedItem {}
	[Tag("LargeTrashable")]
	public partial class CottonStreamerStarsItem {}
	[Tag("LargeTrashable")]
	public partial class CottonStreamerStripesItem {}
	[Tag("SmallTrashable")]
	public partial class CottonThreadItem {}
	[Tag("MediumTrashable")]
	public partial class CouchItem {}
	[Tag("LargeTrashable")]
	public partial class CountryFoundationItem {}
	[Tag("MediumTrashable")]
	public partial class CourtChairItem {}
	[Tag("MediumTrashable")]
	public partial class CourtTableItem {}
	[Tag("MediumTrashable")]
	public partial class CoyoteCarcassItem {}
	[Tag("MediumTrashable")]
	public partial class CrabCarcassItem {}
	[Tag("MediumTrashable")]
	public partial class CrabPotItem {}
	[Tag("LargeTrashable")]
	public partial class CraneItem {}
	[Tag("SmallTrashable")]
	public partial class CreosoteBushSeedItem {}
	[Tag("SmallTrashable")]
	public partial class CreosoteFlowerItem {}
	[Tag("SmallTrashable")]
	public partial class CriminiMushroomsItem {}
	[Tag("SmallTrashable")]
	public partial class CriminiMushroomSporesItem {}
	[Tag("SmallTrashable")]
	public partial class CrimsonSaladItem {}
	[Tag("SmallTrashable")]
	public partial class CrispyBaconItem {}
	[Tag("SmallTrashable")]
	public partial class CulinaryResearchPaperAdvancedItem {}
	[Tag("SmallTrashable")]
	public partial class CulinaryResearchPaperBasicItem {}
	[Tag("SmallTrashable")]
	public partial class CulinaryResearchPaperModernItem {}
	[Tag("LargeTrashable")]
	public partial class CurrencyExchangeItem {}
	[Tag("SmallTrashable")]
	public partial class CuttingEdgeCookingSkillBook {}
	[Tag("SmallTrashable")]
	public partial class CuttingEdgeCookingSkillScroll {}
	[Tag("SmallTrashable")]
	public partial class CuttingEdgeCookingUpgradeItem {}
	[Tag("MediumTrashable")]
	public partial class DecorativeAnchorItem {}
	[Tag("MediumTrashable")]
	public partial class DecorativeGlassBuoyBlueItem {}
	[Tag("MediumTrashable")]
	public partial class DecorativeGlassBuoyGreenItem {}
	[Tag("MediumTrashable")]
	public partial class DecorativeGlassBuoyRedItem {}
	[Tag("MediumTrashable")]
	public partial class DecorativeGlassBuoyVioletItem {}
	[Tag("MediumTrashable")]
	public partial class DecorativeGlassBuoyYellowItem {}
	[Tag("MediumTrashable")]
	public partial class DecorativeHangingBuoyItem {}
	[Tag("MediumTrashable")]
	public partial class DecorativeLifePreserverItem {}
	[Tag("MediumTrashable")]
	public partial class DecorativeShipWheelItem {}
	[Tag("MediumTrashable")]
	public partial class DeerCarcassItem {}
	[Tag("SmallTrashable")]
	public partial class DendrologyResearchPaperAdvancedItem {}
	[Tag("SmallTrashable")]
	public partial class DendrologyResearchPaperBasicItem {}
	[Tag("SmallTrashable")]
	public partial class DendrologyResearchPaperModernItem {}
	[Tag("MediumTrashable")]
	public partial class DerpyHatItem {}
	[Tag("LargeTrashable")]
	public partial class DistributionStationItem {}
	[Tag("SmallTrashable")]
	public partial class DriedFishItem {}
	[Tag("SmallTrashable")]
	public partial class DriedMeatItem {}
	[Tag("SmallTrashable")]
	public partial class DwarfWillowSeedItem {}
	[Tag("MediumTrashable")]
	public partial class EaselItem {}
	[Tag("LargeTrashable")]
	public partial class ElectricLatheItem {}
	[Tag("LargeTrashable")]
	public partial class ElectricMachinistTableItem {}
	[Tag("SmallTrashable")]
	public partial class ElectricMotorItem {}
	[Tag("LargeTrashable")]
	public partial class ElectricPlanerItem {}
	[Tag("LargeTrashable")]
	public partial class ElectricStampingPressItem {}
	[Tag("MediumTrashable")]
	public partial class ElectricWallLampItem {}
	[Tag("LargeTrashable")]
	public partial class ElectricWaterPumpItem {}
	[Tag("LargeTrashable")]
	public partial class ElectronicsAssemblyItem {}
	[Tag("SmallTrashable")]
	public partial class ElectronicsSkillBook {}
	[Tag("SmallTrashable")]
	public partial class ElectronicsSkillScroll {}
	[Tag("SmallTrashable")]
	public partial class ElectronicsUpgradeItem {}
	[Tag("MediumTrashable")]
	public partial class ElevatorCallPostItem {}
	[Tag("MediumTrashable")]
	public partial class ElkCarcassItem {}
	[Tag("LargeTrashable")]
	public partial class ElkMountItem {}
	[Tag("SmallTrashable")]
	public partial class ElkTacoItem {}
	[Tag("SmallTrashable")]
	public partial class ElkWellingtonItem {}
	[Tag("LargeTrashable")]
	public partial class EmbassyDeskItem {}
	[Tag("SmallTrashable")]
	public partial class EngineeringResearchPaperAdvancedItem {}
	[Tag("SmallTrashable")]
	public partial class EngineeringResearchPaperModernItem {}
	[Tag("SmallTrashable")]
	public partial class EpoxyItem {}
	[Tag("SmallTrashable")]
	public partial class EthanolItem {}
	[Tag("LargeTrashable")]
	public partial class ExcavatorItem {}
	[Tag("LargeTrashable")]
	public partial class ExecutiveOfficeItem {}
	[Tag("MediumTrashable")]
	public partial class ExplorerBootsItem {}
	[Tag("MediumTrashable")]
	public partial class ExplorerGlovesItem {}
	[Tag("MediumTrashable")]
	public partial class ExplorerHatItem {}
	[Tag("MediumTrashable")]
	public partial class ExplorerPantsItem {}
	[Tag("MediumTrashable")]
	public partial class ExplorerShirtItem {}
	[Tag("SmallTrashable")]
	public partial class FantasticForestPizzaItem {}
	[Tag("MediumTrashable")]
	public partial class FarmerBootsItem {}
	[Tag("MediumTrashable")]
	public partial class FarmerGlovesItem {}
	[Tag("MediumTrashable")]
	public partial class FarmerHatItem {}
	[Tag("MediumTrashable")]
	public partial class FarmerOverallsItem {}
	[Tag("MediumTrashable")]
	public partial class FarmerShirtItem {}
	[Tag("MediumTrashable")]
	public partial class FarmersTableItem {}
	[Tag("SmallTrashable")]
	public partial class FarmingSkillBook {}
	[Tag("SmallTrashable")]
	public partial class FarmingSkillScroll {}
	[Tag("SmallTrashable")]
	public partial class FarmingUpgradeItem {}
	[Tag("LargeTrashable")]
	public partial class FederationFoundationItem {}
	[Tag("SmallTrashable")]
	public partial class FernCampfireSaladItem {}
	[Tag("SmallTrashable")]
	public partial class FernSporeItem {}
	[Tag("SmallTrashable")]
	public partial class FertilizersSkillBook {}
	[Tag("SmallTrashable")]
	public partial class FertilizersSkillScroll {}
	[Tag("SmallTrashable")]
	public partial class FertilizersUpgradeItem {}
	[Tag("SmallTrashable")]
	public partial class FiberFillerItem {}
	[Tag("SmallTrashable")]
	public partial class FiberglassItem {}
	[Tag("LargeTrashable")]
	public partial class FiberScutchingStationItem {}
	[Tag("SmallTrashable")]
	public partial class FiddleheadsItem {}
	[Tag("SmallTrashable")]
	public partial class FieldCampfireStewItem {}
	[Tag("SmallTrashable")]
	public partial class FilmyFernSporeItem {}
	[Tag("SmallTrashable")]
	public partial class FireweedSeedItem {}
	[Tag("SmallTrashable")]
	public partial class FireweedShootsItem {}
	[Tag("MediumTrashable")]
	public partial class FirLogItem {}
	[Tag("SmallTrashable")]
	public partial class FirSeedItem {}
	[Tag("LargeTrashable")]
	public partial class FisheryItem {}
	[Tag("SmallTrashable")]
	public partial class FishingPoleItem {}
	[Tag("SmallTrashable")]
	public partial class FishNChipsItem {}
	[Tag("LargeTrashable")]
	public partial class FishRackItem {}
	[Tag("MediumTrashable")]
	public partial class FishTrapItem {}
	[Tag("SmallTrashable")]
	public partial class FlatbreadItem {}
	[Tag("MediumTrashable")]
	public partial class FlatSteelDoorItem {}
	[Tag("MediumTrashable")]
	public partial class FlatSteelItem {}
	[Tag("SmallTrashable")]
	public partial class FlaxFiberItem {}
	[Tag("SmallTrashable")]
	public partial class FlaxSeedItem {}
	[Tag("SmallTrashable")]
	public partial class FlaxseedOilItem {}
	[Tag("SmallTrashable")]
	public partial class FlaxStemItem {}
	[Tag("MediumTrashable")]
	public partial class FlaxTrawlerNetItem {}
	[Tag("SmallTrashable")]
	public partial class FlourItem {}
	[Tag("MediumTrashable")]
	public partial class FoxCarcassItem {}
	[Tag("MediumTrashable")]
	public partial class FramedGlassDoorItem {}
	[Tag("MediumTrashable")]
	public partial class FramedGlassItem {}
	[Tag("SmallTrashable")]
	public partial class FriedCamasItem {}
	[Tag("SmallTrashable")]
	public partial class FriedFiddleheadsItem {}
	[Tag("SmallTrashable")]
	public partial class FriedHareHaunchesItem {}
	[Tag("SmallTrashable")]
	public partial class FriedHeartsOfPalmItem {}
	[Tag("SmallTrashable")]
	public partial class FriedTaroItem {}
	[Tag("SmallTrashable")]
	public partial class FriedTomatoesItem {}
	[Tag("LargeTrashable")]
	public partial class FrothFloatationCellItem {}
	[Tag("SmallTrashable")]
	public partial class FruitMuffinItem {}
	[Tag("SmallTrashable")]
	public partial class FruitSaladItem {}
	[Tag("SmallTrashable")]
	public partial class FruitTartItem {}
	[Tag("SmallTrashable")]
	public partial class FurPeltItem {}
	[Tag("MediumTrashable")]
	public partial class GalaxyVaseItem {}
	[Tag("MediumTrashable")]
	public partial class GardenBootsItem {}
	[Tag("MediumTrashable")]
	public partial class GasolineItem {}
	[Tag("SmallTrashable")]
	public partial class GatheringBasicUpgradeItem {}
	[Tag("SmallTrashable")]
	public partial class GatheringResearchPaperBasicItem {}
	[Tag("SmallTrashable")]
	public partial class GearboxItem {}
	[Tag("SmallTrashable")]
	public partial class GeologyResearchPaperAdvancedItem {}
	[Tag("SmallTrashable")]
	public partial class GeologyResearchPaperBasicItem {}
	[Tag("SmallTrashable")]
	public partial class GeologyResearchPaperModernItem {}
	[Tag("SmallTrashable")]
	public partial class GiantCactusFruitItem {}
	[Tag("MediumTrashable")]
	public partial class GigotSleeveShirtItem {}
	[Tag("MediumTrashable")]
	public partial class GlassItem {}
	[Tag("SmallTrashable")]
	public partial class GlassLensItem {}
	[Tag("SmallTrashable")]
	public partial class GlassworkingAdvancedUpgradeItem {}
	[Tag("SmallTrashable")]
	public partial class GlassworkingModernUpgradeItem {}
	[Tag("SmallTrashable")]
	public partial class GlassworkingSkillBook {}
	[Tag("SmallTrashable")]
	public partial class GlassworkingSkillScroll {}
	[Tag("LargeTrashable")]
	public partial class GlassworksItem {}
	[Tag("MediumTrashable")]
	public partial class GoatMountItem {}
	[Tag("MediumTrashable")]
	public partial class GoldBarItem {}
	[Tag("SmallTrashable")]
	public partial class GoldFlakesItem {}
	[Tag("MediumTrashable")]
	public partial class GoldFrameLandscapeItem {}
	[Tag("MediumTrashable")]
	public partial class GoldFramePortraitItem {}
	[Tag("MediumTrashable")]
	public partial class GoldFrameSquareItem {}
	[Tag("MediumTrashable")]
	public partial class GoldFrameTallItem {}
	[Tag("MediumTrashable")]
	public partial class GoldFrameWideItem {}
	[Tag("SmallTrashable")]
	public partial class GoldWiringItem {}
	[Tag("LargeTrashable")]
	public partial class GovernmentOfficeItem {}
	[Tag("SmallTrashable")]
	public partial class GrassSeedItem {}
	[Tag("MediumTrashable")]
	public partial class HandheldCameraItem {}
	[Tag("MediumTrashable")]
	public partial class HandPlowItem {}
	[Tag("MediumTrashable")]
	public partial class HangingLongSteelSignItem {}
	[Tag("MediumTrashable")]
	public partial class HangingSteelFramedSignItem {}
	[Tag("MediumTrashable")]
	public partial class HangingSteelMineSignItem {}
	[Tag("MediumTrashable")]
	public partial class HangingSteelPlainSignItem {}
	[Tag("MediumTrashable")]
	public partial class HangingSteelShopSignItem {}
	[Tag("SmallTrashable")]
	public partial class HardwoodBoardItem {}
	[Tag("MediumTrashable")]
	public partial class HardwoodHewnLogItem {}
	[Tag("MediumTrashable")]
	public partial class HardwoodLumberBenchItem {}
	[Tag("MediumTrashable")]
	public partial class HardwoodLumberChairItem {}
	[Tag("MediumTrashable")]
	public partial class HardwoodLumberDoorItem {}
	[Tag("MediumTrashable")]
	public partial class HardwoodLumberDresserItem {}
	[Tag("MediumTrashable")]
	public partial class HardwoodLumberItem {}
	[Tag("MediumTrashable")]
	public partial class HardwoodLumberTableItem {}
	[Tag("SmallTrashable")]
	public partial class HareCarcassItem {}
	[Tag("SmallTrashable")]
	public partial class HeartOfPalmItem {}
	[Tag("SmallTrashable")]
	public partial class HeartyHometownPizzaItem {}
	[Tag("SmallTrashable")]
	public partial class HeatSinkItem {}
	[Tag("SmallTrashable")]
	public partial class HeliconiaSeedItem {}
	[Tag("SmallTrashable")]
	public partial class HempMooringRopeItem {}
	[Tag("MediumTrashable")]
	public partial class HenleyItem {}
	[Tag("MediumTrashable")]
	public partial class HewnBenchItem {}
	[Tag("MediumTrashable")]
	public partial class HewnChairItem {}
	[Tag("MediumTrashable")]
	public partial class HewnDoorItem {}
	[Tag("MediumTrashable")]
	public partial class HewnDresserItem {}
	[Tag("MediumTrashable")]
	public partial class HewnHardwoodBenchItem {}
	[Tag("MediumTrashable")]
	public partial class HewnHardwoodChairItem {}
	[Tag("MediumTrashable")]
	public partial class HewnHardwoodDoorItem {}
	[Tag("MediumTrashable")]
	public partial class HewnHardwoodTableItem {}
	[Tag("MediumTrashable")]
	public partial class HewnLogItem {}
	[Tag("MediumTrashable")]
	public partial class HewnNightstandItem {}
	[Tag("MediumTrashable")]
	public partial class HewnSoftwoodBenchItem {}
	[Tag("MediumTrashable")]
	public partial class HewnSoftwoodChairItem {}
	[Tag("MediumTrashable")]
	public partial class HewnSoftwoodDoorItem {}
	[Tag("MediumTrashable")]
	public partial class HewnSoftwoodTableItem {}
	[Tag("MediumTrashable")]
	public partial class HewnTableItem {}
	[Tag("SmallTrashable")]
	public partial class HideAshFertilizerItem {}
	[Tag("SmallTrashable")]
	public partial class HosomakiItem {}
	[Tag("SmallTrashable")]
	public partial class HuckleberriesItem {}
	[Tag("SmallTrashable")]
	public partial class HuckleberryExtractItem {}
	[Tag("SmallTrashable")]
	public partial class HuckleberryFritterItem {}
	[Tag("SmallTrashable")]
	public partial class HuckleberryPieItem {}
	[Tag("SmallTrashable")]
	public partial class HuckleberrySeedItem {}
	[Tag("MediumTrashable")]
	public partial class HugeBannerSignItem {}
	[Tag("MediumTrashable")]
	public partial class HugeIronBannerSignItem {}
	[Tag("MediumTrashable")]
	public partial class HugeIronSignItem {}
	[Tag("MediumTrashable")]
	public partial class HugeSteelBannerSignItem {}
	[Tag("MediumTrashable")]
	public partial class HugeSteelSignItem {}
	[Tag("MediumTrashable")]
	public partial class HugeWoodenBannerSignItem {}
	[Tag("SmallTrashable")]
	public partial class HuntingUpgradeItem {}
	[Tag("MediumTrashable")]
	public partial class IceboxItem {}
	[Tag("MediumTrashable")]
	public partial class ImmigrationDeskItem {}
	[Tag("LargeTrashable")]
	public partial class IndustrialBargeItem {}
	[Tag("LargeTrashable")]
	public partial class IndustrialElevatorItem {}
	[Tag("LargeTrashable")]
	public partial class IndustrialGeneratorItem {}
	[Tag("LargeTrashable")]
	public partial class IndustrialRefrigeratorItem {}
	[Tag("SmallTrashable")]
	public partial class IndustrySkillBook {}
	[Tag("SmallTrashable")]
	public partial class IndustrySkillScroll {}
	[Tag("SmallTrashable")]
	public partial class IndustryUpgradeItem {}
	[Tag("SmallTrashable")]
	public partial class InfusedOilItem {}
	[Tag("LargeTrashable")]
	public partial class IronAnchorItem {}
	[Tag("MediumTrashable")]
	public partial class IronAxeItem {}
	[Tag("SmallTrashable")]
	public partial class IronAxleItem {}
	[Tag("MediumTrashable")]
	public partial class IronBarItem {}
	[Tag("MediumTrashable")]
	public partial class IronFrameLandscapeItem {}
	[Tag("MediumTrashable")]
	public partial class IronFramePortraitItem {}
	[Tag("MediumTrashable")]
	public partial class IronFrameSquareItem {}
	[Tag("MediumTrashable")]
	public partial class IronFrameTallItem {}
	[Tag("MediumTrashable")]
	public partial class IronFrameWideItem {}
	[Tag("SmallTrashable")]
	public partial class IronGearItem {}
	[Tag("MediumTrashable")]
	public partial class IronHammerItem {}
	[Tag("MediumTrashable")]
	public partial class IronHoeItem {}
	[Tag("MediumTrashable")]
	public partial class IronHullSheetItem {}
	[Tag("MediumTrashable")]
	public partial class IronMacheteItem {}
	[Tag("MediumTrashable")]
	public partial class IronPickaxeItem {}
	[Tag("MediumTrashable")]
	public partial class IronPipeItem {}
	[Tag("SmallTrashable")]
	public partial class IronPlateItem {}
	[Tag("MediumTrashable")]
	public partial class IronRoadToolItem {}
	[Tag("MediumTrashable")]
	public partial class IronRockDrillItem {}
	[Tag("SmallTrashable")]
	public partial class IronSawBladeItem {}
	[Tag("MediumTrashable")]
	public partial class IronShovelItem {}
	[Tag("MediumTrashable")]
	public partial class IronSickleItem {}
	[Tag("SmallTrashable")]
	public partial class IronWheelItem {}
	[Tag("MediumTrashable")]
	public partial class JaguarCarcassItem {}
	[Tag("LargeTrashable")]
	public partial class JawCrusherItem {}
	[Tag("SmallTrashable")]
	public partial class JointfirSeedItem {}
	[Tag("MediumTrashable")]
	public partial class JoshuaLogItem {}
	[Tag("SmallTrashable")]
	public partial class JoshuaSeedItem {}
	[Tag("SmallTrashable")]
	public partial class JungleCampfireSaladItem {}
	[Tag("SmallTrashable")]
	public partial class JungleCampfireStewItem {}
	[Tag("SmallTrashable")]
	public partial class KelpItem {}
	[Tag("SmallTrashable")]
	public partial class KelpyCrabRollItem {}
	[Tag("LargeTrashable")]
	public partial class KilnItem {}
	[Tag("SmallTrashable")]
	public partial class KingFernSporeItem {}
	[Tag("LargeTrashable")]
	public partial class KitchenItem {}
	[Tag("MediumTrashable")]
	public partial class LaboratoryItem {}
	[Tag("MediumTrashable")]
	public partial class LargeBathMatItem {}
	[Tag("LargeTrashable")]
	public partial class LargeCanoeItem {}
	[Tag("LargeTrashable")]
	public partial class LargeCorrugatedSteelDoorItem {}
	[Tag("MediumTrashable")]
	public partial class LargeCottonBuntingFestiveItem {}
	[Tag("MediumTrashable")]
	public partial class LargeCottonBuntingRedItem {}
	[Tag("MediumTrashable")]
	public partial class LargeCottonBuntingRegalItem {}
	[Tag("MediumTrashable")]
	public partial class LargeCottonBuntingVibrantItem {}
	[Tag("LargeTrashable")]
	public partial class LargeCourtItem {}
	[Tag("MediumTrashable")]
	public partial class LargeFestivePaperLanternItem {}
	[Tag("MediumTrashable")]
	public partial class LargeHangingAshlarBasaltSignItem {}
	[Tag("MediumTrashable")]
	public partial class LargeHangingAshlarGneissSignItem {}
	[Tag("MediumTrashable")]
	public partial class LargeHangingAshlarGraniteSignItem {}
	[Tag("MediumTrashable")]
	public partial class LargeHangingAshlarLimestoneSignItem {}
	[Tag("MediumTrashable")]
	public partial class LargeHangingAshlarSandstoneSignItem {}
	[Tag("MediumTrashable")]
	public partial class LargeHangingAshlarShaleSignItem {}
	[Tag("MediumTrashable")]
	public partial class LargeHangingAshlarStoneSignItem {}
	[Tag("MediumTrashable")]
	public partial class LargeHangingBirchSignItem {}
	[Tag("MediumTrashable")]
	public partial class LargeHangingCedarSignItem {}
	[Tag("MediumTrashable")]
	public partial class LargeHangingCeibaSignItem {}
	[Tag("MediumTrashable")]
	public partial class LargeHangingFirSignItem {}
	[Tag("MediumTrashable")]
	public partial class LargeHangingHardwoodSignItem {}
	[Tag("MediumTrashable")]
	public partial class LargeHangingJoshuaSignItem {}
	[Tag("MediumTrashable")]
	public partial class LargeHangingLumberSignItem {}
	[Tag("MediumTrashable")]
	public partial class LargeHangingMortaredGraniteSignItem {}
	[Tag("MediumTrashable")]
	public partial class LargeHangingMortaredLimestoneSignItem {}
	[Tag("MediumTrashable")]
	public partial class LargeHangingMortaredSandstoneSignItem {}
	[Tag("MediumTrashable")]
	public partial class LargeHangingMortaredStoneSignItem {}
	[Tag("MediumTrashable")]
	public partial class LargeHangingOakSignItem {}
	[Tag("MediumTrashable")]
	public partial class LargeHangingPalmSignItem {}
	[Tag("MediumTrashable")]
	public partial class LargeHangingRedwoodSignItem {}
	[Tag("MediumTrashable")]
	public partial class LargeHangingSaguaroSignItem {}
	[Tag("MediumTrashable")]
	public partial class LargeHangingSoftwoodSignItem {}
	[Tag("MediumTrashable")]
	public partial class LargeHangingSpruceSignItem {}
	[Tag("MediumTrashable")]
	public partial class LargeHangingStoneSignItem {}
	[Tag("LargeTrashable")]
	public partial class LargeLumberDoorItem {}
	[Tag("LargeTrashable")]
	public partial class LargeLumberStockpileItem {}
	[Tag("LargeTrashable")]
	public partial class LargeMetalShipFrameItem {}
	[Tag("MediumTrashable")]
	public partial class LargeNylonBuntingGreenItem {}
	[Tag("MediumTrashable")]
	public partial class LargeNylonBuntingPurpleItem {}
	[Tag("MediumTrashable")]
	public partial class LargeNylonBuntingYellowItem {}
	[Tag("MediumTrashable")]
	public partial class LargePaperLanternItem {}
	[Tag("MediumTrashable")]
	public partial class LargeStandingAshlarBasaltSignItem {}
	[Tag("MediumTrashable")]
	public partial class LargeStandingAshlarGneissSignItem {}
	[Tag("MediumTrashable")]
	public partial class LargeStandingAshlarGraniteSignItem {}
	[Tag("MediumTrashable")]
	public partial class LargeStandingAshlarLimestoneSignItem {}
	[Tag("MediumTrashable")]
	public partial class LargeStandingAshlarSandstoneSignItem {}
	[Tag("MediumTrashable")]
	public partial class LargeStandingAshlarShaleSignItem {}
	[Tag("MediumTrashable")]
	public partial class LargeStandingAshlarStoneSignItem {}
	[Tag("MediumTrashable")]
	public partial class LargeStandingBirchSignItem {}
	[Tag("MediumTrashable")]
	public partial class LargeStandingCedarSignItem {}
	[Tag("MediumTrashable")]
	public partial class LargeStandingCeibaSignItem {}
	[Tag("MediumTrashable")]
	public partial class LargeStandingFirSignItem {}
	[Tag("MediumTrashable")]
	public partial class LargeStandingHardwoodSignItem {}
	[Tag("MediumTrashable")]
	public partial class LargeStandingJoshuaSignItem {}
	[Tag("MediumTrashable")]
	public partial class LargeStandingLumberSignItem {}
	[Tag("MediumTrashable")]
	public partial class LargeStandingMortaredGraniteSignItem {}
	[Tag("MediumTrashable")]
	public partial class LargeStandingMortaredLimestoneSignItem {}
	[Tag("MediumTrashable")]
	public partial class LargeStandingMortaredSandstoneSignItem {}
	[Tag("MediumTrashable")]
	public partial class LargeStandingMortaredStoneSignItem {}
	[Tag("MediumTrashable")]
	public partial class LargeStandingOakSignItem {}
	[Tag("MediumTrashable")]
	public partial class LargeStandingPalmSignItem {}
	[Tag("MediumTrashable")]
	public partial class LargeStandingRedwoodSignItem {}
	[Tag("MediumTrashable")]
	public partial class LargeStandingSaguaroSignItem {}
	[Tag("MediumTrashable")]
	public partial class LargeStandingSoftwoodSignItem {}
	[Tag("MediumTrashable")]
	public partial class LargeStandingSpruceSignItem {}
	[Tag("MediumTrashable")]
	public partial class LargeStandingStoneSignItem {}
	[Tag("LargeTrashable")]
	public partial class LargeWindowedLumberDoorItem {}
	[Tag("LargeTrashable")]
	public partial class LargeWoodenShipFrameItem {}
	[Tag("LargeTrashable")]
	public partial class LaserItem {}
	[Tag("LargeTrashable")]
	public partial class LatheItem {}
	[Tag("MediumTrashable")]
	public partial class LatrineItem {}
	[Tag("SmallTrashable")]
	public partial class LatticeMushroomSporesItem {}
	[Tag("SmallTrashable")]
	public partial class LeatherHideItem {}
	[Tag("SmallTrashable")]
	public partial class LeavenedDoughItem {}
	[Tag("MediumTrashable")]
	public partial class LightBackpackItem {}
	[Tag("SmallTrashable")]
	public partial class LightBulbItem {}
	[Tag("LargeTrashable")]
	public partial class LimestoneBisonStatueItem {}
	[Tag("MediumTrashable")]
	public partial class LimestoneOtterStatueItem {}
	[Tag("MediumTrashable")]
	public partial class LimestoneOwlStatueItem {}
	[Tag("MediumTrashable")]
	public partial class LimestoneWolfStatueItem {}
	[Tag("SmallTrashable")]
	public partial class LinenFabricItem {}
	[Tag("SmallTrashable")]
	public partial class LinenYarnItem {}
	[Tag("SmallTrashable")]
	public partial class LoadedTaroFriesItem {}
	[Tag("SmallTrashable")]
	public partial class LoggingAdvancedUpgradeItem {}
	[Tag("SmallTrashable")]
	public partial class LoggingBasicUpgradeItem {}
	[Tag("LargeTrashable")]
	public partial class LoomItem {}
	[Tag("MediumTrashable")]
	public partial class LowTopShoesItem {}
	[Tag("MediumTrashable")]
	public partial class LumberBenchItem {}
	[Tag("MediumTrashable")]
	public partial class LumberChairItem {}
	[Tag("MediumTrashable")]
	public partial class LumberDoorItem {}
	[Tag("MediumTrashable")]
	public partial class LumberDresserItem {}
	[Tag("MediumTrashable")]
	public partial class LumberItem {}
	[Tag("LargeTrashable")]
	public partial class LumberStockpileItem {}
	[Tag("MediumTrashable")]
	public partial class LumberTableItem {}
	[Tag("SmallTrashable")]
	public partial class LupineSeedItem {}
	[Tag("SmallTrashable")]
	public partial class MacaronsItem {}
	[Tag("MediumTrashable")]
	public partial class MachinistTableItem {}
	[Tag("SmallTrashable")]
	public partial class MasonryAdvancedUpgradeItem {}
	[Tag("SmallTrashable")]
	public partial class MasonryBasicUpgradeItem {}
	[Tag("SmallTrashable")]
	public partial class MasonrySkillBook {}
	[Tag("SmallTrashable")]
	public partial class MasonrySkillScroll {}
	[Tag("MediumTrashable")]
	public partial class MasonryTableItem {}
	[Tag("SmallTrashable")]
	public partial class MeatPieItem {}
	[Tag("SmallTrashable")]
	public partial class MeatStockItem {}
	[Tag("SmallTrashable")]
	public partial class MeatyStewItem {}
	[Tag("LargeTrashable")]
	public partial class MechanicalWaterPumpItem {}
	[Tag("SmallTrashable")]
	public partial class MechanicsAdvancedUpgradeItem {}
	[Tag("SmallTrashable")]
	public partial class MechanicsModernUpgradeItem {}
	[Tag("SmallTrashable")]
	public partial class MechanicsSkillBook {}
	[Tag("SmallTrashable")]
	public partial class MechanicsSkillScroll {}
	[Tag("LargeTrashable")]
	public partial class MediumFishingTrawlerItem {}
	[Tag("LargeTrashable")]
	public partial class MediumMetalShipFrameItem {}
	[Tag("LargeTrashable")]
	public partial class MediumShipyardItem {}
	[Tag("LargeTrashable")]
	public partial class MediumWoodenShipFrameItem {}
	[Tag("LargeTrashable")]
	public partial class MetalKeelItem {}
	[Tag("SmallTrashable")]
	public partial class MetallurgyResearchPaperAdvancedItem {}
	[Tag("SmallTrashable")]
	public partial class MetallurgyResearchPaperBasicItem {}
	[Tag("SmallTrashable")]
	public partial class MetallurgyResearchPaperModernItem {}
	[Tag("MediumTrashable")]
	public partial class MetalRudderItem {}
	[Tag("SmallTrashable")]
	public partial class MillingSkillBook {}
	[Tag("SmallTrashable")]
	public partial class MillingSkillScroll {}
	[Tag("SmallTrashable")]
	public partial class MillingUpgradeItem {}
	[Tag("SmallTrashable")]
	public partial class MillionairesSaladItem {}
	[Tag("LargeTrashable")]
	public partial class MillItem {}
	[Tag("SmallTrashable")]
	public partial class MiningAdvancedUpgradeItem {}
	[Tag("SmallTrashable")]
	public partial class MiningBasicUpgradeItem {}
	[Tag("SmallTrashable")]
	public partial class MiningModernUpgradeItem {}
	[Tag("LargeTrashable")]
	public partial class MintItem {}
	[Tag("SmallTrashable")]
	public partial class MochiItem {}
	[Tag("MediumTrashable")]
	public partial class ModernAxeItem {}
	[Tag("LargeTrashable")]
	public partial class ModernDoubleStreetLightItem {}
	[Tag("MediumTrashable")]
	public partial class ModernHammerItem {}
	[Tag("MediumTrashable")]
	public partial class ModernHoeItem {}
	[Tag("MediumTrashable")]
	public partial class ModernMacheteItem {}
	[Tag("MediumTrashable")]
	public partial class ModernPickaxeItem {}
	[Tag("MediumTrashable")]
	public partial class ModernRockDrillItem {}
	[Tag("MediumTrashable")]
	public partial class ModernScytheItem {}
	[Tag("MediumTrashable")]
	public partial class ModernShovelItem {}
	[Tag("LargeTrashable")]
	public partial class ModernStreetLightItem {}
	[Tag("SmallTrashable")]
	public partial class ModernUpgradeLvl1Item {}
	[Tag("SmallTrashable")]
	public partial class ModernUpgradeLvl2Item {}
	[Tag("SmallTrashable")]
	public partial class ModernUpgradeLvl3Item {}
	[Tag("SmallTrashable")]
	public partial class ModernUpgradeLvl4Item {}
	[Tag("SmallTrashable")]
	public partial class MoonJellyfishItem {}
	[Tag("MediumTrashable")]
	public partial class MortaredGraniteBenchItem {}
	[Tag("MediumTrashable")]
	public partial class MortaredGraniteChairItem {}
	[Tag("MediumTrashable")]
	public partial class MortaredGraniteDoorItem {}
	[Tag("LargeTrashable")]
	public partial class MortaredGraniteFireplaceItem {}
	[Tag("MediumTrashable")]
	public partial class MortaredGraniteItem {}
	[Tag("MediumTrashable")]
	public partial class MortaredGraniteTableItem {}
	[Tag("MediumTrashable")]
	public partial class MortaredLimestoneBenchItem {}
	[Tag("MediumTrashable")]
	public partial class MortaredLimestoneChairItem {}
	[Tag("MediumTrashable")]
	public partial class MortaredLimestoneDoorItem {}
	[Tag("LargeTrashable")]
	public partial class MortaredLimestoneFireplaceItem {}
	[Tag("MediumTrashable")]
	public partial class MortaredLimestoneItem {}
	[Tag("MediumTrashable")]
	public partial class MortaredLimestoneTableItem {}
	[Tag("MediumTrashable")]
	public partial class MortaredSandstoneBenchItem {}
	[Tag("MediumTrashable")]
	public partial class MortaredSandstoneChairItem {}
	[Tag("MediumTrashable")]
	public partial class MortaredSandstoneDoorItem {}
	[Tag("LargeTrashable")]
	public partial class MortaredSandstoneFireplaceItem {}
	[Tag("MediumTrashable")]
	public partial class MortaredSandstoneItem {}
	[Tag("MediumTrashable")]
	public partial class MortaredSandstoneTableItem {}
	[Tag("MediumTrashable")]
	public partial class MortaredStoneBenchItem {}
	[Tag("MediumTrashable")]
	public partial class MortaredStoneChairItem {}
	[Tag("MediumTrashable")]
	public partial class MortaredStoneDoorItem {}
	[Tag("LargeTrashable")]
	public partial class MortaredStoneFireplaceItem {}
	[Tag("MediumTrashable")]
	public partial class MortaredStoneItem {}
	[Tag("MediumTrashable")]
	public partial class MortaredStoneTableItem {}
	[Tag("SmallTrashable")]
	public partial class MortarItem {}
	[Tag("MediumTrashable")]
	public partial class MountainGoatCarcassItem {}
	[Tag("MediumTrashable")]
	public partial class MuffinHatItem {}
	[Tag("SmallTrashable")]
	public partial class NailItem {}
	[Tag("MediumTrashable")]
	public partial class NylonCarpetItem {}
	[Tag("MediumTrashable")]
	public partial class NylonCurtainsItem {}
	[Tag("SmallTrashable")]
	public partial class NylonFabricItem {}
	[Tag("MediumTrashable")]
	public partial class NylonFutonBedItem {}
	[Tag("MediumTrashable")]
	public partial class NylonFutonCouchItem {}
	[Tag("SmallTrashable")]
	public partial class NylonItem {}
	[Tag("SmallTrashable")]
	public partial class NylonMooringRopeItem {}
	[Tag("SmallTrashable")]
	public partial class NylonThreadItem {}
	[Tag("MediumTrashable")]
	public partial class NylonTrawlerNetItem {}
	[Tag("MediumTrashable")]
	public partial class OakLogItem {}
	[Tag("SmallTrashable")]
	public partial class OceanSpraySeedItem {}
	[Tag("SmallTrashable")]
	public partial class OilDrillingSkillBook {}
	[Tag("SmallTrashable")]
	public partial class OilDrillingSkillScroll {}
	[Tag("SmallTrashable")]
	public partial class OilDrillingUpgradeItem {}
	[Tag("MediumTrashable")]
	public partial class OilHeadLampItem {}
	[Tag("SmallTrashable")]
	public partial class OilItem {}
	[Tag("SmallTrashable")]
	public partial class OilPaintItem {}
	[Tag("LargeTrashable")]
	public partial class OilRefineryItem {}
	[Tag("MediumTrashable")]
	public partial class OrangeVaseItem {}
	[Tag("SmallTrashable")]
	public partial class OrchidItem {}
	[Tag("SmallTrashable")]
	public partial class OrchidSeedItem {}
	[Tag("MediumTrashable")]
	public partial class OrnateGoldFrameLandscapeItem {}
	[Tag("MediumTrashable")]
	public partial class OrnateGoldFramePortraitItem {}
	[Tag("MediumTrashable")]
	public partial class OrnateGoldFrameSquareItem {}
	[Tag("MediumTrashable")]
	public partial class OrnateGoldFrameTallItem {}
	[Tag("MediumTrashable")]
	public partial class OrnateGoldFrameWideItem {}
	[Tag("MediumTrashable")]
	public partial class OrnateIronFrameLandscapeItem {}
	[Tag("MediumTrashable")]
	public partial class OrnateIronFramePortraitItem {}
	[Tag("MediumTrashable")]
	public partial class OrnateIronFrameSquareItem {}
	[Tag("MediumTrashable")]
	public partial class OrnateIronFrameTallItem {}
	[Tag("MediumTrashable")]
	public partial class OrnateIronFrameWideItem {}
	[Tag("MediumTrashable")]
	public partial class OrnateWoodenFrameLandscapeItem {}
	[Tag("MediumTrashable")]
	public partial class OrnateWoodenFramePortraitItem {}
	[Tag("MediumTrashable")]
	public partial class OrnateWoodenFrameSquareItem {}
	[Tag("MediumTrashable")]
	public partial class OrnateWoodenFrameTallItem {}
	[Tag("MediumTrashable")]
	public partial class OrnateWoodenFrameWideItem {}
	[Tag("SmallTrashable")]
	public partial class OtterCarcassItem {}
	[Tag("MediumTrashable")]
	public partial class OutpostNettingItem {}
	[Tag("MediumTrashable")]
	public partial class OutpostTableItem {}
	[Tag("SmallTrashable")]
	public partial class PacificSardineItem {}
	[Tag("MediumTrashable")]
	public partial class PaddedChairItem {}
	[Tag("MediumTrashable")]
	public partial class PalmLogItem {}
	[Tag("SmallTrashable")]
	public partial class PalmSeedItem {}
	[Tag("SmallTrashable")]
	public partial class PapayaItem {}
	[Tag("SmallTrashable")]
	public partial class PapayaSeedItem {}
	[Tag("SmallTrashable")]
	public partial class PaperItem {}
	[Tag("SmallTrashable")]
	public partial class PaperMillingSkillBook {}
	[Tag("SmallTrashable")]
	public partial class PaperMillingSkillScroll {}
	[Tag("SmallTrashable")]
	public partial class PaperMillingUpgradeItem {}
	[Tag("SmallTrashable")]
	public partial class PastryDoughItem {}
	[Tag("SmallTrashable")]
	public partial class PeltFertilizerItem {}
	[Tag("MediumTrashable")]
	public partial class PetroleumItem {}
	[Tag("SmallTrashable")]
	public partial class PhadThaiItem {}
	[Tag("SmallTrashable")]
	public partial class PhosphateFertilizerItem {}
	[Tag("SmallTrashable")]
	public partial class PineappleFriendRiceItem {}
	[Tag("SmallTrashable")]
	public partial class PineappleItem {}
	[Tag("SmallTrashable")]
	public partial class PineappleSeedItem {}
	[Tag("SmallTrashable")]
	public partial class PirozhokItem {}
	[Tag("SmallTrashable")]
	public partial class PistonItem {}
	[Tag("MediumTrashable")]
	public partial class PlanterPotRoundItem {}
	[Tag("MediumTrashable")]
	public partial class PlanterPotSquareItem {}
	[Tag("SmallTrashable")]
	public partial class PlantFibersItem {}
	[Tag("MediumTrashable")]
	public partial class PlantVaseItem {}
	[Tag("MediumTrashable")]
	public partial class PlasticBuoyItem {}
	[Tag("SmallTrashable")]
	public partial class PlasticItem {}
	[Tag("SmallTrashable")]
	public partial class PokeBowlItem {}
	[Tag("LargeTrashable")]
	public partial class PortableSteamEngineItem {}
	[Tag("SmallTrashable")]
	public partial class PotterySkillBook {}
	[Tag("SmallTrashable")]
	public partial class PotterySkillScroll {}
	[Tag("SmallTrashable")]
	public partial class PotteryUpgradeItem {}
	[Tag("LargeTrashable")]
	public partial class PoweredCartItem {}
	[Tag("LargeTrashable")]
	public partial class PoweredStorageSiloItem {}
	[Tag("SmallTrashable")]
	public partial class PrairieDogCarcassItem {}
	[Tag("SmallTrashable")]
	public partial class PreparedMeatItem {}
	[Tag("SmallTrashable")]
	public partial class PricklyPearFruitItem {}
	[Tag("SmallTrashable")]
	public partial class PricklyPearSeedItem {}
	[Tag("SmallTrashable")]
	public partial class PrimeCutItem {}
	[Tag("MediumTrashable")]
	public partial class PrimitiveCanvasBootsItem {}
	[Tag("MediumTrashable")]
	public partial class PrimitiveCanvasPantsItem {}
	[Tag("MediumTrashable")]
	public partial class PrimitiveCanvasShortsItem {}
	[Tag("MediumTrashable")]
	public partial class PrimitiveFurBeltItem {}
	[Tag("MediumTrashable")]
	public partial class PrimitiveLeatherBootsItem {}
	[Tag("MediumTrashable")]
	public partial class PrimitiveLeatherPantsItem {}
	[Tag("MediumTrashable")]
	public partial class PrimitiveLeatherShirtItem {}
	[Tag("MediumTrashable")]
	public partial class PrimitiveLeatherShortsItem {}
	[Tag("LargeTrashable")]
	public partial class PrintingPressItem {}
	[Tag("SmallTrashable")]
	public partial class PulpFillerItem {}
	[Tag("LargeTrashable")]
	public partial class PumpJackItem {}
	[Tag("SmallTrashable")]
	public partial class PumpkinItem {}
	[Tag("SmallTrashable")]
	public partial class PumpkinSeedItem {}
	[Tag("SmallTrashable")]
	public partial class PupusasItem {}
	[Tag("MediumTrashable")]
	public partial class PurpleVaseItem {}
	[Tag("SmallTrashable")]
	public partial class QuicklimeItem {}
	[Tag("SmallTrashable")]
	public partial class RadiatorItem {}
	[Tag("SmallTrashable")]
	public partial class RawBaconItem {}
	[Tag("SmallTrashable")]
	public partial class RawFishItem {}
	[Tag("SmallTrashable")]
	public partial class RawMeatItem {}
	[Tag("SmallTrashable")]
	public partial class RawRoastItem {}
	[Tag("SmallTrashable")]
	public partial class RawSausageItem {}
	[Tag("LargeTrashable")]
	public partial class RealEstateDeskItem {}
	[Tag("MediumTrashable")]
	public partial class RebarItem {}
	[Tag("MediumTrashable")]
	public partial class RecurveBowItem {}
	[Tag("MediumTrashable")]
	public partial class RedVaseItem {}
	[Tag("MediumTrashable")]
	public partial class RedwoodLogItem {}
	[Tag("SmallTrashable")]
	public partial class RedwoodSeedItem {}
	[Tag("MediumTrashable")]
	public partial class RefrigeratorItem {}
	[Tag("LargeTrashable")]
	public partial class RegistrarItem {}
	[Tag("MediumTrashable")]
	public partial class ReinforcedConcreteItem {}
	[Tag("LargeTrashable")]
	public partial class ResearchTableItem {}
	[Tag("SmallTrashable")]
	public partial class RiceFlourItem {}
	[Tag("SmallTrashable")]
	public partial class RiceItem {}
	[Tag("SmallTrashable")]
	public partial class RiceNoodlesItem {}
	[Tag("SmallTrashable")]
	public partial class RivetItem {}
	[Tag("SmallTrashable")]
	public partial class RoastPumpkinItem {}
	[Tag("LargeTrashable")]
	public partial class RoboticAssemblyLineItem {}
	[Tag("MediumTrashable")]
	public partial class RockerBoxItem {}
	[Tag("LargeTrashable")]
	public partial class RollingMillItem {}
	[Tag("SmallTrashable")]
	public partial class RootCampfireSaladItem {}
	[Tag("SmallTrashable")]
	public partial class RootCampfireStewItem {}
	[Tag("MediumTrashable")]
	public partial class RubberWheelItem {}
	[Tag("LargeTrashable")]
	public partial class RugLargeItem {}
	[Tag("MediumTrashable")]
	public partial class RugMediumItem {}
	[Tag("MediumTrashable")]
	public partial class RugSmallItem {}
	[Tag("MediumTrashable")]
	public partial class RunningShoesItem {}
	[Tag("MediumTrashable")]
	public partial class SaguaroRibItem {}
	[Tag("SmallTrashable")]
	public partial class SaguaroSeedItem {}
	[Tag("SmallTrashable")]
	public partial class SalmonItem {}
	[Tag("MediumTrashable")]
	public partial class SaltBasketItem {}
	[Tag("MediumTrashable")]
	public partial class SandalsItem {}
	[Tag("LargeTrashable")]
	public partial class SawmillItem {}
	[Tag("SmallTrashable")]
	public partial class SaxifrageSeedItem {}
	[Tag("SmallTrashable")]
	public partial class ScrapMeatItem {}
	[Tag("LargeTrashable")]
	public partial class ScreeningMachineItem {}
	[Tag("LargeTrashable")]
	public partial class ScrewPressItem {}
	[Tag("SmallTrashable")]
	public partial class ScrewsItem {}
	[Tag("SmallTrashable")]
	public partial class SeagrassSeedItem {}
	[Tag("SmallTrashable")]
	public partial class SearedMeatItem {}
	[Tag("SmallTrashable")]
	public partial class SeededCamasRollItem {}
	[Tag("LargeTrashable")]
	public partial class SensorBasedBeltSorterItem {}
	[Tag("SmallTrashable")]
	public partial class SensuousSeaPizzaItem {}
	[Tag("SmallTrashable")]
	public partial class ServoItem {}
	[Tag("LargeTrashable")]
	public partial class SettlementCraftingTableItem {}
	[Tag("LargeTrashable")]
	public partial class ShaperItem {}
	[Tag("SmallTrashable")]
	public partial class SharkFilletSoupItem {}
	[Tag("MediumTrashable")]
	public partial class SheepMountItem {}
	[Tag("MediumTrashable")]
	public partial class ShelfCabinetItem {}
	[Tag("SmallTrashable")]
	public partial class ShipwrightAdvancedUpgradeItem {}
	[Tag("SmallTrashable")]
	public partial class ShipwrightBasicUpgradeItem {}
	[Tag("MediumTrashable")]
	public partial class ShipwrightHatItem {}
	[Tag("MediumTrashable")]
	public partial class ShipwrightOverallsItem {}
	[Tag("MediumTrashable")]
	public partial class ShipwrightSandalsItem {}
	[Tag("MediumTrashable")]
	public partial class ShipwrightShirtItem {}
	[Tag("SmallTrashable")]
	public partial class ShipwrightSkillBook {}
	[Tag("SmallTrashable")]
	public partial class ShipwrightSkillScroll {}
	[Tag("SmallTrashable")]
	public partial class ShornWoolItem {}
	[Tag("MediumTrashable")]
	public partial class ShortsItem {}
	[Tag("SmallTrashable")]
	public partial class SimmeredMeatItem {}
	[Tag("SmallTrashable")]
	public partial class SimpleSyrupItem {}
	[Tag("LargeTrashable")]
	public partial class SinkItem {}
	[Tag("LargeTrashable")]
	public partial class SkidSteerItem {}
	[Tag("MediumTrashable")]
	public partial class SmallBathMatItem {}
	[Tag("MediumTrashable")]
	public partial class SmallCanoeItem {}
	[Tag("MediumTrashable")]
	public partial class SmallCottonBuntingFestiveItem {}
	[Tag("MediumTrashable")]
	public partial class SmallCottonBuntingRedItem {}
	[Tag("MediumTrashable")]
	public partial class SmallCottonBuntingRegalItem {}
	[Tag("MediumTrashable")]
	public partial class SmallCottonBuntingVibrantItem {}
	[Tag("LargeTrashable")]
	public partial class SmallCourtItem {}
	[Tag("MediumTrashable")]
	public partial class SmallHangingAshlarBasaltSignItem {}
	[Tag("MediumTrashable")]
	public partial class SmallHangingAshlarGneissSignItem {}
	[Tag("MediumTrashable")]
	public partial class SmallHangingAshlarGraniteSignItem {}
	[Tag("MediumTrashable")]
	public partial class SmallHangingAshlarLimestoneSignItem {}
	[Tag("MediumTrashable")]
	public partial class SmallHangingAshlarSandstoneSignItem {}
	[Tag("MediumTrashable")]
	public partial class SmallHangingAshlarShaleSignItem {}
	[Tag("MediumTrashable")]
	public partial class SmallHangingAshlarStoneSignItem {}
	[Tag("MediumTrashable")]
	public partial class SmallHangingBirchSignItem {}
	[Tag("MediumTrashable")]
	public partial class SmallHangingCedarSignItem {}
	[Tag("MediumTrashable")]
	public partial class SmallHangingCeibaSignItem {}
	[Tag("MediumTrashable")]
	public partial class SmallHangingFirSignItem {}
	[Tag("MediumTrashable")]
	public partial class SmallHangingHardwoodSignItem {}
	[Tag("MediumTrashable")]
	public partial class SmallHangingJoshuaSignItem {}
	[Tag("MediumTrashable")]
	public partial class SmallHangingLumberSignItem {}
	[Tag("MediumTrashable")]
	public partial class SmallHangingMortaredGraniteSignItem {}
	[Tag("MediumTrashable")]
	public partial class SmallHangingMortaredLimestoneSignItem {}
	[Tag("MediumTrashable")]
	public partial class SmallHangingMortaredSandstoneSignItem {}
	[Tag("MediumTrashable")]
	public partial class SmallHangingMortaredStoneSignItem {}
	[Tag("MediumTrashable")]
	public partial class SmallHangingOakSignItem {}
	[Tag("MediumTrashable")]
	public partial class SmallHangingPalmSignItem {}
	[Tag("MediumTrashable")]
	public partial class SmallHangingRedwoodSignItem {}
	[Tag("MediumTrashable")]
	public partial class SmallHangingSaguaroSignItem {}
	[Tag("MediumTrashable")]
	public partial class SmallHangingSoftwoodSignItem {}
	[Tag("MediumTrashable")]
	public partial class SmallHangingSpruceSignItem {}
	[Tag("MediumTrashable")]
	public partial class SmallHangingStoneSignItem {}
	[Tag("MediumTrashable")]
	public partial class SmallNylonBuntingGreenItem {}
	[Tag("MediumTrashable")]
	public partial class SmallNylonBuntingPurpleItem {}
	[Tag("MediumTrashable")]
	public partial class SmallNylonBuntingYellowItem {}
	[Tag("MediumTrashable")]
	public partial class SmallPaperLanternItem {}
	[Tag("MediumTrashable")]
	public partial class SmallPaperMachineItem {}
	[Tag("LargeTrashable")]
	public partial class SmallShipyardItem {}
	[Tag("MediumTrashable")]
	public partial class SmallSinkItem {}
	[Tag("MediumTrashable")]
	public partial class SmallStandingAshlarBasaltSignItem {}
	[Tag("MediumTrashable")]
	public partial class SmallStandingAshlarGneissSignItem {}
	[Tag("MediumTrashable")]
	public partial class SmallStandingAshlarGraniteSignItem {}
	[Tag("MediumTrashable")]
	public partial class SmallStandingAshlarLimestoneSignItem {}
	[Tag("MediumTrashable")]
	public partial class SmallStandingAshlarSandstoneSignItem {}
	[Tag("MediumTrashable")]
	public partial class SmallStandingAshlarShaleSignItem {}
	[Tag("MediumTrashable")]
	public partial class SmallStandingAshlarStoneSignItem {}
	[Tag("MediumTrashable")]
	public partial class SmallStandingBirchSignItem {}
	[Tag("MediumTrashable")]
	public partial class SmallStandingCedarSignItem {}
	[Tag("MediumTrashable")]
	public partial class SmallStandingCeibaSignItem {}
	[Tag("MediumTrashable")]
	public partial class SmallStandingFirSignItem {}
	[Tag("MediumTrashable")]
	public partial class SmallStandingHardwoodSignItem {}
	[Tag("MediumTrashable")]
	public partial class SmallStandingJoshuaSignItem {}
	[Tag("MediumTrashable")]
	public partial class SmallStandingLumberSignItem {}
	[Tag("MediumTrashable")]
	public partial class SmallStandingMortaredGraniteSignItem {}
	[Tag("MediumTrashable")]
	public partial class SmallStandingMortaredLimestoneSignItem {}
	[Tag("MediumTrashable")]
	public partial class SmallStandingMortaredSandstoneSignItem {}
	[Tag("MediumTrashable")]
	public partial class SmallStandingMortaredStoneSignItem {}
	[Tag("MediumTrashable")]
	public partial class SmallStandingOakSignItem {}
	[Tag("MediumTrashable")]
	public partial class SmallStandingPalmSignItem {}
	[Tag("MediumTrashable")]
	public partial class SmallStandingRedwoodSignItem {}
	[Tag("MediumTrashable")]
	public partial class SmallStandingSaguaroSignItem {}
	[Tag("MediumTrashable")]
	public partial class SmallStandingSoftwoodSignItem {}
	[Tag("MediumTrashable")]
	public partial class SmallStandingSpruceSignItem {}
	[Tag("MediumTrashable")]
	public partial class SmallStandingStoneSignItem {}
	[Tag("MediumTrashable")]
	public partial class SmallStockpileItem {}
	[Tag("MediumTrashable")]
	public partial class SmallWoodCartItem {}
	[Tag("LargeTrashable")]
	public partial class SmallWoodenBoatItem {}
	[Tag("LargeTrashable")]
	public partial class SmallWoodenShipFrameItem {}
	[Tag("SmallTrashable")]
	public partial class SmeltingSkillBook {}
	[Tag("SmallTrashable")]
	public partial class SmeltingSkillScroll {}
	[Tag("SmallTrashable")]
	public partial class SmeltingUpgradeItem {}
	[Tag("MediumTrashable")]
	public partial class SmithApronItem {}
	[Tag("MediumTrashable")]
	public partial class SmithBandanaItem {}
	[Tag("MediumTrashable")]
	public partial class SmithBootsItem {}
	[Tag("MediumTrashable")]
	public partial class SmithGlovesItem {}
	[Tag("MediumTrashable")]
	public partial class SmithPantsItem {}
	[Tag("MediumTrashable")]
	public partial class SmithShirtItem {}
	[Tag("SmallTrashable")]
	public partial class SmoothGutNoodleRollItem {}
	[Tag("MediumTrashable")]
	public partial class SnappingTurtleCarcassItem {}
	[Tag("SmallTrashable")]
	public partial class SoftwoodBoardItem {}
	[Tag("MediumTrashable")]
	public partial class SoftwoodHewnLogItem {}
	[Tag("MediumTrashable")]
	public partial class SoftwoodLumberBenchItem {}
	[Tag("MediumTrashable")]
	public partial class SoftwoodLumberChairItem {}
	[Tag("MediumTrashable")]
	public partial class SoftwoodLumberDoorItem {}
	[Tag("MediumTrashable")]
	public partial class SoftwoodLumberDresserItem {}
	[Tag("MediumTrashable")]
	public partial class SoftwoodLumberItem {}
	[Tag("MediumTrashable")]
	public partial class SoftwoodLumberTableItem {}
	[Tag("LargeTrashable")]
	public partial class SolarGeneratorItem {}
	[Tag("SmallTrashable")]
	public partial class SpikyRollItem {}
	[Tag("LargeTrashable")]
	public partial class SpinMelterItem {}
	[Tag("SmallTrashable")]
	public partial class SpoiledFoodItem {}
	[Tag("MediumTrashable")]
	public partial class SpruceLogItem {}
	[Tag("SmallTrashable")]
	public partial class SpruceSeedItem {}
	[Tag("MediumTrashable")]
	public partial class SquareBeltItem {}
	[Tag("LargeTrashable")]
	public partial class StampMillItem {}
	[Tag("MediumTrashable")]
	public partial class StandingGlassSignItem {}
	[Tag("MediumTrashable")]
	public partial class StandingLongSteelSignItem {}
	[Tag("MediumTrashable")]
	public partial class StandingSteelBrickSignItem {}
	[Tag("LargeTrashable")]
	public partial class SteamEngineItem {}
	[Tag("MediumTrashable")]
	public partial class SteamTractorHarvesterItem {}
	[Tag("LargeTrashable")]
	public partial class SteamTractorItem {}
	[Tag("MediumTrashable")]
	public partial class SteamTractorPlowItem {}
	[Tag("MediumTrashable")]
	public partial class SteamTractorSowerItem {}
	[Tag("LargeTrashable")]
	public partial class SteamTruckItem {}
	[Tag("MediumTrashable")]
	public partial class SteelAbstractFixtureItem {}
	[Tag("MediumTrashable")]
	public partial class SteelAxeItem {}
	[Tag("SmallTrashable")]
	public partial class SteelAxleItem {}
	[Tag("MediumTrashable")]
	public partial class SteelBarItem {}
	[Tag("MediumTrashable")]
	public partial class SteelBuoyItem {}
	[Tag("MediumTrashable")]
	public partial class SteelCeilingLightItem {}
	[Tag("MediumTrashable")]
	public partial class SteelFloorLampItem {}
	[Tag("SmallTrashable")]
	public partial class SteelGearboxItem {}
	[Tag("SmallTrashable")]
	public partial class SteelGearItem {}
	[Tag("MediumTrashable")]
	public partial class SteelHammerItem {}
	[Tag("LargeTrashable")]
	public partial class SteelHangingFixtureItem {}
	[Tag("LargeTrashable")]
	public partial class SteelHangingLampItem {}
	[Tag("MediumTrashable")]
	public partial class SteelHoeItem {}
	[Tag("MediumTrashable")]
	public partial class SteelKitchenLampItem {}
	[Tag("MediumTrashable")]
	public partial class SteelMacheteItem {}
	[Tag("MediumTrashable")]
	public partial class SteelMooragePostItem {}
	[Tag("MediumTrashable")]
	public partial class SteelPickaxeItem {}
	[Tag("MediumTrashable")]
	public partial class SteelPipeItem {}
	[Tag("SmallTrashable")]
	public partial class SteelPlateItem {}
	[Tag("SmallTrashable")]
	public partial class SteelSawBladeItem {}
	[Tag("MediumTrashable")]
	public partial class SteelSearchlightItem {}
	[Tag("MediumTrashable")]
	public partial class SteelShovelItem {}
	[Tag("MediumTrashable")]
	public partial class SteelSickleItem {}
	[Tag("MediumTrashable")]
	public partial class SteelSquareFixtureItem {}
	[Tag("MediumTrashable")]
	public partial class SteelTableLampItem {}
	[Tag("MediumTrashable")]
	public partial class StenographersTableItem {}
	[Tag("MediumTrashable")]
	public partial class StockpileItem {}
	[Tag("MediumTrashable")]
	public partial class StoneAxeItem {}
	[Tag("MediumTrashable")]
	public partial class StoneBrazierItem {}
	[Tag("MediumTrashable")]
	public partial class StoneHammerItem {}
	[Tag("MediumTrashable")]
	public partial class StoneMacheteItem {}
	[Tag("MediumTrashable")]
	public partial class StonePickaxeItem {}
	[Tag("MediumTrashable")]
	public partial class StoneRoadItem {}
	[Tag("MediumTrashable")]
	public partial class StoneRoadToolItem {}
	[Tag("MediumTrashable")]
	public partial class StoneSickleItem {}
	[Tag("MediumTrashable")]
	public partial class StopSignItem {}
	[Tag("MediumTrashable")]
	public partial class StorageChestItem {}
	[Tag("LargeTrashable")]
	public partial class StorageSiloItem {}
	[Tag("MediumTrashable")]
	public partial class StoreItem {}
	[Tag("MediumTrashable")]
	public partial class StoveItem {}
	[Tag("MediumTrashable")]
	public partial class StreetlampItem {}
	[Tag("MediumTrashable")]
	public partial class StreetSignItem {}
	[Tag("LargeTrashable")]
	public partial class StuffedAlligatorItem {}
	[Tag("LargeTrashable")]
	public partial class StuffedBisonItem {}
	[Tag("LargeTrashable")]
	public partial class StuffedElkItem {}
	[Tag("LargeTrashable")]
	public partial class StuffedGoatItem {}
	[Tag("LargeTrashable")]
	public partial class StuffedJaguarItem {}
	[Tag("SmallTrashable")]
	public partial class StuffedTurkeyItem {}
	[Tag("MediumTrashable")]
	public partial class StuffedWolfItem {}
	[Tag("SmallTrashable")]
	public partial class SubstrateItem {}
	[Tag("SmallTrashable")]
	public partial class SugarcaneItem {}
	[Tag("SmallTrashable")]
	public partial class SugarItem {}
	[Tag("SmallTrashable")]
	public partial class SunCheeseItem {}
	[Tag("SmallTrashable")]
	public partial class SunflowerItem {}
	[Tag("SmallTrashable")]
	public partial class SunflowerSeedItem {}
	[Tag("SmallTrashable")]
	public partial class SweetDeerJerkyItem {}
	[Tag("SmallTrashable")]
	public partial class SweetSaladItem {}
	[Tag("MediumTrashable")]
	public partial class SwirlVaseItem {}
	[Tag("SmallTrashable")]
	public partial class SyntheticRubberItem {}
	[Tag("MediumTrashable")]
	public partial class TailorBowlerHatItem {}
	[Tag("SmallTrashable")]
	public partial class TailoringModernUpgradeItem {}
	[Tag("SmallTrashable")]
	public partial class TailoringSkillBook {}
	[Tag("SmallTrashable")]
	public partial class TailoringSkillScroll {}
	[Tag("LargeTrashable")]
	public partial class TailoringTableItem {}
	[Tag("SmallTrashable")]
	public partial class TailoringUpgradeItem {}
	[Tag("MediumTrashable")]
	public partial class TailorPantsItem {}
	[Tag("MediumTrashable")]
	public partial class TailorShirtItem {}
	[Tag("MediumTrashable")]
	public partial class TailorShoesItem {}
	[Tag("MediumTrashable")]
	public partial class TallBootsItem {}
	[Tag("MediumTrashable")]
	public partial class TallowCandleItem {}
	[Tag("SmallTrashable")]
	public partial class TallowItem {}
	[Tag("MediumTrashable")]
	public partial class TallowLampItem {}
	[Tag("MediumTrashable")]
	public partial class TallowWallLampItem {}
	[Tag("SmallTrashable")]
	public partial class TaroFriesItem {}
	[Tag("SmallTrashable")]
	public partial class TaroRootItem {}
	[Tag("SmallTrashable")]
	public partial class TaroSeedItem {}
	[Tag("SmallTrashable")]
	public partial class TastyTropicalPizzaItem {}
	[Tag("MediumTrashable")]
	public partial class TinyStockpileItem {}
	[Tag("MediumTrashable")]
	public partial class ToiletItem {}
	[Tag("SmallTrashable")]
	public partial class TomatoItem {}
	[Tag("SmallTrashable")]
	public partial class TomatoSeedItem {}
	[Tag("LargeTrashable")]
	public partial class ToolBenchItem {}
	[Tag("SmallTrashable")]
	public partial class ToppedPorridgeItem {}
	[Tag("SmallTrashable")]
	public partial class TorchItem {}
	[Tag("MediumTrashable")]
	public partial class TorchStandItem {}
	[Tag("SmallTrashable")]
	public partial class TortillaItem {}
	[Tag("MediumTrashable")]
	public partial class TowelRackItem {}
	[Tag("MediumTrashable")]
	public partial class TownFoundationItem {}
	[Tag("LargeTrashable")]
	public partial class TransmissionPoleItem {}
	[Tag("MediumTrashable")]
	public partial class TrapperFurHatItem {}
	[Tag("MediumTrashable")]
	public partial class TrapperLeatherHatItem {}
	[Tag("LargeTrashable")]
	public partial class TreasuryItem {}
	[Tag("SmallTrashable")]
	public partial class TrilliumFlowerItem {}
	[Tag("SmallTrashable")]
	public partial class TrilliumSeedItem {}
	[Tag("MediumTrashable")]
	public partial class TripodCameraItem {}
	[Tag("MediumTrashable")]
	public partial class TrousersItem {}
	[Tag("SmallTrashable")]
	public partial class TroutItem {}
	[Tag("LargeTrashable")]
	public partial class TruckItem {}
	[Tag("SmallTrashable")]
	public partial class TunaItem {}
	[Tag("MediumTrashable")]
	public partial class TunicItem {}
	[Tag("SmallTrashable")]
	public partial class TurkeyCarcassItem {}
	[Tag("MediumTrashable")]
	public partial class UpholsteredChairItem {}
	[Tag("MediumTrashable")]
	public partial class UpholsteredCouchItem {}
	[Tag("SmallTrashable")]
	public partial class UrchinItem {}
	[Tag("SmallTrashable")]
	public partial class ValveItem {}
	[Tag("SmallTrashable")]
	public partial class VegetableMedleyItem {}
	[Tag("SmallTrashable")]
	public partial class VegetableSoupItem {}
	[Tag("SmallTrashable")]
	public partial class VegetableStockItem {}
	[Tag("MediumTrashable")]
	public partial class VillagersCowlLoweredItem {}
	[Tag("MediumTrashable")]
	public partial class VillagersCowlRaisedItem {}
	[Tag("MediumTrashable")]
	public partial class WainwrightTableItem {}
	[Tag("MediumTrashable")]
	public partial class WallCandleItem {}
	[Tag("MediumTrashable")]
	public partial class WanderersHoodLoweredItem {}
	[Tag("MediumTrashable")]
	public partial class WanderersHoodRaisedItem {}
	[Tag("MediumTrashable")]
	public partial class WashboardItem {}
	[Tag("MediumTrashable")]
	public partial class WashingMachineItem {}
	[Tag("LargeTrashable")]
	public partial class WasteFilterItem {}
	[Tag("SmallTrashable")]
	public partial class WaterweedSeedItem {}
	[Tag("LargeTrashable")]
	public partial class WaterwheelItem {}
	[Tag("SmallTrashable")]
	public partial class WetBrickItem {}
	[Tag("SmallTrashable")]
	public partial class WheatItem {}
	[Tag("SmallTrashable")]
	public partial class WheatSeedItem {}
	[Tag("MediumTrashable")]
	public partial class WheelbarrowItem {}
	[Tag("SmallTrashable")]
	public partial class WhiteBursageSeedItem {}
	[Tag("SmallTrashable")]
	public partial class WildMixItem {}
	[Tag("SmallTrashable")]
	public partial class WildStewItem {}
	[Tag("SmallTrashable")]
	public partial class WiltedFiddleheadsItem {}
	[Tag("LargeTrashable")]
	public partial class WindmillItem {}
	[Tag("LargeTrashable")]
	public partial class WindTurbineItem {}
	[Tag("MediumTrashable")]
	public partial class WolfCarcassItem {}
	[Tag("MediumTrashable")]
	public partial class WoodCartItem {}
	[Tag("LargeTrashable")]
	public partial class WoodenBargeItem {}
	[Tag("MediumTrashable")]
	public partial class WoodenBowItem {}
	[Tag("MediumTrashable")]
	public partial class WoodenCeilingLightItem {}
	[Tag("LargeTrashable")]
	public partial class WoodenElevatorItem {}
	[Tag("LargeTrashable")]
	public partial class WoodenFabricBedItem {}
	[Tag("MediumTrashable")]
	public partial class WoodenFloorLampItem {}
	[Tag("MediumTrashable")]
	public partial class WoodenFrameLandscapeItem {}
	[Tag("MediumTrashable")]
	public partial class WoodenFramePortraitItem {}
	[Tag("MediumTrashable")]
	public partial class WoodenFrameSquareItem {}
	[Tag("MediumTrashable")]
	public partial class WoodenFrameTallItem {}
	[Tag("MediumTrashable")]
	public partial class WoodenFrameWideItem {}
	[Tag("MediumTrashable")]
	public partial class WoodenHoeItem {}
	[Tag("SmallTrashable")]
	public partial class WoodenHullPlanksItem {}
	[Tag("LargeTrashable")]
	public partial class WoodenKeelItem {}
	[Tag("MediumTrashable")]
	public partial class WoodenKitchenLampItem {}
	[Tag("SmallTrashable")]
	public partial class WoodenMoldItem {}
	[Tag("MediumTrashable")]
	public partial class WoodenMooragePostItem {}
	[Tag("SmallTrashable")]
	public partial class WoodenOarItem {}
	[Tag("SmallTrashable")]
	public partial class WoodenRudderItem {}
	[Tag("MediumTrashable")]
	public partial class WoodenShovelItem {}
	[Tag("MediumTrashable")]
	public partial class WoodenStrawBedItem {}
	[Tag("MediumTrashable")]
	public partial class WoodenTableLampItem {}
	[Tag("LargeTrashable")]
	public partial class WoodenTransportShipItem {}
	[Tag("SmallTrashable")]
	public partial class WoodenWheelItem {}
	[Tag("SmallTrashable")]
	public partial class WoodPulpItem {}
	[Tag("LargeTrashable")]
	public partial class WoodShopCartItem {}
	[Tag("MediumTrashable")]
	public partial class WoolCarpetItem {}
	[Tag("MediumTrashable")]
	public partial class WoolCurtainsItem {}
	[Tag("SmallTrashable")]
	public partial class WoolFabricItem {}
	[Tag("SmallTrashable")]
	public partial class WoolYarnItem {}
	[Tag("MediumTrashable")]
	public partial class WorkBackpackItem {}
	[Tag("MediumTrashable")]
	public partial class WorkbenchItem {}
	[Tag("MediumTrashable")]
	public partial class WorkBootsItem {}
	[Tag("SmallTrashable")]
	public partial class WorldlyDonutItem {}
	[Tag("SmallTrashable")]
	public partial class YeastItem {}
	[Tag("LargeTrashable")]
	public partial class ZoningOfficeItem {}
}
