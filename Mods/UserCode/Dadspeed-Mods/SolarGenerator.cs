﻿namespace Eco.Mods.TechTree
{
    using System.Collections.Generic;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.Objects;
    using Eco.Gameplay.Items;
    using Eco.Shared.Localization;
    using Eco.Gameplay.Items.Recipes;

    public partial class SolarGeneratorRecipe
    {
        partial void ModsPreInitialize()
        {
            /// Make recipe a lot more expensive
            /// Remove bonus from Upgrades/Skill Level
            this.Recipes[0].Ingredients.Clear();
            this.Recipes[0].Ingredients.AddRange(new List<IngredientElement> {
               new IngredientElement(typeof(SteelPlateItem), 32, true),
               new IngredientElement(typeof(ServoItem), 16, true),
               new IngredientElement(typeof(BasicCircuitItem), 12, true),
                /// Add Framed Glass to make it more expensive and give some extra business to Glass Working
               new IngredientElement(typeof(FramedGlassItem), 24, true),
               new IngredientElement(typeof(GoldWiringItem), 24, true),
            });

            /// Increase Exp by 2x to compensate slightly for increased costs
            this.ExperienceOnCraft *= 2;

            /// Increase calories required by 3x
            this.LaborInCalories = CreateLaborInCaloriesValue(1800, typeof(ElectronicsSkill));

            /// Increase craft time by 2x
            this.CraftMinutes = CreateCraftTimeValue(beneficiary: typeof(SolarGeneratorRecipe), start: 40, skillType: typeof(ElectronicsSkill), typeof(ElectronicsFocusedSpeedTalent), typeof(ElectronicsParallelSpeedTalent));
        }
    }
}
