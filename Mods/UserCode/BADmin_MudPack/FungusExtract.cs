﻿namespace Eco.Mods.TechTree 
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using Eco.Gameplay.Blocks;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.DynamicValues;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Objects;
    using Eco.Gameplay.Players;
    using Eco.Gameplay.Skills;
    using Eco.Gameplay.Settlements;
    using Eco.Gameplay.Systems;
    using Eco.Gameplay.Systems.TextLinks;
    using Eco.Shared.Localization;
    using Eco.Shared.Serialization;
    using Eco.Shared.Utils;
    using Eco.Core.Items;
    using Eco.World;
    using Eco.World.Blocks;
    using Eco.Gameplay.Pipes;
    using Eco.Core.Controller;
    using Eco.Gameplay.Items.Recipes;

    [Serialized]
    [LocDisplayName("Fungus Extract")] 
    [Weight(100)] 
    [Ecopedia("Items", "Products", createAsSubPage: true)]
    [LocDescription("A potent fungus extract")]
    public partial class FungusExtractItem : Item
    {
        
    }

    [Ecopedia("Items", "Products", createAsSubPage: true)]
    [RequiresSkill(typeof(CuttingEdgeCookingSkill), 1)]
    public partial class FungusExtractRecipe : RecipeFamily {
        public FungusExtractRecipe() {
            var recipe = new Recipe();
            var name = "FungusExtract";
            LocString displayName = Localizer.DoStr("Fungus Extract");

            List<IngredientElement> ingredients = new List<IngredientElement>();
            List<CraftingElement> items = new List<CraftingElement>();
            this.Recipes = new List<Recipe>();

            // What do we need?
            ingredients.Add(new IngredientElement("Fungus", 5 ,true));

            // What do we get?
            items.Add(new CraftingElement<FungusExtractItem>(1));

            // initalize recipe values
            recipe.Init(name,displayName,ingredients,items);

            // add the recipe to the recipe family 
            this.Recipes.Add(recipe);

            // Defines the amount of labor required and the required skill to add labor
            this.LaborInCalories = CreateLaborInCaloriesValue(500, typeof(CuttingEdgeCookingSkill));

            // Defines our crafting time for the recipe
            this.CraftMinutes = CreateCraftTimeValue(beneficiary: typeof(FungusExtractRecipe), start: 3, skillType: typeof(CuttingEdgeCookingSkill), typeof(CuttingEdgeCookingFocusedSpeedTalent), typeof(CuttingEdgeCookingParallelSpeedTalent));

            // XPs
            this.ExperienceOnCraft = 5; 

            // Perform pre/post initialization for user mods and initialize our recipe instance with the display name "Charred Mortar"
            this.ModsPreInitialize();
            this.Initialize(displayText: Localizer.DoStr("Fungus Extract"), recipeType: typeof(FungusExtractRecipe));
            this.ModsPostInitialize();

            // Register our RecipeFamily instance with the crafting system so it can be crafted.
            CraftingComponent.AddRecipe(tableType: typeof(LaboratoryObject), recipe: this);
        }
        /// <summary>Hook for mods to customize RecipeFamily before initialization. You can change recipes, xp, labor, time here.</summary>
        partial void ModsPreInitialize();

        /// <summary>Hook for mods to customize RecipeFamily after initialization, but before registration. You can change skill requirements here.</summary>
        partial void ModsPostInitialize();
    }
    
}