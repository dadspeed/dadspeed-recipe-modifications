﻿namespace Eco.Mods.TechTree
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using Eco.Gameplay.Blocks;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.DynamicValues;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Objects;
    using Eco.Gameplay.Players;
    using Eco.Gameplay.Skills;
    using Eco.Gameplay.Settlements;
    using Eco.Gameplay.Systems;
    using Eco.Gameplay.Systems.TextLinks;
    using Eco.Shared.Localization;
    using Eco.Shared.Serialization;
    using Eco.Shared.Utils;
    using Eco.Core.Items;
    using Eco.World;
    using Eco.World.Blocks;
    using Eco.Gameplay.Pipes;
    using Eco.Core.Controller;
    using Eco.Gameplay.Items.Recipes;

    [Ecopedia("Items", "Products", createAsSubPage: true)]
    [RequiresSkill(typeof(AdvancedSmeltingSkill), 2)]
    public partial class AdvancedLiquifactedCoalRecipe : RecipeFamily {
        public AdvancedLiquifactedCoalRecipe() {
            var recipe = new Recipe();
            var name = "AdvancedLiquifactedCoal";
            LocString displayName = Localizer.DoStr("Advanced Liquifacted Coal");

            List<IngredientElement> ingredients = new List<IngredientElement>();
            List<CraftingElement> items = new List<CraftingElement>();
            this.Recipes = new List<Recipe>();

            // What do we need?
            ingredients.Add(new IngredientElement("Coal", 7, typeof(AdvancedSmeltingSkill), typeof(AdvancedSmeltingLavishResourcesTalent)));
            ingredients.Add(new IngredientElement(typeof(QuicklimeItem), 1 , true));

            // What do we get?
            items.Add(new CraftingElement<LiquifactedCoalItem>(4));

            // initalize recipe values
            recipe.Init(name,displayName,ingredients,items);

            // add the recipe to the recipe family 
            this.Recipes.Add(recipe);

            // Defines the amount of labor required and the required skill to add labor
            this.LaborInCalories = CreateLaborInCaloriesValue(80, typeof(AdvancedSmeltingSkill));

            // Defines our crafting time for the recipe
            this.CraftMinutes = CreateCraftTimeValue(beneficiary: typeof(AdvancedLiquifactedCoalRecipe), start: 1, skillType: typeof(AdvancedSmeltingSkill), typeof(AdvancedSmeltingFocusedSpeedTalent), typeof(AdvancedSmeltingParallelSpeedTalent));

            // XPs
            this.ExperienceOnCraft = 2; 

            // Perform pre/post initialization for user mods and initialize our recipe instance with the display name "Charred Mortar"
            this.ModsPreInitialize();
            this.Initialize(displayText: Localizer.DoStr("Advanced Liquifacted Coal"), recipeType: typeof(AdvancedLiquifactedCoalRecipe));
            this.ModsPostInitialize();

            // Register our RecipeFamily instance with the crafting system so it can be crafted.
            CraftingComponent.AddRecipe(tableType: typeof(BlastFurnaceObject), recipe: this);
        }
        /// <summary>Hook for mods to customize RecipeFamily before initialization. You can change recipes, xp, labor, time here.</summary>
        partial void ModsPreInitialize();

        /// <summary>Hook for mods to customize RecipeFamily after initialization, but before registration. You can change skill requirements here.</summary>
        partial void ModsPostInitialize();
    }
}