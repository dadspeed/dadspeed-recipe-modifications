﻿namespace Eco.Mods.TechTree 
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using Eco.Gameplay.Blocks;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.DynamicValues;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Objects;
    using Eco.Gameplay.Players;
    using Eco.Gameplay.Skills;
    using Eco.Gameplay.Settlements;
    using Eco.Gameplay.Systems;
    using Eco.Gameplay.Systems.TextLinks;
    using Eco.Shared.Localization;
    using Eco.Shared.Serialization;
    using Eco.Shared.Utils;
    using Eco.Shared.Time;
    using Eco.Core.Items;
    using Eco.World;
    using Eco.World.Blocks;
    using Eco.Gameplay.Pipes;
    using Eco.Core.Controller;
    using Eco.Gameplay.Items.Recipes;

    [Serialized]
    [LocDisplayName("Fish Oil")] 
    [Weight(100)] 
    [Ecopedia("Items", "Products", createAsSubPage: true)]
    [LocDescription("A Cooking oil derived from fish")]
    [Tag("Fat")]
    [Tag("Oil")]
    [Fuel(1000)][Tag("Fuel")] // Marks Fish oil as fuel item. Change is for tallow wall lamp, set to half tallow fuel
    public partial class FishOilItem : FoodItem
    {
        public override LocString DisplayNamePlural     => Localizer.DoStr("Fish Oil");
        public override float Calories                  => 200;
        public override Nutrients Nutrition             => new Nutrients() { Carbs = 0, Fat = 25, Protein = 0, Vitamins = 3};
        protected override float BaseShelfLife          => (float)TimeUtil.HoursToSeconds(220);
    }
}